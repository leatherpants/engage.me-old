<?php

Route::group(['namespace' => 'Backend', 'prefix' => 'backend', 'middleware' => ['admin', 'auth']], function () {
    Route::get('', ['uses' => 'HomeController@getIndex', 'as' => 'backend']);

    Route::group(['prefix' => 'companies'], function () {
        Route::get('', ['uses' => 'CompanyController@getIndex', 'as' => 'backend.companies']);
        Route::get('{slug}', ['uses' => 'CompanyController@getCompany', 'as' => 'backend.companies.company']);
        Route::get('{slug}/jobs', ['uses' => 'CompanyController@getJobs', 'as' => 'backend.company.jobs']);
        Route::get('{slug}/location', ['uses' => 'CompanyController@getLocation', 'as' => 'backend.company.location']);
        Route::post('{slug}/location', ['uses' => 'CompanyController@postLocation']);
    });

    Route::group(['prefix' => 'feedback'], function () {
        Route::get('', ['uses' => 'FeedbackController@getIndex', 'as' => 'backend.feedback']);
    });

    Route::group(['prefix' => 'help'], function () {
        Route::get('', ['uses' => 'HelpController@getIndex', 'as' => 'backend.help']);
    });

    Route::group(['prefix' => 'news'], function () {
        Route::get('', ['uses' => 'NewsController@getIndex', 'as' => 'backend.news']);
        Route::get('add', ['uses' => 'NewsController@addNews', 'as' => 'backend.news.add']);
        Route::get('edit/{id}', ['uses' => 'NewsController@editNews', 'as' => 'backend.news.edit']);
        Route::post('edit/{id}', ['uses' => 'NewsController@updateNews']);
    });

    Route::group(['prefix' => 'settings'], function () {
        Route::get('', ['uses' => 'SettingsController@getIndex', 'as' => 'backend.settings']);
        Route::get('language', ['uses' => 'LanguagesController@getIndex', 'as' => 'backend.languages']);
        Route::post('language', ['uses' => 'LanguagesController@updateToggle']);
    });

    Route::group(['prefix' => 'statistics'], function () {
        Route::get('', ['uses' => 'StatisticsController@getIndex', 'as' => 'backend.statistics']);
        Route::get('users', ['uses' => 'StatisticsController@getUsers', 'as' => 'backend.statistics.users']);
    });

    Route::group(['prefix' => 'support'], function () {
        Route::get('', ['uses' => 'SupportController@getIndex', 'as' => 'backend.support']);
        Route::get('assigned', ['uses' => 'SupportController@getAssignedTickets', 'as' => 'backend.support.assigned']);
        Route::get('closed', ['uses' => 'SupportController@getClosedTickets', 'as' => 'backend.support.closed']);
        Route::get('{id}/assign', ['uses' => 'SupportController@assignToMe', 'as' => 'backend.assign']);
        Route::get('{id}', ['uses' => 'SupportController@getTicket', 'as' => 'backend.support.ticket']);
    });

    Route::group(['prefix' => 'users'], function () {
        Route::get('', ['uses' => 'UserController@getIndex', 'as' => 'backend.users']);
        Route::get('{slug}', ['uses' => 'UserController@getView', 'as' => 'backend.users.view']);
        Route::get('{slug}/settings', ['uses' => 'UserController@getSettings', 'as' => 'backend.users.settings']);
        Route::get('{slug}/support', ['uses' => 'UserController@getSupport', 'as' => 'backend.users.support']);
    });
});

Route::group(['namespace' => 'Frontend'], function () {
    Route::get('', ['uses' => 'HomeController@getIndex', 'as' => 'index']);

    Route::get('signin', ['uses' => 'AuthController@getSignIn', 'as' => 'login']);
    Route::post('signin', ['uses' => 'AuthController@postSignIn']);
    Route::get('signup', ['uses' => 'AuthController@getSignUp', 'as' => 'signup']);
    Route::post('signup', ['uses' => 'AuthController@postSignUp']);
    Route::get('signout', ['uses' => 'AuthController@getSignOut', 'as' => 'signout']);

    Route::group(['middleware' => 'auth'], function () {
        Route::get('home', ['uses' => 'HomeController@getHome', 'as' => 'home']);

        Route::get('aboutus', ['uses' => 'ContentController@getAboutUs', 'as' => 'aboutus']);

        Route::group(['prefix' => 'applications', 'middleware' => 'user'], function () {
            Route::get('', ['uses' => 'ApplicationsController@getIndex', 'as' => 'applications']);
            Route::get('write', ['uses' => 'ApplicationsController@getWrite', 'as' => 'applications.write']);
            Route::get('{company}/{title}', ['uses' => 'ApplicationsController@getView', 'as' => 'applications.view']);
        });

        Route::group(['prefix' => 'blog'], function () {
            Route::get('', ['uses' => 'NewsController@getIndex', 'as' => 'blog']);
            Route::get('{slug}', ['uses' => 'NewsController@getStoryBySlug', 'as' => 'blog.story']);
        });

        Route::group(['prefix' => 'company'], function () {
            Route::get('', ['uses' => 'CompanyController@getIndex', 'as' => 'companies']);
            Route::get('{slug}', ['uses' => 'CompanyController@getCompany', 'as' => 'companies.view']);
            Route::get('{slug}/employees', ['uses' => 'CompanyController@getEmployees', 'as' => 'companies.employees']);
        });

        Route::group(['prefix' => 'contacts'], function () {
            Route::get('', ['uses' => 'ContactsController@getIndex', 'as' => 'contacts']);
            Route::get('sent', ['uses' => 'ContactsController@sentRequests', 'as' => 'contacts.sent']);
            Route::get('received', ['uses' => 'ContactsController@receivedRequests', 'as' => 'contacts.received']);
            Route::get('add/{userid}', ['uses' => 'ContactsController@addContact', 'as' => 'contacts.add']);
            Route::get('accept/{userid}', ['uses' => 'ContactsController@acceptContact', 'as' => 'contacts.accept']);
            Route::get('delete/{userid}', ['uses' => 'ContactsController@deleteContact', 'as' => 'contacts.delete']);
        });

        Route::group(['prefix' => 'cv', 'middleware' => 'user'], function () {
            Route::get('all', ['uses' => 'CVController@getAll', 'as' => 'cv.all']);
            Route::get('all/{company}/{slug}', ['uses' => 'CVController@getAllWithLetter', 'as' => 'cv.all.letter']);
            Route::get('cv', ['uses' => 'CVController@getCV', 'as' => 'cv.cv']);
            Route::get('letter/{company}/{slug}', ['uses' => 'CVController@getLetter', 'as' => 'cv.letter']);
            Route::get('frontpage', ['uses' => 'CVController@getFrontpage', 'as' => 'cv.frontpage']);
        });

        Route::group(['prefix' => 'help'], function () {
            Route::get('', ['uses' => 'HelpController@getIndex', 'as' => 'help']);

            Route::group(['prefix' => 'bug'], function () {
                Route::get('', ['uses' => 'BugController@getIndex', 'as' => 'bug']);
            });

            Route::group(['prefix' => 'forum'], function () {
                Route::get('', ['uses' => 'ForumController@getIndex', 'as' => 'forum']);
            });

            Route::group(['prefix' => 'support'], function () {
                Route::get('', ['uses' => 'SupportController@getIndex', 'as' => 'support']);
                Route::get('create', ['uses' => 'SupportController@addRequest', 'as' => 'support.create']);
                Route::post('create', ['uses' => 'SupportController@createRequest']);
                Route::get('{id}', ['uses' => 'SupportController@getTicket', 'as' => 'support.ticket']);
            });
        });

        Route::group(['prefix' => 'jobs'], function () {
            Route::get('', ['uses' => 'JobsController@getIndex', 'as' => 'jobs']);
            Route::get('{company}/{title}', ['uses' => 'JobsController@getJobs', 'as' => 'jobs.view']);
        });

        Route::group(['prefix' => 'messages'], function () {
            Route::get('', ['uses' => 'MessagesController@getIndex', 'as' => 'messages']);
        });

        Route::group(['prefix' => 'notifications'], function () {
            Route::get('', ['uses' => 'NotificationController@getIndex', 'as' => 'notifications']);
        });

        Route::group(['prefix' => 'search'], function () {
            Route::get('', ['uses' => 'SearchController@getIndex', 'as' => 'search']);
            Route::get('results', ['uses' => 'SearchController@getResults', 'as' => 'search.results']);
        });

        Route::group(['prefix' => 'settings'], function () {
            Route::get('', ['uses' => 'SettingsController@getIndex', 'as' => 'settings']);
            Route::get('account', ['uses' => 'SettingsController@getAccount', 'as' => 'settings.account']);
            Route::post('account', ['uses' => 'SettingsController@postAccount']);
            Route::get('email', ['uses' => 'SettingsController@getEmail', 'as' => 'settings.email']);
            Route::post('email', ['uses' => 'SettingsController@postEmail']);
            Route::get('image', ['uses' => 'SettingsController@getImage', 'as' => 'settings.image']);
            Route::post('image', ['uses' => 'SettingsController@postImage']);
            Route::get('language', ['uses' => 'SettingsController@getLanguage', 'as' => 'settings.language']);
            Route::post('language', ['uses' => 'SettingsController@postLanguage']);
            Route::get('password', ['uses' => 'SettingsController@getPassword', 'as' => 'settings.password']);
            Route::post('password', ['uses' => 'SettingsController@postPassword']);
            Route::get('search', ['uses' => 'SettingsController@getSearch', 'as' => 'settings.search']);
            Route::post('search', ['uses' => 'SettingsController@postSearch']);
        });

        Route::group(['prefix' => 'yourcompany', 'namespace' => 'YourCompany', 'middleware' => 'company'], function () {
            Route::get('', ['uses' => 'DashboardController@getIndex', 'as' => 'yourcompany']);
            Route::get('{slug}', ['uses' => 'DashboardController@getCompany', 'as' => 'yourcompany.company']);
            Route::get('{slug}/details', ['uses' => 'DashboardController@getDetails', 'as' => 'yourcompany.details']);
            Route::get('{slug}/employees', ['uses' => 'DashboardController@getEmployees', 'as' => 'yourcompany.employees']);
        });

        Route::get('{slug}', ['uses' => 'UserController@getProfile', 'as' => 'profile']);
        Route::get('{slug}/details', ['uses' => 'UserController@getDetails', 'as' => 'profile.details']);
        Route::get('{slug}/profiles', ['uses' => 'UserController@getProfiles', 'as' => 'profile.profiles']);
        Route::get('{slug}/stats', ['uses' => 'UserController@getStats', 'as' => 'profile.stats']);
    });
});
