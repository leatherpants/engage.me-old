<?php

namespace Tests\Feature;

use Tests\TestCase;
use App\engageme\Users\Models\Users;
use Illuminate\Foundation\Testing\RefreshDatabase;

class SearchTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function test_get_search_index()
    {
        $user = factory(Users::class)->create([
            'type' => 'user',
        ]);

        $response = $this->actingAs($user)->get('/search');
        $response->assertSuccessful();
    }

    /** @test */
    public function test_get_search_results()
    {
        $user = factory(Users::class)->create([
            'type' => 'user',
        ]);

        $search = factory(Users::class)->create([
            'type' => 'user',
        ]);

        $response = $this->actingAs($user)->get('/search/results?q='.$search->first_name);
        $response->assertSuccessful();
    }
}
