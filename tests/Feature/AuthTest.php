<?php

namespace Tests\Feature;

use Tests\TestCase;
use App\engageme\Users\Models\Users;
use Illuminate\Foundation\Testing\RefreshDatabase;

class AuthTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function test_get_login_page()
    {
        $response = $this->get(route('login'));
        $response->assertSuccessful();
    }

    /** @test */
    public function test_get_login()
    {
        $user = factory(Users::class)->create([
            'type' => 'user',
        ]);

        $response = $this->post(route('login'), ['email' => $user->email, 'password' => 'secret']);
        $response->assertStatus(302);
        $response->assertRedirect(route('home'));
    }

    /** @test */
    public function test_get_login_false()
    {
        $user = factory(Users::class)->create([
            'type' => 'user',
        ]);

        $response = $this->post(route('login'), ['email' => $user->email, 'password' => 'xxxyyy']);
        $response->assertStatus(302);
        $response->assertRedirect(route('index'));
    }

    /** @test */
    public function test_get_register_page()
    {
        $response = $this->get(route('signup'));
        $response->assertSuccessful();
    }

    /** @test */
    public function test_get_register()
    {
        $user = [
            'first_name' => 'John',
            'last_name' => 'Doe',
            'email' => 'johndoe@test.com',
            'password' => 'secret',
        ];

        $response = $this->post(route('signup'), $user);
        $response->assertStatus(302);
        $response->assertRedirect(route('login'));
    }

    /** @test */
    public function test_get_logout()
    {
        $user = factory(Users::class)->create([
            'type' => 'user',
        ]);

        $response = $this->actingAs($user)->get(route('signout'));
        $response->assertRedirect(route('login'));
    }
}
