<?php

namespace Tests\Feature;

use Tests\TestCase;
use App\engageme\Users\Models\Users;
use Illuminate\Foundation\Testing\RefreshDatabase;

class PagesTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function test_get_index_not_logged_in()
    {
        $response = $this->get(route('index'));
        $response->assertViewIs('auth.signin');
    }

    /** @test */
    public function test_get_index()
    {
        $user = factory(Users::class)->create([
            'type' => 'user',
        ]);

        $response = $this->actingAs($user)->get(route('index'));
        $response->assertStatus(302);
        $response->assertRedirect(route('home'));
    }

    /** @test */
    public function test_get_about_us()
    {
        $user = factory(Users::class)->create([
            'type' => 'user',
        ]);

        $response = $this->actingAs($user)->get(route('aboutus'));
        $response->assertSuccessful();
    }
}
