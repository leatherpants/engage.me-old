<?php

namespace Tests\Feature;

use Tests\TestCase;
use App\engageme\Users\Models\Users;
use App\engageme\Users\Models\UsersSettings;
use Illuminate\Foundation\Testing\RefreshDatabase;

class SettingsTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function test_get_settings()
    {
        $user = factory(Users::class)->create([
            'type' => 'user',
        ]);

        factory(UsersSettings::class)->create([
            'user_id' => $user->id,
            'beta' => true,
        ]);

        $response = $this->actingAs($user)->get('/settings');

        $response->assertStatus(200);
    }

    /** @test */
    public function test_get_settings_account()
    {
        $user = factory(Users::class)->create([
            'type' => 'user',
        ]);

        factory(UsersSettings::class)->create([
            'user_id' => $user->id,
            'beta' => true,
        ]);

        $response = $this->actingAs($user)->get('/settings/account');
        $response->assertStatus(200);
    }

    /** @test */
    public function test_get_settings_email()
    {
        $user = factory(Users::class)->create([
            'type' => 'user',
        ]);

        factory(UsersSettings::class)->create([
            'user_id' => $user->id,
            'beta' => true,
        ]);

        $response = $this->actingAs($user)->get('/settings/email');
        $response->assertStatus(200);
    }

    /** @test */
    public function test_get_settings_image()
    {
        $user = factory(Users::class)->create([
            'type' => 'user',
        ]);

        factory(UsersSettings::class)->create([
            'user_id' => $user->id,
            'beta' => true,
        ]);

        $response = $this->actingAs($user)->get('/settings/image');
        $response->assertStatus(200);
    }

    /** @test */
    public function test_get_settings_language()
    {
        $user = factory(Users::class)->create([
            'type' => 'user',
        ]);

        factory(UsersSettings::class)->create([
            'user_id' => $user->id,
            'beta' => true,
        ]);

        $response = $this->actingAs($user)->get('/settings/language');
        $response->assertStatus(200);
    }

    /** @test */
    public function test_get_settings_password()
    {
        $user = factory(Users::class)->create([
            'type' => 'user',
        ]);

        factory(UsersSettings::class)->create([
            'user_id' => $user->id,
            'beta' => true,
        ]);

        $response = $this->actingAs($user)->get('/settings/password');
        $response->assertStatus(200);
    }

    /** @test */
    public function test_get_settings_search()
    {
        $user = factory(Users::class)->create([
            'type' => 'user',
        ]);

        factory(UsersSettings::class)->create([
            'user_id' => $user->id,
            'beta' => true,
        ]);

        $response = $this->actingAs($user)->get('/settings/search');
        $response->assertStatus(200);
    }

    /** @test */
    public function test_get_settings_search_no_beta()
    {
        $user = factory(Users::class)->create([
            'type' => 'user',
        ]);

        factory(UsersSettings::class)->create([
            'user_id' => $user->id,
            'beta' => false,
        ]);

        $response = $this->actingAs($user)->get('/settings/search');
        $response->assertStatus(302);
        $response->assertRedirect('/settings');
    }
}
