<?php

namespace Tests\Feature;

use Tests\TestCase;
use App\engageme\Users\Models\Users;
use App\engageme\Companies\Models\Companies;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\engageme\Companies\Models\CompaniesDetails;
use App\engageme\Companies\Models\CompaniesSocials;

class CompaniesTest extends TestCase
{
    use RefreshDatabase;

    /** @test  */
    public function test_get_companies()
    {
        $user = factory(Users::class)->create([
            'type' => 'user',
        ]);

        $response = $this->actingAs($user)->get('/company');

        $response->assertStatus(200);
    }

    /** @test */
    public function test_get_company()
    {
        $user = factory(Users::class)->create([
            'type' => 'user',
        ]);

        $company = factory(Companies::class)->create([
            'title' => 'engage.me',
            'slug' => 'engageme',
        ]);

        factory(CompaniesSocials::class)->create([
            'company_id' => $company->id,
        ]);

        factory(CompaniesDetails::class)->create([
            'company_id' => $company->id,
        ]);

        $response = $this->actingAs($user)->get('/company/'.$company->slug);

        $response->assertStatus(200);
    }

    /** @test */
    public function test_get_company_fails()
    {
        $user = factory(Users::class)->create([
            'type' => 'user',
        ]);

        $response = $this->actingAs($user)->get('/company/scuderia-ferrrari');

        $response->assertStatus(404);
    }

    /** @test */
    public function test_get_company_employees()
    {
        $user = factory(Users::class)->create([
            'type' => 'user',
        ]);

        $company = factory(Companies::class)->create([
            'title' => 'engage.me',
            'slug' => 'engageme',
        ]);

        factory(CompaniesSocials::class)->create([
            'company_id' => $company->id,
        ]);

        factory(CompaniesDetails::class)->create([
            'company_id' => $company->id,
        ]);

        $response = $this->actingAs($user)->get('/company/'.$company->slug.'/employees');

        $response->assertStatus(200);
    }

    /** @test */
    public function test_get_company_employees_fails()
    {
        $user = factory(Users::class)->create([
            'type' => 'user',
        ]);

        $response = $this->actingAs($user)->get('/company/scuderia-ferrrari/employees');

        $response->assertStatus(404);
    }
}
