<?php

namespace Tests\Feature\Backend;

use Tests\TestCase;
use App\engageme\Users\Models\Users;
use Illuminate\Foundation\Testing\RefreshDatabase;

class HomeTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function test_get_backend()
    {
        $user = factory(Users::class)->create([
            'type' => 'user',
            'isAdmin' => true,
        ]);

        $response = $this->actingAs($user)->get('/backend');
        $response->assertSuccessful();
    }

    /** @test */
    public function test_get_backend_fails()
    {
        $user = factory(Users::class)->create([
            'type' => 'user',
            'isAdmin' => false,
        ]);

        $response = $this->actingAs($user)->get('/backend');
        $response->assertStatus(302);
        $response->assertRedirect('/home');
    }
}
