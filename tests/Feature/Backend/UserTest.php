<?php

namespace Tests\Feature\Backend;

use Tests\TestCase;
use App\engageme\Users\Models\Users;
use App\engageme\Users\Models\UsersSettings;
use Illuminate\Foundation\Testing\RefreshDatabase;

class UserTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function test_get_user_backend()
    {
        $user = factory(Users::class)->create([
            'type' => 'user',
            'isAdmin' => true,
        ]);

        factory(UsersSettings::class)->create([
            'user_id' => $user->id,
        ]);

        $response = $this->actingAs($user)->get('/backend/users');
        $response->assertSuccessful();
    }

    /** @test */
    public function test_get_user()
    {
        $user = factory(Users::class)->create([
            'type' => 'user',
            'isAdmin' => true,
        ]);

        $response = $this->actingAs($user)->get('/backend/users/'.$user->slug_name);
        $response->assertSuccessful();
    }

    /** @test */
    public function test_get_user_fails()
    {
        $user = factory(Users::class)->create([
            'type' => 'user',
            'isAdmin' => true,
        ]);

        $response = $this->actingAs($user)->get('/backend/users/john-doe');
        $response->assertNotFound();
    }

    /** @test */
    public function test_get_user_settings()
    {
        $user = factory(Users::class)->create([
            'type' => 'user',
            'isAdmin' => true,
        ]);

        $response = $this->actingAs($user)->get('/backend/users/'.$user->slug_name.'/settings');
        $response->assertSuccessful();
    }

    /** @test */
    public function test_get_user_settings_fails()
    {
        $user = factory(Users::class)->create([
            'type' => 'user',
            'isAdmin' => true,
        ]);

        $response = $this->actingAs($user)->get('/backend/users/john-doe/settings');
        $response->assertNotFound();
    }

    /** @test */
    public function test_get_user_support()
    {
        $user = factory(Users::class)->create([
            'type' => 'user',
            'isAdmin' => true,
        ]);

        $response = $this->actingAs($user)->get('/backend/users/'.$user->slug_name.'/support');
        $response->assertSuccessful();
    }

    /** @test */
    public function test_get_user_support_fails()
    {
        $user = factory(Users::class)->create([
            'type' => 'user',
            'isAdmin' => true,
        ]);

        $response = $this->actingAs($user)->get('/backend/users/john-doe/support');
        $response->assertNotFound();
    }
}
