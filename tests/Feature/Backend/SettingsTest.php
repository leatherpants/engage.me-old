<?php

namespace Tests\Feature\Backend;

use Tests\TestCase;
use App\engageme\Users\Models\Users;
use Illuminate\Foundation\Testing\RefreshDatabase;

class SettingsTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function test_get_settings_backend()
    {
        $user = factory(Users::class)->create([
            'type' => 'user',
            'isAdmin' => true,
        ]);

        $response = $this->actingAs($user)->get('/backend/settings');
        $response->assertSuccessful();
    }
}
