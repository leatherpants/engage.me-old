<?php

namespace Tests\Feature;

use Tests\TestCase;
use App\engageme\Users\Models\Users;
use Illuminate\Foundation\Testing\RefreshDatabase;

class HelpTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function test_get_help()
    {
        $user = factory(Users::class)->create([
            'type' => 'user',
        ]);

        $response = $this->actingAs($user)->get('/help');
        $response->assertSuccessful();
    }
}
