<?php

namespace Tests\Feature;

use Tests\TestCase;
use App\engageme\Jobs\Models\Jobs;
use App\engageme\Users\Models\Users;
use App\engageme\Companies\Models\Companies;
use App\engageme\Countries\Models\Countries;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\engageme\Companies\Models\CompaniesLocations;

class JobsTest extends TestCase
{
    use RefreshDatabase;

    /** @test  */
    public function test_get_jobs()
    {
        $user = factory(Users::class)->create([
            'type' => 'user',
        ]);

        $response = $this->actingAs($user)->get('/jobs');

        $response->assertStatus(200);
    }

    /** @test */
    public function test_get_job()
    {
        $user = factory(Users::class)->create([
            'type' => 'user',
        ]);

        $company = factory(Companies::class)->create([
            'title' => 'Scuderia Ferrari',
            'slug' => 'scuderia-ferrari',
        ]);

        $countries = factory(Countries::class)->create();

        $companyLocation = factory(CompaniesLocations::class)->create([
            'company_id' => $company->id,
            'country' => $countries->code,
        ]);

        $job = factory(Jobs::class)->create([
            'company_id' => $company->id,
            'location_id' => $companyLocation->company_id,
            'title' => 'Simulator driver',
            'slug' => 'simulator-driver',
        ]);

        $response = $this->actingAs($user)->get('/jobs/'.$company->slug.'/'.$job->slug);

        $response->assertStatus(200);
    }

    /** @test */
    public function test_get_job_fails()
    {
        $user = factory(Users::class)->create([
            'type' => 'user',
        ]);

        $response = $this->actingAs($user)->get('/jobs/mclaren-renault/simdriver');

        $response->assertStatus(404);
    }
}
