<?php

namespace Tests\Feature;

use Tests\TestCase;
use App\engageme\Users\Models\Users;
use Illuminate\Foundation\Testing\RefreshDatabase;

class CVTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function test_get_cv_frontpage()
    {
        $user = factory(Users::class)->create([
            'type' => 'user',
        ]);

        $response = $this->actingAs($user)->get('/cv/frontpage');

        $response->assertStatus(200);
    }

    /** @test */
    public function test_get_cv()
    {
        $user = factory(Users::class)->create([
            'type' => 'user',
        ]);

        $response = $this->actingAs($user)->get('/cv/cv');

        $response->assertStatus(200);
    }
}
