<?php

namespace Tests\Feature;

use Tests\TestCase;
use App\engageme\Users\Models\Users;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ContactsTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function test_get_contacts()
    {
        $user = factory(Users::class)->create([
            'type' => 'user',
        ]);

        $response = $this->actingAs($user)->get('/contacts');

        $response->assertStatus(200);
    }

    /** @test */
    public function test_get_contacts_sent()
    {
        $user = factory(Users::class)->create([
            'type' => 'user',
        ]);

        $response = $this->actingAs($user)->get('/contacts/sent');

        $response->assertStatus(200);
    }

    /** @test */
    public function test_get_contacts_received()
    {
        $user = factory(Users::class)->create([
            'type' => 'user',
        ]);

        $response = $this->actingAs($user)->get('/contacts/received');

        $response->assertStatus(200);
    }

    /** @test */
    public function test_add_contacts()
    {
        $user = factory(Users::class)->create([
            'type' => 'user',
        ]);

        $contact = factory(Users::class)->create([
            'type' => 'user',
        ]);

        $contacts = [
            'sender' => $user->id,
            'receiver' => $contact->id,
            'status' => 1,
        ];

        $this->actingAs($user)->post(
            '/contacts/add/'.$contact->id,
            $contacts
        );

        $this->assertCount(3, $contacts);
    }
}
