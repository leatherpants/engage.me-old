<?php

namespace Tests\Feature;

use Tests\TestCase;
use App\engageme\Users\Models\Users;
use App\engageme\Companies\Models\Companies;
use App\engageme\Applications\Models\Applications;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\engageme\Companies\Models\CompaniesLocations;

class ApplicationsTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function test_get_applications()
    {
        $user = factory(Users::class)->create([
            'type' => 'user',
        ]);

        $response = $this->actingAs($user)->get('/applications');

        $response->assertStatus(200);
    }

    /** @test */
    public function test_write_applications()
    {
        $user = factory(Users::class)->create([
            'type' => 'user',
        ]);

        $response = $this->actingAs($user)->get('/applications/write');

        $response->assertStatus(200);
    }

    /** @test */
    public function test_get_application()
    {
        $user = factory(Users::class)->create([
            'type' => 'user',
        ]);

        $company = factory(Companies::class)->create([
            'title' => 'Scuderia Ferrari',
            'slug' => 'scuderia-ferrari',
        ]);

        $companyLocation = factory(CompaniesLocations::class)->create([
            'company_id' => $company->id,
        ]);

        $application = factory(Applications::class)->create([
            'user_id' => $user->id,
            'company_id' => $company->id,
            'location_id' => $companyLocation->id,
            'title' => 'Simulator driver',
            'slug' => 'simulator-driver',
        ]);

        $response = $this->actingAs($user)->get('/applications/'.$company->slug.'/'.$application->slug);

        $response->assertStatus(200);
    }
}
