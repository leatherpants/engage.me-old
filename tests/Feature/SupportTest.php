<?php

namespace Tests\Feature;

use Tests\TestCase;
use App\engageme\Users\Models\Users;
use App\engageme\Support\Models\SupportTickets;
use Illuminate\Foundation\Testing\RefreshDatabase;

class SupportTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function test_get_support()
    {
        $user = factory(Users::class)->create([
            'type' => 'user',
        ]);

        $response = $this->actingAs($user)->get('/help/support');
        $response->assertSuccessful();
    }

    /** @test */
    public function test_get_create_support_ticket()
    {
        $user = factory(Users::class)->create([
            'type' => 'user',
        ]);

        $response = $this->actingAs($user)->get('/help/support/create');
        $response->assertSuccessful();
    }

    /** @test */
    public function test_get_store_support_ticket()
    {
        $user = factory(Users::class)->create([
            'type' => 'user',
        ]);

        $ticket = factory(SupportTickets::class)->create();

        $data = [
            'user_id' => $user->id,
            'title' => $ticket->title,
            'content' => $ticket->content,
        ];

        $response = $this->actingAs($user)->post('/help/support/create', $data);
        $response->assertStatus(302);
        $response->assertRedirect('/help/support');
    }

    /** @test */
    public function test_get_ticket_with_assignee()
    {
        $user = factory(Users::class)->create([
            'type' => 'user',
        ]);

        $assignee = factory(Users::class)->create([
            'type' => 'user',
        ]);

        $ticket = factory(SupportTickets::class)->create([
            'user_id' => $user->id,
            'assignee' => $assignee->id,
        ]);

        $response = $this->actingAs($user)->get('/help/support/'.$ticket->id);
        $response->assertSuccessful();
    }

    /** @test */
    public function test_get_ticket_without_assignee()
    {
        $user = factory(Users::class)->create([
            'type' => 'user',
        ]);

        $ticket = factory(SupportTickets::class)->create([
            'user_id' => $user->id,
            'assignee' => null,
        ]);

        $response = $this->actingAs($user)->get('/help/support/'.$ticket->id);
        $response->assertSuccessful();
    }
}
