<?php

namespace Tests\Feature;

use Tests\TestCase;
use App\engageme\Users\Models\Users;
use Illuminate\Foundation\Testing\RefreshDatabase;

class UsersTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function test_get_users_profile()
    {
        $user = factory(Users::class)->create([
            'display_name' => 'Sandra Granath',
            'slug_name' => 'sandra-granath',
            'type' => 'user',
        ]);

        $response = $this->actingAs($user)->get('/sandra-granath');

        $response->assertStatus(200);
    }

    /** @test */
    public function test_get_users_profile_fails()
    {
        $user = factory(Users::class)->create([
                'type' => 'user',
            ]);

        $response = $this->actingAs($user)->get('/sandra-granath');

        $response->assertStatus(404);
    }

    /** @test */
    public function test_get_users_details()
    {
        $user = factory(Users::class)->create([
            'display_name' => 'Sandra Granath',
            'slug_name' => 'sandra-granath',
            'type' => 'user',
        ]);

        $response = $this->actingAs($user)->get('/sandra-granath/details');

        $response->assertStatus(200);
    }

    /** @test */
    public function test_get_users_profiles()
    {
        $user = factory(Users::class)->create([
            'display_name' => 'Sandra Granath',
            'slug_name' => 'sandra-granath',
            'type' => 'user',
        ]);

        $response = $this->actingAs($user)->get('/sandra-granath/profiles');

        $response->assertStatus(200);
    }

    /** @test */
    public function test_get_users_stats()
    {
        $user = factory(Users::class)->create([
            'display_name' => 'Sandra Granath',
            'slug_name' => 'sandra-granath',
            'type' => 'user',
        ]);

        $response = $this->actingAs($user)->get('/sandra-granath/stats');

        $response->assertStatus(200);
    }
}
