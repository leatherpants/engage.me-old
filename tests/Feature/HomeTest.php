<?php

namespace Tests\Feature;

use Tests\TestCase;
use App\engageme\Users\Models\Users;
use Illuminate\Foundation\Testing\RefreshDatabase;

class HomeTest extends TestCase
{
    use RefreshDatabase;

    /** @test  */
    public function test_get_home_dashboard()
    {
        $user = factory(Users::class)->create([
            'type' => 'user',
        ]);

        $response = $this->actingAs($user)->get('/home');

        $response->assertStatus(200);
    }
}
