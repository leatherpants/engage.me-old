<?php

namespace Tests\Feature;

use Tests\TestCase;
use App\engageme\News\Models\News;
use App\engageme\Users\Models\Users;
use Illuminate\Foundation\Testing\RefreshDatabase;

class NewsTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function test_get_news()
    {
        $user = factory(Users::class)->create([
            'type' => 'user',
        ]);

        $response = $this->actingAs($user)->get('/blog');
        $response->assertSuccessful();
    }

    /** @test */
    public function test_get_news_by_slug()
    {
        $user = factory(Users::class)->create([
            'type' => 'user',
        ]);

        $news = factory(News::class)->create([
            'user_id' => $user->id,
        ]);

        $response = $this->actingAs($user)->get('/blog/'.$news->slug);
        $response->assertSuccessful();
    }

    /** @test */
    public function test_get_news_by_slug_fails()
    {
        $user = factory(Users::class)->create([
            'type' => 'user',
        ]);

        $response = $this->actingAs($user)->get('/blog/news-not-available');
        $response->assertNotFound();
    }
}
