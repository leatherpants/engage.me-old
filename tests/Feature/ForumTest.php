<?php

namespace Tests\Feature;

use Tests\TestCase;
use App\engageme\Users\Models\Users;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ForumTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function test_get_forum()
    {
        $user = factory(Users::class)->create([
            'type' => 'user',
        ]);

        $response = $this->actingAs($user)->get('/help/forum');
        $response->assertSuccessful();
    }
}
