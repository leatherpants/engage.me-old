<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersEducationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users_educations', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('user_id');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->string('lang', 5);
            $table->string('name')->nullable();
            $table->string('city')->nullable();
            $table->string('subject')->nullable();
            $table->string('degree')->nullable();
            $table->text('notes')->nullable();
            $table->date('begin_date')->nullable();
            $table->date('end_date')->nullable();
            $table->boolean('education');
            $table->nullableTimestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users_educations');
    }
}
