<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('email');
            $table->string('password');
            $table->string('display_name');
            $table->string('slug_name');
            $table->string('first_name');
            $table->string('last_name');
            $table->string('gender', 1)->nullable();
            $table->string('lang', 5)->nullable();
            $table->date('birthday')->nullable();
            $table->string('occupation')->nullable();
            $table->enum('type', ['all', 'company', 'user']);
            $table->boolean('verified')->default(false);
            $table->boolean('isAdmin')->default(false);
            $table->boolean('isActive')->default(true);
            $table->rememberToken();
            $table->nullableTimestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
