<?php

use Faker\Generator as Faker;

$factory->define(\App\engageme\Companies\Models\CompaniesSocials::class, function (Faker $faker) {
    return [
        'company_id' => $faker->randomDigit,
        'social_provider' => $faker->randomDigit,
        'url' => $faker->url,
    ];
});
