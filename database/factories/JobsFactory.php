<?php

use Faker\Generator as Faker;

$factory->define(\App\engageme\Jobs\Models\Jobs::class, function (Faker $faker) {
    return [
        'company_id' => function () {
            return factory(\App\engageme\Companies\Models\Companies::class)->create()->id;
        },
        'location_id' => function () {
            return factory(\App\engageme\Companies\Models\Companies::class)->create()->id;
        },
        'contact_id' => function () {
            return factory(\App\engageme\Companies\Models\Companies::class)->create()->id;
        },
        'title' => $faker->title,
        'slug' => $faker->slug,
        'desc' => $faker->text,
        'level' => $faker->text,
        'job_type' => $faker->text,
    ];
});
