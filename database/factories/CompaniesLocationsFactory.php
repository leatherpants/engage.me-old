<?php

use Faker\Generator as Faker;

$factory->define(\App\engageme\Companies\Models\CompaniesLocations::class, function (Faker $faker) {
    return [
        'company_id' => function () {
            return factory(\App\engageme\Companies\Models\Companies::class)->create()->id;
        },
        'street' => $faker->streetName,
        'zip_code' => $faker->postcode,
        'city' => $faker->city,
        'country' => $faker->countryCode,
    ];
});
