<?php

use Faker\Generator as Faker;

$factory->define(\App\engageme\Countries\Models\Countries::class, function (Faker $faker) {
    return [
        'code' => $faker->countryCode,
        'name' => $faker->country,
    ];
});
