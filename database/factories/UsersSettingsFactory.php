<?php

use Faker\Generator as Faker;

$factory->define(\App\engageme\Users\Models\UsersSettings::class, function (Faker $faker) {
    return [
        'user_id' => function() {
            return factory(\App\engageme\Users\Models\Users::class)->create()->id;
        },
        'beta' => $faker->boolean,
        'search' => $faker->boolean,
    ];
});
