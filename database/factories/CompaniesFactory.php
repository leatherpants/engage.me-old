<?php

use Faker\Generator as Faker;

$factory->define(\App\engageme\Companies\Models\Companies::class, function (Faker $faker) {
    return [
        'title' => $faker->title,
        'slug' => $faker->slug,
        'desc' => $faker->text,
        'image' => $faker->imageUrl(),
    ];
});
