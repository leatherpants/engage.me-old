<?php

use Faker\Generator as Faker;

$factory->define(\App\engageme\Users\Models\Users::class, function (Faker $faker) {
    return [
        'email' => $faker->email,
        'password' => bcrypt('secret'),
        'display_name' => $faker->name,
        'slug_name' => str_slug($faker->name),
        'first_name' => $faker->firstName,
        'last_name' => $faker->lastName,
        'gender' => $faker->randomAscii,
        'lang' => $faker->languageCode,
        'birthday' => $faker->date('Y-m-d'),
        'occupation' => $faker->jobTitle,
        'type' => $faker->text,
        'verified' => $faker->boolean,
        'isAdmin' => $faker->boolean,
        'isActive' => $faker->boolean,
    ];
});
