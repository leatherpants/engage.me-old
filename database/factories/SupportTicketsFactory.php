<?php

use Faker\Generator as Faker;

$factory->define(\App\engageme\Support\Models\SupportTickets::class, function (Faker $faker) {
    return [
        'user_id' => $faker->randomDigit,
        'assignee' => $faker->randomDigit,
        'title' => $faker->title,
        'content' => $faker->text,
        'status' => $faker->randomDigit,
        'priority' => $faker->randomDigit,
    ];
});
