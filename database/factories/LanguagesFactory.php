<?php

use Faker\Generator as Faker;

$factory->define(\App\engageme\Languages\Models\Languages::class, function (Faker $faker) {
    return [
        'code' => $faker->languageCode,
        'name' => $faker->country,
        'active' => $faker->randomDigit,
    ];
});
