<?php

use Faker\Generator as Faker;

$factory->define(\App\engageme\Companies\Models\CompaniesDetails::class, function (Faker $faker) {
    return [
        'company_id' => $faker->randomDigit,
        'owner_id' => $faker->randomDigit,
        'founded_year' => $faker->year,
        'content' => $faker->text,
    ];
});
