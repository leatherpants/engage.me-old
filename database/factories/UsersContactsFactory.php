<?php

use Faker\Generator as Faker;

$factory->define(\App\engageme\Users\Models\UsersContacts::class, function (Faker $faker) {
    return [
        'sender' => function () {
            return factory(App\engageme\Users\Models\Users::class)->create()->id;
        },
        'receiver' => function () {
            return factory(\App\engageme\Users\Models\Users::class)->create()->id;
        },
        'status' => $faker->randomDigit,
    ];
});
