<?php

use Faker\Generator as Faker;

$factory->define(\App\engageme\Applications\Models\Applications::class, function (Faker $faker) {
    return [
        'user_id' => function () {
            return factory(\App\engageme\Users\Models\Users::class)->create()->id;
        },
        'lang' => $faker->languageCode,
        'company_id' => $faker->randomDigit,
        'location_id' => $faker->randomDigit,
        'contact_id' => $faker->randomDigit,
        'type' => $faker->text,
        'title' => $faker->title,
        'slug' => $faker->slug,
        'content' => $faker->text,
        'status' => $faker->text,
    ];
});
