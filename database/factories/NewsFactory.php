<?php

use Faker\Generator as Faker;

$factory->define(\App\engageme\News\Models\News::class, function (Faker $faker) {
    return [
        'lang' => $faker->countryCode,
        'user_id' => $faker->randomDigit,
        'title' => $faker->title,
        'slug' => str_slug($faker->title),
        'content' => $faker->text,
    ];
});
