<?php

namespace App\Http\Middleware;

use Closure;

class SecurityHeaders
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $response = $next($request);

        /*
         * Reference: https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers
         */

        $response->header('content-security-policy', "default-src 'self'; font-src https://fonts.gstatic.com http://*.engageme.granath data: 'self'; img-src http://media.engageme.granath 'self'; style-src https://fonts.googleapis.com 'unsafe-inline' 'self'; script-src 'unsafe-inline' 'self';");
        $response->header('strict-transport-security', 'max-age=631138519');
        $response->header('x-content-type-options', 'nosniff');
        $response->header('x-frame-options', 'SAMEORIGIN');
        $response->header('x-xss-protection', '1; mode=block');

        return $response;
    }
}
