<?php

namespace App\Http\Middleware;

use Closure;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;

class UpdateUserTimestamp
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $response = $next($request);

        if (Auth::check()) {
            $user = Auth::user();
            $lastUpdated = time() - strtotime($user->updated_at);

            if ($user && $lastUpdated > 600) {
                $user->updated_at = Carbon::now();
                $user->save();
            }
        }

        return $response;
    }
}
