<?php

namespace App\Http\Middleware;

use Closure;
use Carbon\Carbon;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;

class Language
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::check()) {
            if ($request->user()->lang == 'se') {
                Carbon::setLocale('sv');
            }
            Carbon::setLocale($request->user()->lang);
            App::setLocale($request->user()->lang);
        } else {
            $lang = explode('-', $request->server('HTTP_ACCEPT_LANGUAGE'));

            /* Checking language in Firefox */
            if (strlen($lang[0]) == 5) {
                $l = explode(',', $lang[0]);
                $l = $l[0];
            } else {
                $l = $lang[0];
            }

            App::setLocale($l);
        }

        return $next($request);
    }
}
