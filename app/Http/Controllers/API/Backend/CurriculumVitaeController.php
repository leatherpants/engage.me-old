<?php

namespace App\Http\Controllers\API\Backend;

use App\Http\Controllers\Controller;
use App\engageme\Users\Repository\UsersRepository;
use App\engageme\Users\Repository\UsersSkillsRepository;
use App\engageme\Users\Repository\UsersHobbiesRepository;
use App\engageme\Users\Repository\UsersSocialsRepository;
use App\engageme\Users\Repository\UsersProjectsRepository;
use App\engageme\Users\Repository\UsersLanguagesRepository;
use App\engageme\Users\Repository\UsersCVMessagesRepository;
use App\engageme\Users\Repository\UsersEducationsRepository;
use App\engageme\Users\Repository\UsersSoftSkillsRepository;
use App\engageme\Users\Repository\UsersExperiencesRepository;
use App\engageme\Users\Repository\UsersCVDesignItemsRepository;
use App\engageme\Applications\Repository\ApplicationsRepository;
use App\engageme\Users\Repository\UsersAddressPrivateRepository;

class CurriculumVitaeController extends Controller
{
    /**
     * @var UsersAddressPrivateRepository
     */
    protected $usersAddressPrivateRepository;

    /**
     * @var UsersCVDesignItemsRepository
     */
    protected $usersCVDesignItemsRepository;

    /**
     * @var UsersCVMessagesRepository
     */
    protected $usersCVMessagesRepository;

    /**
     * @var UsersEducationsRepository
     */
    protected $usersEducationsRepository;

    /**
     * @var UsersExperiencesRepository
     */
    protected $usersExperiencesRepository;

    /**
     * @var UsersHobbiesRepository
     */
    protected $usersHobbiesRepository;

    /**
     * @var UsersLanguagesRepository
     */
    protected $usersLanguagesRepository;

    /**
     * @var UsersProjectsRepository
     */
    protected $usersProjectsRepository;

    /**
     * @var UsersRepository
     */
    protected $usersRepository;

    /**
     * @var UsersSkillsRepository
     */
    protected $usersSkillsRepository;

    /**
     * @var UsersSocialsRepository
     */
    protected $usersSocialsRepository;

    /**
     * @var UsersSoftSkillsRepository
     */
    protected $usersSoftSkillsRepository;

    /**
     * @var ApplicationsRepository
     */
    private $applicationsRepository;

    /**
     * CurriculumVitaeController constructor.
     * @param UsersAddressPrivateRepository $usersAddressPrivateRepository
     * @param UsersCVDesignItemsRepository $usersCVDesignItemsRepository
     * @param UsersCVMessagesRepository $usersCVMessagesRepository
     * @param UsersEducationsRepository $usersEducationsRepository
     * @param UsersExperiencesRepository $usersExperiencesRepository
     * @param UsersHobbiesRepository $usersHobbiesRepository
     * @param UsersLanguagesRepository $usersLanguagesRepository
     * @param UsersProjectsRepository $usersProjectsRepository
     * @param UsersRepository $usersRepository
     * @param UsersSkillsRepository $usersSkillsRepository
     * @param UsersSocialsRepository $usersSocialsRepository
     * @param UsersSoftSkillsRepository $usersSoftSkillsRepository
     * @param ApplicationsRepository $applicationsRepository
     */
    public function __construct(UsersAddressPrivateRepository $usersAddressPrivateRepository,
                                UsersCVDesignItemsRepository $usersCVDesignItemsRepository,
                                UsersCVMessagesRepository $usersCVMessagesRepository,
                                UsersEducationsRepository $usersEducationsRepository,
                                UsersExperiencesRepository $usersExperiencesRepository,
                                UsersHobbiesRepository $usersHobbiesRepository,
                                UsersLanguagesRepository $usersLanguagesRepository,
                                UsersProjectsRepository $usersProjectsRepository,
                                UsersRepository $usersRepository,
                                UsersSkillsRepository $usersSkillsRepository,
                                UsersSocialsRepository $usersSocialsRepository,
                                UsersSoftSkillsRepository $usersSoftSkillsRepository,
                                ApplicationsRepository $applicationsRepository)
    {
        $this->usersAddressPrivateRepository = $usersAddressPrivateRepository;
        $this->usersCVDesignItemsRepository = $usersCVDesignItemsRepository;
        $this->usersCVMessagesRepository = $usersCVMessagesRepository;
        $this->usersEducationsRepository = $usersEducationsRepository;
        $this->usersExperiencesRepository = $usersExperiencesRepository;
        $this->usersHobbiesRepository = $usersHobbiesRepository;
        $this->usersLanguagesRepository = $usersLanguagesRepository;
        $this->usersProjectsRepository = $usersProjectsRepository;
        $this->usersRepository = $usersRepository;
        $this->usersSkillsRepository = $usersSkillsRepository;
        $this->usersSocialsRepository = $usersSocialsRepository;
        $this->usersSoftSkillsRepository = $usersSoftSkillsRepository;
        $this->applicationsRepository = $applicationsRepository;
    }

    /**
     * Get all requested data for the cv of users application.
     * @param int|null $id
     * @param string $lang
     * @return array
     */
    public function getCV($id = null, string $lang)
    {
        $skills = $this->usersSkillsRepository->getSkillsByUserID($id, $lang);
        $educations = $this->usersEducationsRepository->getEducationsByUserID($id, $lang);
        $furthers = $this->usersEducationsRepository->getFurtherEducationsByUserID($id, $lang);
        $autodidactics = $this->usersProjectsRepository->getAutodidacticActivitiesByUserID($id, $lang);
        $engagements = $this->usersProjectsRepository->getEngagementsByUserID($id, $lang);
        $langs = $this->usersLanguagesRepository->getLanguagesByUserID($id, $lang);
        $softs = $this->usersSoftSkillsRepository->getSoftSkillsByUserID($id, $lang);
        $hobbies = $this->usersHobbiesRepository->getHobbiesByUserID($id, $lang);
        $socials = $this->usersSocialsRepository->getSocialMediaByActiveStatus($id);
        $message = $this->usersCVMessagesRepository->getMessageByUserID($id, $lang);

        $data = [
            'experiences' => $this->getExperiencesByUserID($id, $lang),
            'skills' => $skills,
            'educations' => $educations,
            'furthers' => $furthers,
            'autodidactics' => $autodidactics,
            'engagements' => $engagements,
            'langs' => $langs,
            'softs' => $softs,
            'hobbies' => $hobbies,
            'socials' => $socials,
            'message' => $message,
        ];

        return $data;
    }

    /**
     * Get all requested data for the cover page of users application.
     * @param int|null $id
     * @param string $lang
     * @return array
     */
    public function getFrontpage($id = null, string $lang)
    {
        $user = $this->usersRepository->getDataByUserID($id);
        $address = $this->usersAddressPrivateRepository->getAddressByUserID($id);
        $item = $this->usersCVDesignItemsRepository->getItemsByUserID($id);
        $message = $this->usersCVMessagesRepository->getMessageByUserID($id, $lang);

        $data = [
            'user' => $user,
            'address' => $address,
            'item' => $item,
            'message' => $message,
        ];

        return $data;
    }

    /**
     * Handling of users experiences, when he summarized multiple stages.
     * @param int|null $id
     * @param string $lang
     * @return array
     */
    private function getExperiencesByUserID($id = null, string $lang)
    {
        $data = [];
        $child = [];
        $parents = $this->usersExperiencesRepository->getExperiencesByUserID($id, $lang, 0);

        foreach ($parents as $p => $parent) {
            $child[$p] = $this->usersExperiencesRepository->getExperiencesByUserID($id, $lang, $parent['id']);

            $data[$p]['name'] = $parent['name'];
            $data[$p]['url'] = $parent['url'];
            $data[$p]['title'] = $parent['title'];
            $data[$p]['desc'] = $parent['desc'];
            $data[$p]['begin_date'] = $parent['begin_date'];
            $data[$p]['end_date'] = $parent['end_date'];
            $data[$p]['parent'] = $child[$p];
        }

        return $data;
    }

    /**
     * Get all requested data for the application letter of users application.
     * @param int|null $id
     * @param string $company
     * @param string $slug
     * @return array
     */
    public function getLetterByUserID($id = null, string $company, string $slug)
    {
        $datas = $this->applicationsRepository->getApplicationsByCompanyAndSlug($id, $company, $slug);

        $data = [
            'title' => $datas->title,
            'content' => $datas->content,
            'salutation' => $datas->salutation(),
            'companyname' => $datas->company->title,
            'contactname' => ($datas->contact !== null) ? $datas->contact->name : '',
            'companystreet' => $datas->location->street,
            'companyzipcode' => $datas->location->zip_code,
            'companycity' => $datas->location->city,
            'user' => $datas->user->display_name,
        ];

        return $data;
    }
}
