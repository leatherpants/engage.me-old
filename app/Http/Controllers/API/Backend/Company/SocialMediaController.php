<?php

namespace App\Http\Controllers\API\Backend\Company;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\engageme\Companies\Repository\CompaniesSocialsRepository;

class SocialMediaController extends Controller
{
    /**
     * @var CompaniesSocialsRepository
     */
    private $companiesSocialsRepository;

    /**
     * SocialMediaController constructor.
     * @param CompaniesSocialsRepository $companiesSocialsRepository
     */
    public function __construct(CompaniesSocialsRepository $companiesSocialsRepository)
    {
        $this->companiesSocialsRepository = $companiesSocialsRepository;
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function addSocial(Request $request)
    {
        $data = $request->only(['company_id', 'social_provider', 'url']);
        $this->companiesSocialsRepository->addSocial($data);

        return \Response::json([
            'success' => 'Entry was saved',
        ]);
    }

    /**
     * @param int $id
     * @return mixed
     */
    public function getAllEntries($id)
    {
        $data = $this->companiesSocialsRepository->getEntriesByCompanyID($id);

        return \Response::json([
            'data' => $data,
        ]);
    }

    /**
     * @param int $id
     * @return mixed
     */
    public function getEntryByID($id)
    {
        $data = $this->companiesSocialsRepository->getEntryByID($id);

        return \Response::json($data);
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function updateSocial(Request $request)
    {
        $data = $request->only(['company_id', 'social_provider', 'url']);
        $this->companiesSocialsRepository->updateSocial($data);

        return \Response::json([
            'success' => 'Entry was updated',
        ]);
    }
}
