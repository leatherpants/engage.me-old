<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\engageme\News\Repository\NewsRepository;

class NewsController extends Controller
{
    /**
     * @var NewsRepository
     */
    private $newsRepository;

    /**
     * NewsController constructor.
     * @param NewsRepository $newsRepository
     */
    public function __construct(NewsRepository $newsRepository)
    {
        $this->newsRepository = $newsRepository;
    }

    /**
     * Get all news.
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getIndex()
    {
        $stories = $this->newsRepository->getAll();

        return view('news.index')
            ->with('stories', $stories);
    }

    /**
     * @param string $slug
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getStoryBySlug($slug)
    {
        $story = $this->newsRepository->getStoryBySlug($slug);

        if (!$story) {
            abort(404);
        }

        return view('news.story')
            ->with('story', $story);
    }
}
