<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\API\Backend\CurriculumVitaeController;

class CVController extends Controller
{
    /**
     * @var CurriculumVitaeController
     */
    protected $curriculumVitaeController;

    /**
     * CVController constructor.
     * @param CurriculumVitaeController $curriculumVitaeController
     */
    public function __construct(CurriculumVitaeController $curriculumVitaeController)
    {
        $this->curriculumVitaeController = $curriculumVitaeController;
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getAll()
    {
        $id = Auth::id();
        $lang = Auth::user()->lang;

        $front = $this->curriculumVitaeController->getFrontpage($id, $lang);

        $data = $this->curriculumVitaeController->getCV($id, $lang);

        return view('cv.williams.design1.all')
            ->with('front', $front)
            ->with('data', $data);
    }

    /**
     * @param string $company
     * @param string $slug
     * @return mixed
     */
    public function getAllWithLetter($company, $slug)
    {
        $id = Auth::id();
        $lang = Auth::user()->lang;

        $front = $this->curriculumVitaeController->getFrontpage($id, $lang);

        $letter = $this->curriculumVitaeController->getLetterByUserID($id, $company, $slug);

        $data = $this->curriculumVitaeController->getCV($id, $lang);

        return view('cv.williams.design1.all')
            ->with('front', $front)
            ->with('data', $data)
            ->with('letter', $letter);
    }

    /**
     * Get the CV in chosen language of the user.
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getCV()
    {
        $id = Auth::id();
        $lang = Auth::user()->lang;

        $data = $this->curriculumVitaeController->getCV($id, $lang);

        return view('cv.williams.design1.cv')
            ->with('data', $data);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getFrontpage()
    {
        $id = Auth::id();
        $lang = Auth::user()->lang;

        $data = $this->curriculumVitaeController->getFrontpage($id, $lang);

        return view('cv.williams.design1.frontpage')
            ->with('data', $data);
    }

    /**
     * @param string $company
     * @param string $slug
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getLetter($company, $slug)
    {
        $id = Auth::id();

        $data = $this->curriculumVitaeController->getLetterByUserID($id, $company, $slug);

        return view('cv.williams.design1.letter')
            ->with('data', $data);
    }
}
