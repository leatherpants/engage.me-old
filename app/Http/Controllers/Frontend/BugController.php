<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;

class BugController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getIndex()
    {
        return view('bug.index');
    }
}
