<?php

namespace App\Http\Controllers\Frontend\YourCompany;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\engageme\Users\Repository\UsersRepository;
use App\engageme\Companies\Repository\CompaniesRepository;
use App\engageme\Companies\Repository\CompaniesDetailsRepository;
use App\engageme\Companies\Repository\CompaniesEmployeesRepository;

class DashboardController extends Controller
{
    /**
     * @var UsersRepository
     */
    private $usersRepository;
    /**
     * @var CompaniesDetailsRepository
     */
    private $companiesDetailsRepository;

    /**
     * @var CompaniesRepository
     */
    private $companiesRepository;

    /**
     * @var CompaniesEmployeesRepository
     */
    private $companiesEmployeesRepository;

    /**
     * DashboardController constructor.
     * @param UsersRepository $usersRepository
     * @param CompaniesDetailsRepository $companiesDetailsRepository
     * @param CompaniesRepository $companiesRepository
     * @param CompaniesEmployeesRepository $companiesEmployeesRepository
     */
    public function __construct(UsersRepository $usersRepository,
                                CompaniesDetailsRepository $companiesDetailsRepository,
                                CompaniesRepository $companiesRepository,
                                CompaniesEmployeesRepository $companiesEmployeesRepository)
    {
        $this->usersRepository = $usersRepository;
        $this->companiesDetailsRepository = $companiesDetailsRepository;
        $this->companiesRepository = $companiesRepository;
        $this->companiesEmployeesRepository = $companiesEmployeesRepository;
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getIndex()
    {
        $id = Auth::id();

        $user = $this->usersRepository->getDataByUserID($id);
        $companies = $this->companiesDetailsRepository->getCompaniesByOwnerID($user->id);

        return view('yourcompany.dashboard')
            ->with('user', $user)
            ->with('companies', $companies);
    }

    /**
     * @param string $slug
     * @return mixed
     */
    public function getCompany($slug)
    {
        $id = Auth::id();

        $user = $this->usersRepository->getDataByUserID($id);
        $companies = $this->companiesDetailsRepository->getCompaniesByOwnerID($user->id);
        $company = $this->companiesRepository->getCompanyBySlug($slug);
        $employeesCount = $this->companiesEmployeesRepository->getCountOfEmployeesByCompanyID($company->id);

        return view('yourcompany.company')
            ->with('user', $user)
            ->with('companies', $companies)
            ->with('company', $company)
            ->with('employeesCount', $employeesCount);
    }

    /**
     * @param string $slug
     * @return mixed
     */
    public function getDetails($slug)
    {
        $id = Auth::id();

        $user = $this->usersRepository->getDataByUserID($id);
        $companies = $this->companiesDetailsRepository->getCompaniesByOwnerID($user->id);
        $company = $this->companiesRepository->getCompanyBySlug($slug);
        $details = $this->companiesDetailsRepository->getDetailsByID($company->id);

        return view('yourcompany.details')
            ->with('user', $user)
            ->with('companies', $companies)
            ->with('company', $company)
            ->with('details', $details);
    }

    /**
     * @param string $slug
     * @return mixed
     */
    public function getEmployees($slug)
    {
        $id = Auth::id();

        $user = $this->usersRepository->getDataByUserID($id);
        $companies = $this->companiesDetailsRepository->getCompaniesByOwnerID($user->id);
        $company = $this->companiesRepository->getCompanyBySlug($slug);
        $employees = $this->companiesEmployeesRepository->getEmployeesByCompanyID($company->id);

        return view('yourcompany.employees')
            ->with('user', $user)
            ->with('companies', $companies)
            ->with('company', $company)
            ->with('employees', $employees);
    }
}
