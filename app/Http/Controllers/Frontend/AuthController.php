<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Http\Requests\SignInRequest;
use App\Http\Requests\SignUpRequest;
use Illuminate\Support\Facades\Auth;
use App\engageme\Users\Repository\UsersRepository;
use App\engageme\Users\Repository\UsersPhotosRepository;
use App\engageme\Users\Repository\UsersSettingsRepository;
use App\engageme\Users\Repository\UsersCVDesignItemsRepository;
use App\engageme\Notifications\Repository\NotificationsRepository;

class AuthController extends Controller
{
    /**
     * @var UsersRepository
     */
    private $usersRepository;

    /**
     * @var UsersCVDesignItemsRepository
     */
    private $usersCVDesignItemsRepository;

    /**
     * @var UsersPhotosRepository
     */
    private $usersPhotosRepository;

    /**
     * @var UsersSettingsRepository
     */
    private $usersSettingsRepository;

    /**
     * @var NotificationsRepository
     */
    private $notificationsRepository;

    /**
     * AuthController constructor.
     * @param UsersRepository $usersRepository
     * @param UsersCVDesignItemsRepository $usersCVDesignItemsRepository
     * @param UsersPhotosRepository $usersPhotosRepository
     * @param UsersSettingsRepository $usersSettingsRepository
     * @param NotificationsRepository $notificationsRepository
     */
    public function __construct(UsersRepository $usersRepository,
                                UsersCVDesignItemsRepository $usersCVDesignItemsRepository,
                                UsersPhotosRepository $usersPhotosRepository,
                                UsersSettingsRepository $usersSettingsRepository,
                                NotificationsRepository $notificationsRepository)
    {
        $this->usersRepository = $usersRepository;
        $this->usersPhotosRepository = $usersPhotosRepository;
        $this->notificationsRepository = $notificationsRepository;
        $this->usersSettingsRepository = $usersSettingsRepository;
        $this->usersCVDesignItemsRepository = $usersCVDesignItemsRepository;
    }

    /**
     * Get login form.
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getSignIn()
    {
        return view('auth.signin');
    }

    /**
     * @param SignInRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function postSignIn(SignInRequest $request)
    {
        if (Auth::attempt($request->only(['email', 'password'], $request->has('remember')))) {
            return redirect()
                ->route('home');
        } else {
            return redirect()
                ->back()
                ->withErrors([
                    'error' => trans('error.wrong_email_or_password'),
                ]);
        }
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getSignUp()
    {
        return view('auth.signup');
    }

    /**
     * @param SignUpRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function postSignUp(SignUpRequest $request)
    {
        $data = $request->only(['first_name', 'last_name', 'email', 'password']);

        $user = $this->usersRepository->createUser($data);

        $cvitem = [
            'user_id' => $user->id,
        ];
        $this->usersCVDesignItemsRepository->createPhoto($cvitem);

        $photo = [
            'user_id' => $user->id,
            'type' => 'original',
        ];
        $this->usersPhotosRepository->createPhoto($photo);

        $notification = [
            'type' => 'user_created',
            'user_id' => $user->id,
        ];
        $this->notificationsRepository->createNotification($notification);

        $settings = [
            'user_id' => $user->id,
        ];
        $this->usersSettingsRepository->createSettings($settings);

        return redirect()
            ->route('login');
    }

    /**
     * @return \Illuminate\Http\RedirectResponse
     */
    public function getSignOut()
    {
        Auth::logout();

        return redirect()
            ->route('login');
    }
}
