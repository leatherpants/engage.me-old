<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\engageme\Users\Repository\UsersRepository;

class SearchController extends Controller
{
    /**
     * @var UsersRepository
     */
    private $usersRepository;

    /**
     * SearchController constructor.
     * @param UsersRepository $usersRepository
     */
    public function __construct(UsersRepository $usersRepository)
    {
        $this->usersRepository = $usersRepository;
    }

    /**
     * Search index, if you would not search about the header.
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getIndex()
    {
        return view('search.index');
    }

    /**
     * Get results of search.
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getResults(Request $request)
    {
        $id = auth()->id();
        $query = $request->input('q');

        $results = $this->usersRepository->searchableUser($id, $query);

        return view('search.results')
            ->with('results', $results);
    }
}
