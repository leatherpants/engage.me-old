<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\engageme\Companies\Repository\CompaniesRepository;
use App\engageme\Applications\Repository\ApplicationsRepository;
use App\engageme\Companies\Repository\CompaniesContactsRepository;
use App\engageme\Companies\Repository\CompaniesLocationsRepository;

class ApplicationsController extends Controller
{
    /**
     * @var ApplicationsRepository
     */
    protected $applicationsRepository;

    /**
     * @var CompaniesRepository
     */
    protected $companiesRepository;

    /**
     * @var CompaniesLocationsRepository
     */
    protected $companiesLocationsRepository;

    /**
     * @var CompaniesContactsRepository
     */
    protected $companiesContactsRepository;

    /**
     * ApplicationsController constructor.
     * @param ApplicationsRepository $applicationsRepository
     * @param CompaniesRepository $companiesRepository
     * @param CompaniesLocationsRepository $companiesLocationsRepository
     * @param CompaniesContactsRepository $companiesContactsRepository
     */
    public function __construct(ApplicationsRepository $applicationsRepository,
                                CompaniesRepository $companiesRepository,
                                CompaniesLocationsRepository $companiesLocationsRepository,
                                CompaniesContactsRepository $companiesContactsRepository)
    {
        $this->applicationsRepository = $applicationsRepository;
        $this->companiesRepository = $companiesRepository;
        $this->companiesLocationsRepository = $companiesLocationsRepository;
        $this->companiesContactsRepository = $companiesContactsRepository;
    }

    /**
     * Get all applications.
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getIndex()
    {
        $user = auth()->user();
        $user_id = auth()->id();
        $lang = $user->lang;

        $apps = $this->applicationsRepository->getAllByUserIDLang($user_id, $lang);

        return view('applications.index')
            ->with('apps', $apps);
    }

    /**
     * @return mixed
     */
    public function getWrite()
    {
        $companies = $this->companiesRepository->getAll();
        $locations = $this->companiesLocationsRepository->getAll();
        $contacts = $this->companiesContactsRepository->getAll();

        return view('applications.write')
            ->with('companies', $companies)
            ->with('locations', $locations)
            ->with('contacts', $contacts);
    }

    /**
     * @param string $company
     * @param string $slug
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getView($company, $slug)
    {
        $id = auth()->id();

        $app = $this->applicationsRepository->getApplicationsByCompanyAndSlug($id, $company, $slug);

        return view('applications.view')
            ->with('app', $app);
    }
}
