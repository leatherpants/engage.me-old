<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\CreateSupportRequest;
use App\engageme\Users\Repository\UsersRepository;
use App\engageme\Support\Repository\SupportTicketsRepository;

class SupportController extends Controller
{
    /**
     * @var UsersRepository
     */
    private $usersRepository;
    /**
     * @var SupportTicketsRepository
     */
    private $supportTicketsRepository;

    /**
     * SupportController constructor.
     * @param UsersRepository $usersRepository
     * @param SupportTicketsRepository $supportTicketsRepository
     */
    public function __construct(UsersRepository $usersRepository,
                                SupportTicketsRepository $supportTicketsRepository)
    {
        $this->usersRepository = $usersRepository;
        $this->supportTicketsRepository = $supportTicketsRepository;
    }

    /**
     * @return mixed
     */
    public function getIndex()
    {
        $user_id = Auth::id();

        $user = $this->usersRepository->getDataByUserID($user_id);

        $tickets = $this->supportTicketsRepository->getTicketsByUserID($user->id);
        $countOfTickets = $this->supportTicketsRepository->getCountOfTicketsByUserID($user->id);

        return view('support.index')
            ->with('user', $user)
            ->with('countOfTickets', $countOfTickets)
            ->with('tickets', $tickets);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function addRequest()
    {
        return view('support.create');
    }

    /**
     * @param CreateSupportRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function createRequest(CreateSupportRequest $request)
    {
        $data = [
            'user_id' => Auth::id(),
            'title' => $request->title,
            'content' => $request->input('content'),
        ];

        $this->supportTicketsRepository->createRequest($data);

        return redirect()
            ->route('support');
    }

    /**
     * @param int $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getTicket(int $id)
    {
        $ticket = $this->supportTicketsRepository->getTicketByID($id);

        return view('support.ticket')
            ->with('ticket', $ticket);
    }
}
