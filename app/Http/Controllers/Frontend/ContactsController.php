<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\engageme\Users\Repository\UsersRepository;
use App\engageme\Users\Repository\UsersContactsRepository;

class ContactsController extends Controller
{
    /**
     * @var UsersContactsRepository
     */
    protected $usersContactsRepository;

    /**
     * @var UsersRepository
     */
    protected $usersRepository;

    /**
     * ContactsController constructor.
     * @param UsersContactsRepository $usersContactsRepository
     * @param UsersRepository $usersRepository
     */
    public function __construct(UsersContactsRepository $usersContactsRepository,
                                UsersRepository $usersRepository)
    {
        $this->usersContactsRepository = $usersContactsRepository;
        $this->usersRepository = $usersRepository;
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getIndex()
    {
        $user = Auth::user();

        return view('contacts.index')
            ->with('user', $user);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function sentRequests()
    {
        $user = Auth::user();

        return view('contacts.sent')
            ->with('user', $user);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function receivedRequests()
    {
        $user = Auth::user();

        return view('contacts.received')
            ->with('user', $user);
    }

    /**
     * @param int $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function acceptContact($id)
    {
        $this->usersRepository->getDataByUserID($id);
        $this->usersContactsRepository->updateRequest(Auth::id(), $id);

        return redirect()
            ->route('contacts.received');
    }

    /**
     * @param int $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function addContact($id)
    {
        $user = $this->usersRepository->getDataByUserID($id);
        $this->usersContactsRepository->checkFriendship($user, $id);

        $this->usersContactsRepository->addRequest(Auth::id(), $id);

        return redirect()
            ->route('contacts.sent');
    }

    /**
     * @param int $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function deleteContact(int $id)
    {
        $user = $this->usersRepository->getDataByUserID($id);
        auth()->user()->deleteFriend($user);

        return redirect()
            ->route('contacts');
    }

    /**
     * @param int $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getFriendship($id)
    {
        $check = $this->usersContactsRepository->checkFriendship(Auth::id(), $id);

        if (Auth::id() == $id) {
            return view('user._partials.buttons.contacts');
        }

        if (!$check) {
            return view('contacts._partials.buttons.add')->with('id', $id);
        }

        if ($check->status == 1) {
            return view('user._partials.buttons.contacts');
        }

        if ($check->sender == $id) {
            return view('contacts._partials.buttons.receiver');
        }

        if ($check->receiver == $id) {
            return view('contacts._partials.buttons.sender');
        }
    }
}
