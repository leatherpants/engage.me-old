<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\engageme\Jobs\Repository\JobsRepository;

class JobsController extends Controller
{
    /**
     * @var JobsRepository
     */
    private $jobsRepository;

    /**
     * JobsController constructor.
     * @param JobsRepository $jobsRepository
     */
    public function __construct(JobsRepository $jobsRepository)
    {
        $this->jobsRepository = $jobsRepository;
    }

    /**
     * Get all jobs.
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getIndex()
    {
        $jobs = $this->jobsRepository->getAll();

        return view('jobs.index')
            ->with('jobs', $jobs);
    }

    /**
     * @param string $company
     * @param string $title
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getJobs($company, $title)
    {
        $job = $this->jobsRepository->getJobByCompanyAndSlug($company, $title);

        if (!$job) {
            abort(404);
        }

        return view('jobs.view')
            ->with('job', $job);
    }
}
