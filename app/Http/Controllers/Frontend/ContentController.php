<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\engageme\Users\Repository\UsersRepository;

class ContentController extends Controller
{
    /**
     * @var UsersRepository
     */
    private $usersRepository;

    /**
     * ContentController constructor.
     * @param UsersRepository $usersRepository
     */
    public function __construct(UsersRepository $usersRepository)
    {
        $this->usersRepository = $usersRepository;
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getAboutUs()
    {
        $users = $this->usersRepository->getCount();

        return view('content.about_us')
            ->with('users', $users);
    }
}
