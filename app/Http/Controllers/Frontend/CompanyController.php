<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\engageme\Jobs\Repository\JobsRepository;
use App\engageme\Companies\Repository\CompaniesRepository;
use App\engageme\Companies\Repository\CompaniesDetailsRepository;
use App\engageme\Companies\Repository\CompaniesSocialsRepository;
use App\engageme\Companies\Repository\CompaniesEmployeesRepository;

class CompanyController extends Controller
{
    /**
     * @var CompaniesDetailsRepository
     */
    protected $companiesDetailsRepository;

    /**
     * @var CompaniesEmployeesRepository
     */
    private $companiesEmployeesRepository;

    /**
     * @var CompaniesRepository
     */
    protected $companiesRepository;

    /**
     * @var CompaniesSocialsRepository
     */
    protected $companiesSocialsRepository;

    /**
     * @var JobsRepository
     */
    private $jobsRepository;

    /**
     * CompanyController constructor.
     * @param CompaniesDetailsRepository $companiesDetailsRepository
     * @param CompaniesEmployeesRepository $companiesEmployeesRepository
     * @param CompaniesRepository $companiesRepository
     * @param CompaniesSocialsRepository $companiesSocialsRepository
     * @param JobsRepository $jobsRepository
     */
    public function __construct(CompaniesDetailsRepository $companiesDetailsRepository,
                                CompaniesEmployeesRepository $companiesEmployeesRepository,
                                CompaniesRepository $companiesRepository,
                                CompaniesSocialsRepository $companiesSocialsRepository,
                                JobsRepository $jobsRepository)
    {
        $this->companiesDetailsRepository = $companiesDetailsRepository;
        $this->companiesRepository = $companiesRepository;
        $this->companiesSocialsRepository = $companiesSocialsRepository;
        $this->jobsRepository = $jobsRepository;
        $this->companiesEmployeesRepository = $companiesEmployeesRepository;
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getIndex()
    {
        $companies = $this->companiesRepository->getAll();

        return view('companies.index')
            ->with('companies', $companies);
    }

    /**
     * @param string $slug
     * @return mixed
     */
    public function getCompany($slug)
    {
        $company = $this->companiesRepository->getCompanyBySlug($slug);

        if (!$company) {
            abort(404);
        }

        $socials = $this->companiesSocialsRepository->getSocialsByID($company->id);
        $detail = $this->companiesDetailsRepository->getDetailsByID($company->id);
        $jobs = $this->jobsRepository->getLimitedJobsbyCompanyID($company->id);

        return view('companies.company')
            ->with('company', $company)
            ->with('detail', $detail)
            ->with('jobs', $jobs)
            ->with('socials', $socials);
    }

    /**
     * @param string $slug
     * @return mixed
     */
    public function getEmployees($slug)
    {
        $company = $this->companiesRepository->getCompanyBySlug($slug);

        if (!$company) {
            abort(404);
        }

        $socials = $this->companiesSocialsRepository->getSocialsByID($company->id);
        $detail = $this->companiesDetailsRepository->getDetailsByID($company->id);
        $employees = $this->companiesEmployeesRepository->getEmployeesByCompanyID($company->id);

        return view('companies.employees')
            ->with('company', $company)
            ->with('detail', $detail)
            ->with('employees', $employees)
            ->with('socials', $socials);
    }
}
