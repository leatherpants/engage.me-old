<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use App\Http\Requests\UpdateUsersAccountRequest;
use App\Http\Requests\UpdateUsersLanguageRequest;
use App\Http\Requests\UpdateUsersPasswordRequest;
use App\engageme\Users\Repository\UsersRepository;
use App\engageme\Languages\Repository\LanguagesRepository;

class SettingsController extends Controller
{
    /**
     * @var LanguagesRepository
     */
    protected $languagesRepository;

    /**
     * @var UsersRepository
     */
    protected $usersRepository;

    /**
     * SettingsController constructor.
     * @param LanguagesRepository $languagesRepository
     * @param UsersRepository $usersRepository
     */
    public function __construct(LanguagesRepository $languagesRepository,
                                UsersRepository $usersRepository)
    {
        $this->languagesRepository = $languagesRepository;
        $this->usersRepository = $usersRepository;
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getIndex()
    {
        return view('settings.index');
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getAccount()
    {
        $id = Auth::id();

        $user = $this->usersRepository->getDataByUserID($id);

        return view('settings.account')
            ->with('user', $user);
    }

    /**
     * @param UpdateUsersAccountRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function postAccount(UpdateUsersAccountRequest $request)
    {
        $id = Auth::id();

        $request = [
            'id' => $id,
            'first_name' => $request->input('first_name'),
            'last_name' => $request->input('last_name'),
            'slug_name' => str_slug($request->first_name.' '.$request->last_name),
            'display_name' => $request->first_name.' '.$request->last_name,
            'gender' => $request->input('gender'),
            'birthday' => ($request->input('birthday') !== null) ? date('Y-m-d', strtotime($request->input('birthday'))) : null,
            'occupation' => $request->input('occupation'),
        ];

        $this->usersRepository->updateUserAccount($request);

        return redirect()
            ->route('settings.account')->with('success', 'Settings were changed!');
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getEmail()
    {
        $id = Auth::id();

        $user = $this->usersRepository->getDataByUserID($id);

        return view('settings.email')
            ->with('user', $user);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getImage()
    {
        $id = Auth::id();

        $user = $this->usersRepository->getDataByUserID($id);

        return view('settings.image')
            ->with('user', $user);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getLanguage()
    {
        $id = Auth::id();

        $user = $this->usersRepository->getDataByUserID($id);
        $langs = $this->languagesRepository->getActiveLanguages();

        return view('settings.language')
            ->with('langs', $langs)
            ->with('user', $user);
    }

    /**
     * @param UpdateUsersLanguageRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function postLanguage(UpdateUsersLanguageRequest $request)
    {
        $id = Auth::id();

        $request = [
            'id' => $id,
            'lang' => $request->input('lang'),
        ];

        $this->usersRepository->updateUserLanguage($request);

        return redirect()
            ->route('settings.language');
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getPassword()
    {
        return view('settings.password');
    }

    /**
     * @param UpdateUsersPasswordRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function postPassword(UpdateUsersPasswordRequest $request)
    {
        $id = Auth::id();

        $user = $this->usersRepository->getDataByUserID($id);

        if (Hash::check($request->input('current_password'), $user['password']) && $request->input('new_password') == $request->input('confirm_password')) {
            $request = [
                'id' => $id,
                'password' => $request->input('new_password'),
            ];

            $this->usersRepository->updateUserPassword($request);

            return redirect()
                ->route('settings.password')
                ->with('success', 'Password changed');
        } else {
            return redirect()
                ->route('settings.password')
                ->with('error', 'Password not changed');
        }
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function getSearch()
    {
        $id = Auth::id();

        $user = $this->usersRepository->getDataByUserID($id);

        /* no access as normal user */
        if (!$user->settings->beta) {
            return redirect()
                ->route('settings');
        }

        return view('settings.search')
            ->with('user', $user);
    }
}
