<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\engageme\Users\Repository\UsersRepository;
use App\engageme\Users\Repository\UsersSocialsRepository;
use App\engageme\Users\Repository\UsersExperiencesRepository;

class UserController extends Controller
{
    /**
     * @var UsersExperiencesRepository
     */
    protected $usersExperiencesRepository;

    /**
     * @var UsersRepository
     */
    protected $usersRepository;

    /**
     * @var UsersSocialsRepository
     */
    protected $usersSocialsRepository;

    /**
     * @var ContactsController
     */
    protected $contactsController;

    /**
     * UserController constructor.
     * @param UsersExperiencesRepository $usersExperiencesRepository
     * @param UsersRepository $usersRepository
     * @param UsersSocialsRepository $usersSocialsRepository
     * @param ContactsController $contactsController
     */
    public function __construct(UsersExperiencesRepository $usersExperiencesRepository,
                                UsersRepository $usersRepository,
                                UsersSocialsRepository $usersSocialsRepository,
                                ContactsController $contactsController)
    {
        $this->usersExperiencesRepository = $usersExperiencesRepository;
        $this->usersRepository = $usersRepository;
        $this->usersSocialsRepository = $usersSocialsRepository;
        $this->contactsController = $contactsController;
    }

    /**
     * @param string $slug
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getProfile(string $slug)
    {
        $user = $this->usersRepository->getDataBySlug($slug);

        if (!$user) {
            abort(404);
        }

        $contact = $this->contactsController->getFriendship($user->id);

        if ($user->id !== Auth::id()) {
            $contact = $this->contactsController->getFriendship($user->id);
            $experiences = $this->usersExperiencesRepository->getAllExperiencesByUserID($user->id, $user->lang);
            $socials = $this->usersSocialsRepository->getSocialsByUserID($user->id);

            return view('user.not_own_profile')
                ->with('contact', $contact)
                ->with('experiences', $experiences)
                ->with('socials', $socials)
                ->with('user', $user);
        }

        return view('user.profile')
            ->with('user', $user)
            ->with('contact', $contact);
    }

    /**
     * @param string $slug
     * @return mixed
     */
    public function getDetails(string $slug)
    {
        $user = $this->usersRepository->getDataBySlug($slug);
        $contact = $this->contactsController->getFriendship($user->id);
        $experiences = $this->usersExperiencesRepository->getAllExperiencesByUserID($user->id, $user->lang);

        return view('user.details')
            ->with('user', $user)
            ->with('contact', $contact)
            ->with('experiences', $experiences);
    }

    /**
     * @param string $slug
     * @return mixed
     */
    public function getProfiles(string $slug)
    {
        $user = $this->usersRepository->getDataBySlug($slug);
        $contact = $this->contactsController->getFriendship($user->id);
        $socials = $this->usersSocialsRepository->getSocialsByUserID($user->id);

        return view('user.profiles')
            ->with('user', $user)
            ->with('contact', $contact)
            ->with('socials', $socials);
    }

    /**
     * @param string $slug
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getStats(string $slug)
    {
        $user = $this->usersRepository->getDataBySlug($slug);
        $contact = $this->contactsController->getFriendship($user->id);

        return view('user.stats')
            ->with('user', $user)
            ->with('contact', $contact);
    }
}
