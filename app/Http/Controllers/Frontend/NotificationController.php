<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\engageme\Notifications\Repository\NotificationsRepository;

class NotificationController extends Controller
{
    /**
     * @var NotificationsRepository
     */
    private $notificationsRepository;

    /**
     * NotificationController constructor.
     * @param NotificationsRepository $notificationsRepository
     */
    public function __construct(NotificationsRepository $notificationsRepository)
    {
        $this->notificationsRepository = $notificationsRepository;
    }

    /**
     * Get overview of user notifications.
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getIndex()
    {
        $id = Auth::id();

        $notifications = $this->notificationsRepository->getNotificationsByUserID($id);

        return view('notifications.index')
            ->with('notifications', $notifications);
    }
}
