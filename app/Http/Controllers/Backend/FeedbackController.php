<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;

class FeedbackController extends Controller
{
    /**
     * Get all feedbacks.
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getIndex()
    {
        return view('backend.feedback.index');
    }
}
