<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\engageme\News\Repository\NewsRepository;

class NewsController extends Controller
{
    /**
     * @var NewsRepository
     */
    private $newsRepository;

    /**
     * NewsController constructor.
     *
     * @param NewsRepository $newsRepository
     */
    public function __construct(NewsRepository $newsRepository)
    {
        $this->newsRepository = $newsRepository;
    }

    /**
     * Get all news.
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getIndex()
    {
        $stories = $this->newsRepository->getAll();

        return view('backend.news.index')
            ->with('stories', $stories);
    }

    /**
     * Write new news.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function addNews()
    {
        return view('backend.news.add');
    }

    /**
     * Edit news.
     * @param int $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function editNews(int $id)
    {
        $story = $this->newsRepository->getStoryByID($id);

        return view('backend.news.edit')
            ->with('story', $story);
    }

    /**
     * @param Request $request
     * @param int $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function updateNews(Request $request, int $id)
    {
        $string = $request->input('content');
        $paragraphCloseOpen = '</p><p>';
        $replacementStrings = [
            "\r\n\r\n",
            "\n\n",
            "\r\n",
            "\n",
        ];
        foreach ($replacementStrings as $replacementString) {
            $string = str_replace($replacementString, $paragraphCloseOpen, $string);
        }

        $content = str_replace('<p></p>', '', $string);

        $content = '<p>'.$content.'</p>';

        $data = [
            'id'      => $id,
            'title'   => $request->input('title'),
            'slug'    => str_slug($request->title),
            'content' => $content,
        ];

        $this->newsRepository->updateStory($data);

        return redirect()
            ->route('backend.news');
    }
}
