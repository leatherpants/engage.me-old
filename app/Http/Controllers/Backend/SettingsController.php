<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;

class SettingsController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getIndex()
    {
        return view('backend.settings.index');
    }
}
