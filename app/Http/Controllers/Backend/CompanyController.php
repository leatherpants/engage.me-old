<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\engageme\Jobs\Repository\JobsRepository;
use App\engageme\Companies\Repository\CompaniesRepository;
use App\engageme\Countries\Repository\CountriesRepository;
use App\engageme\Companies\Repository\CompaniesSocialsRepository;
use App\engageme\Companies\Repository\CompaniesLocationsRepository;

class CompanyController extends Controller
{
    /**
     * @var CompaniesRepository
     */
    protected $companiesRepository;

    /**
     * @var CompaniesLocationsRepository
     */
    private $companiesLocationsRepository;

    /**
     * @var CompaniesSocialsRepository
     */
    private $companiesSocialsRepository;

    /**
     * @var CountriesRepository
     */
    private $countriesRepository;

    /**
     * @var JobsRepository
     */
    private $jobsRepository;

    /**
     * CompanyController constructor.
     * @param CompaniesRepository $companiesRepository
     * @param CompaniesLocationsRepository $companiesLocationsRepository
     * @param CompaniesSocialsRepository $companiesSocialsRepository
     * @param CountriesRepository $countriesRepository
     * @param JobsRepository $jobsRepository
     */
    public function __construct(CompaniesRepository $companiesRepository,
                                CompaniesLocationsRepository $companiesLocationsRepository,
                                CompaniesSocialsRepository $companiesSocialsRepository,
                                CountriesRepository $countriesRepository,
                                JobsRepository $jobsRepository)
    {
        $this->companiesRepository = $companiesRepository;
        $this->companiesLocationsRepository = $companiesLocationsRepository;
        $this->companiesSocialsRepository = $companiesSocialsRepository;
        $this->countriesRepository = $countriesRepository;
        $this->jobsRepository = $jobsRepository;
    }

    /**
     * Get overview of companies.
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getIndex()
    {
        $companies = $this->companiesRepository->getAll();

        return view('backend.companies.index')
            ->with('companies', $companies);
    }

    /**
     * Get company.
     * @param string $slug
     * @return mixed
     */
    public function getCompany(string $slug)
    {
        $company = $this->companiesRepository->getCompanyBySlug($slug);
        $location = $this->companiesLocationsRepository->getCompanyLocationByID($company->id);
        $socials = $this->companiesSocialsRepository->getSocialsByID($company->id);
        $jobs = $this->jobsRepository->getLimitedJobsbyCompanyID($company->id);

        return view('backend.companies.company')
            ->with('company', $company)
            ->with('jobs', $jobs)
            ->with('location', $location)
            ->with('socials', $socials);
    }

    /**
     * Get job offers of company.
     * @param string $slug
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getJobs(string $slug)
    {
        $company = $this->companiesRepository->getCompanyBySlug($slug);
        $jobs = $this->jobsRepository->getJobsByCompanyID($company->id);

        return view('backend.companies.jobs')
            ->with('company', $company)
            ->with('jobs', $jobs);
    }

    /**
     * Get location of company.
     * @param string $slug
     * @return mixed
     */
    public function getLocation(string $slug)
    {
        $company = $this->companiesRepository->getCompanyBySlug($slug);
        $location = $this->companiesLocationsRepository->getCompanyLocationByID($company->id);
        $countries = $this->countriesRepository->getAll();

        return view('backend.companies.location')
            ->with('company', $company)
            ->with('countries', $countries)
            ->with('location', $location);
    }
}
