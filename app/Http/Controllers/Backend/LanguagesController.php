<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\engageme\Languages\Repository\LanguagesRepository;

class LanguagesController extends Controller
{
    /**
     * @var LanguagesRepository
     */
    private $languagesRepository;

    /**
     * LanguagesController constructor.
     * @param LanguagesRepository $languagesRepository
     */
    public function __construct(LanguagesRepository $languagesRepository)
    {
        $this->languagesRepository = $languagesRepository;
    }

    /**
     * Get all languages.
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getIndex()
    {
        $languages = $this->languagesRepository->getAll();

        return view('backend.languages.index')
            ->with('languages', $languages);
    }

    /**
     * Update if language is seenable or not for users.
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function updateToggle(Request $request)
    {
        if ($request->input('active') == 1) {
            $active = 0;
        } else {
            $active = 1;
        }

        $data = [
            'code' => $request->input('code'),
            'active' => $active,
        ];

        $this->languagesRepository->updateToggle($data);

        return redirect()
            ->route('backend.languages');
    }
}
