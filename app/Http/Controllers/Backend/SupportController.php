<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\engageme\Support\Repository\SupportTicketsRepository;

class SupportController extends Controller
{
    /**
     * @var SupportTicketsRepository
     */
    private $supportTicketsRepository;

    /**
     * SupportController constructor.
     * @param SupportTicketsRepository $supportTicketsRepository
     */
    public function __construct(SupportTicketsRepository $supportTicketsRepository)
    {
        $this->supportTicketsRepository = $supportTicketsRepository;
    }

    /**
     * Get requests overview.
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getIndex()
    {
        $tickets = $this->supportTicketsRepository->getAll();

        return view('backend.support.index')
            ->with('tickets', $tickets);
    }

    /**
     * Get assigned requests.
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getAssignedTickets()
    {
        $user_id = auth()->id();

        $tickets = $this->supportTicketsRepository->getAssignedTicketsByUserID($user_id);

        return view('backend.support.assigned')
            ->with('tickets', $tickets);
    }

    /**
     * Get closed requests.
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getClosedTickets()
    {
        $tickets = $this->supportTicketsRepository->getClosedTickets();

        return view('backend.support.closed')
            ->with('tickets', $tickets);
    }

    /**
     * @param int $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function assignToMe($id)
    {
        $user_id = Auth::id();

        $data = [
            'id' => $id,
            'assignee' => $user_id,
        ];

        $this->supportTicketsRepository->assignToMe($data);

        return redirect()
            ->route('backend.support');
    }

    /**
     * Get ticket by ID.
     * @param int $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getTicket(int $id)
    {
        $ticket = $this->supportTicketsRepository->getTicketByID($id);

        return view('backend.support.ticket')
            ->with('ticket', $ticket);
    }
}
