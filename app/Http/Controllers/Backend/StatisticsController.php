<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\engageme\Users\Repository\UsersRepository;

class StatisticsController extends Controller
{
    /**
     * @var UsersRepository
     */
    private $usersRepository;

    /**
     * StatisticsController constructor.
     * @param UsersRepository $usersRepository
     */
    public function __construct(UsersRepository $usersRepository)
    {
        $this->usersRepository = $usersRepository;
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getIndex()
    {
        return view('backend.statistics.index');
    }

    /**
     * @return mixed
     */
    public function getUsers()
    {
        $total = $this->usersRepository->getCount();
        $yearly = $this->usersRepository->getYearlyCount();
        $monthly = $this->usersRepository->getMonthlyCount();
        $daily = $this->usersRepository->getDailyCount();

        return view('backend.statistics.users')
            ->with('total', $total)
            ->with('yearly', $yearly)
            ->with('monthly', $monthly)
            ->with('daily', $daily);
    }
}
