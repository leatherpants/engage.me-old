<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\engageme\Users\Repository\UsersRepository;
use App\engageme\Support\Repository\SupportTicketsRepository;

class UserController extends Controller
{
    /**
     * @var UsersRepository
     */
    private $usersRepository;

    /**
     * @var SupportTicketsRepository
     */
    private $supportRepository;

    /**
     * UserController constructor.
     * @param UsersRepository $usersRepository
     * @param SupportTicketsRepository $supportRepository
     */
    public function __construct(UsersRepository $usersRepository,
                                SupportTicketsRepository $supportRepository)
    {
        $this->usersRepository = $usersRepository;
        $this->supportRepository = $supportRepository;
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getIndex()
    {
        $users = $this->usersRepository->getAll();

        return view('backend.user.index')
            ->with('users', $users);
    }

    /**
     * @param string $slug_name
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getView($slug_name)
    {
        $user = $this->usersRepository->getDataBySlug($slug_name);

        if (!$user) {
            abort(404);
        }

        return view('backend.user.view')
            ->with('user', $user);
    }

    /**
     * @param string $slug_name
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getSettings($slug_name)
    {
        $user = $this->usersRepository->getDataBySlug($slug_name);

        if (!$user) {
            abort(404);
        }

        return view('backend.user.settings')
            ->with('user', $user);
    }

    /**
     * @param string $slug_name
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getSupport($slug_name)
    {
        $user = $this->usersRepository->getDataBySlug($slug_name);

        if (!$user) {
            abort(404);
        }

        $tickets = $this->supportRepository->getTicketsByUserID($user->id);

        return view('backend.user.support')
            ->with('user', $user)
            ->with('tickets', $tickets);
    }
}
