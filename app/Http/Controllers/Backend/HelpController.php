<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;

class HelpController extends Controller
{
    /**
     * Get all help content.
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getIndex()
    {
        return view('backend.help.index');
    }
}
