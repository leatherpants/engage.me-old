<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\engageme\Jobs\Repository\JobsRepository;
use App\engageme\Users\Repository\UsersRepository;
use App\engageme\Companies\Repository\CompaniesRepository;

class HomeController extends Controller
{
    /**
     * @var CompaniesRepository
     */
    protected $companiesRepository;

    /**
     * @var JobsRepository
     */
    private $jobsRepository;

    /**
     * @var UsersRepository
     */
    protected $usersRepository;

    /**
     * HomeController constructor.
     * @param CompaniesRepository $companiesRepository
     * @param JobsRepository $jobsRepository
     * @param UsersRepository $usersRepository
     */
    public function __construct(CompaniesRepository $companiesRepository,
                                JobsRepository $jobsRepository,
                                UsersRepository $usersRepository)
    {
        $this->companiesRepository = $companiesRepository;
        $this->jobsRepository = $jobsRepository;
        $this->usersRepository = $usersRepository;
    }

    /**
     * Get dashboard.
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getIndex()
    {
        $companies = $this->companiesRepository->getLatestCompanies();
        $jobs = $this->jobsRepository->getLatestJobs();
        $users = $this->usersRepository->getLatestUsers();

        $latests = [
            'companies' => $companies,
            'jobs' => $jobs,
            'users' => $users,
        ];

        return view('backend.home')
            ->with('latests', $latests);
    }
}
