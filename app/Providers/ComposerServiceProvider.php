<?php

namespace App\Providers;

use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class ComposerServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        View::composers(
            [
                'App\engageme\Composer\ContactComposer' => [
                    'contacts.index',
                    'contacts.received',
                    'contacts.sent',
                ],
                'App\engageme\Composer\DashboardComposer' => [
                    'backend.home',
                ],
                'App\engageme\Composer\SupportComposer' => [
                    'backend.support.*',
                ],
                'App\engageme\Composer\UserDashboardComposer' => [
                    'home',
                ],
            ]
        );
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
