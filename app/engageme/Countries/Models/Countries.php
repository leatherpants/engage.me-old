<?php

namespace App\engageme\Countries\Models;

use Illuminate\Database\Eloquent\Model;

class Countries extends Model
{
    /**
     * The table associated with the model.
     * @var string
     */
    protected $table = 'countries';

    public $timestamps = false;
}
