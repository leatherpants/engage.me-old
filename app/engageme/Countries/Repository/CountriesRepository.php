<?php

namespace App\engageme\Countries\Repository;

use App\engageme\Countries\Models\Countries;

class CountriesRepository
{
    /**
     * @var Countries
     */
    private $model;

    /**
     * CountriesRepository constructor.
     * @param Countries $countries
     */
    public function __construct(Countries $countries)
    {
        $this->model = $countries;
    }

    /**
     * Get all countries.
     * @return mixed
     */
    public function getAll()
    {
        return $this->model
            ->orderBy('code', 'ASC')
            ->get();
    }
}
