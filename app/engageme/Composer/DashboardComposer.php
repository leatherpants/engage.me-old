<?php

namespace App\engageme\Composer;

use Illuminate\View\View;
use App\engageme\Jobs\Repository\JobsRepository;
use App\engageme\Users\Repository\UsersRepository;
use App\engageme\Companies\Repository\CompaniesRepository;
use App\engageme\Support\Repository\SupportTicketsRepository;
use App\engageme\Applications\Repository\ApplicationsRepository;

class DashboardComposer
{
    /**
     * @var ApplicationsRepository
     */
    private $applicationsRepository;

    /**
     * @var UsersRepository
     */
    protected $usersRepository;

    /**
     * @var JobsRepository
     */
    protected $jobsRepository;

    /**
     * @var CompaniesRepository
     */
    protected $companiesRepository;

    /**
     * @var SupportTicketsRepository
     */
    private $supportRepository;

    /**
     * DashboardComposer constructor.
     * @param ApplicationsRepository $applicationsRepository
     * @param CompaniesRepository $companiesRepository
     * @param JobsRepository $jobsRepository
     * @param UsersRepository $usersRepository
     * @param SupportTicketsRepository $supportRepository
     */
    public function __construct(ApplicationsRepository $applicationsRepository,
                                CompaniesRepository $companiesRepository,
                                JobsRepository $jobsRepository,
                                UsersRepository $usersRepository,
                                SupportTicketsRepository $supportRepository)
    {
        $this->companiesRepository = $companiesRepository;
        $this->jobsRepository = $jobsRepository;
        $this->usersRepository = $usersRepository;
        $this->applicationsRepository = $applicationsRepository;
        $this->supportRepository = $supportRepository;
    }

    /**
     * @param View $view
     */
    public function compose(View $view)
    {
        $view->with('applications', $this->applicationsRepository->getAppCount());
        $view->with('companies', $this->companiesRepository->getAll());
        $view->with('jobs', $this->jobsRepository->getAll());
        $view->with('users', $this->usersRepository->getCount());
        $view->with('support', $this->supportRepository->getCount());
    }
}
