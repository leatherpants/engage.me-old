<?php

namespace App\engageme\Composer;

use Illuminate\View\View;
use Illuminate\Support\Facades\Auth;
use App\engageme\Users\Repository\UsersContactsRepository;

class ContactComposer
{
    /**
     * @var UsersContactsRepository
     */
    protected $usersContactsRepository;

    /**
     * ContactComposer constructor.
     * @param UsersContactsRepository $usersContactsRepository
     */
    public function __construct(UsersContactsRepository $usersContactsRepository)
    {
        $this->usersContactsRepository = $usersContactsRepository;
    }

    public function compose(View $view)
    {
        $id = Auth::id();

        $view->with('amountOfContacts', $this->usersContactsRepository->amountOfContacts($id));
        $view->with('amountOfReceived', $this->usersContactsRepository->amountOfReceived($id));
        $view->with('amountOfSent', $this->usersContactsRepository->amountOfSent($id));
        $view->with('contacts', $this->usersContactsRepository->contacts($id));
        $view->with('requestsTo', $this->usersContactsRepository->requestsFrom($id));
        $view->with('requestsFrom', $this->usersContactsRepository->requestsTo($id));
    }
}
