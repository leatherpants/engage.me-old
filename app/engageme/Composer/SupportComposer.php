<?php

namespace App\engageme\Composer;

use Illuminate\View\View;
use Illuminate\Support\Facades\Auth;
use App\engageme\Support\Repository\SupportTicketsRepository;

class SupportComposer
{
    /**
     * @var SupportTicketsRepository
     */
    private $supportTicketsRepository;

    /**
     * SupportComposer constructor.
     * @param SupportTicketsRepository $supportTicketsRepository
     */
    public function __construct(SupportTicketsRepository $supportTicketsRepository)
    {
        $this->supportTicketsRepository = $supportTicketsRepository;
    }

    /**
     * @param View $view
     */
    public function compose(View $view)
    {
        $id = (Auth::check()) ? Auth::id() : null;

        $view->with('requestsCount', $this->supportTicketsRepository->getCount());
        $view->with('assignedCount', $this->supportTicketsRepository->getAssigneeCount($id));
        $view->with('closedTickets', $this->supportTicketsRepository->closedTicketsCount());
    }
}
