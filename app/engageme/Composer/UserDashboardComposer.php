<?php

namespace App\engageme\Composer;

use Illuminate\View\View;
use Illuminate\Support\Facades\Auth;
use App\engageme\Users\Repository\UsersContactsRepository;
use App\engageme\Applications\Repository\ApplicationsRepository;

class UserDashboardComposer
{
    /**
     * @var ApplicationsRepository
     */
    protected $applicationsRepository;

    /**
     * @var UsersContactsRepository
     */
    protected $usersContactsRepository;

    /**
     * UserDashboardComposer constructor.
     * @param ApplicationsRepository $applicationsRepository
     * @param UsersContactsRepository $usersContactsRepository
     */
    public function __construct(ApplicationsRepository $applicationsRepository,
                                UsersContactsRepository $usersContactsRepository)
    {
        $this->applicationsRepository = $applicationsRepository;
        $this->usersContactsRepository = $usersContactsRepository;
    }

    public function compose(View $view)
    {
        $id = (Auth::check()) ? Auth::id() : null;

        $view->with('applications', $this->applicationsRepository->getAppCountByUserID($id));
        $view->with('contacts', $this->usersContactsRepository->amountOfContacts($id));
        $view->with('request', $this->usersContactsRepository->amountOfReceived($id));
    }
}
