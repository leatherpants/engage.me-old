<?php

namespace App\engageme\Events\Models;

use App\engageme\Users\Models\Users;
use Illuminate\Database\Eloquent\Model;

class Events extends Model
{
    public function users()
    {
        return $this->belongsTo(Users::class, 'id');
    }
}
