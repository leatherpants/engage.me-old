<?php

namespace App\engageme\Notifications\Repository;

use App\engageme\Notifications\Models\Notifications;

class NotificationsRepository
{
    /**
     * @var Notifications
     */
    private $model;

    /**
     * NotificationsRepository constructor.
     * @param Notifications $notifications
     */
    public function __construct(Notifications $notifications)
    {
        $this->model = $notifications;
    }

    /**
     * Create new notifications.
     * @param array $request
     */
    public function createNotification(array $request)
    {
        $noti = new Notifications();
        $noti->type = $request['type'];
        $noti->user_id = $request['user_id'];
        $noti->read = 0;
        $noti->save();
    }

    /**
     * Get all notification by user id.
     * @param int|null $id
     * @return mixed
     */
    public function getNotificationsByUserID($id = null)
    {
        return $this->model
            ->where('user_id', $id)
            ->groupBy('user_id')
            ->get();
    }
}
