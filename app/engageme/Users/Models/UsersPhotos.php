<?php

namespace App\engageme\Users\Models;

use Illuminate\Database\Eloquent\Model;

class UsersPhotos extends Model
{
    /**
     * The table associated with the model.
     * @var string
     */
    protected $table = 'users_photos';
}
