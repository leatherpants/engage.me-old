<?php

namespace App\engageme\Users\Models;

use Illuminate\Database\Eloquent\Model;

class UsersSocials extends Model
{
    /**
     * The table associated with the model.
     * @var string
     */
    protected $table = 'users_socials';

    /**
     * The attributes that are mass assignable.
     * @var array
     */
    protected $fillable = [
        'email',
        'password',
        'slug_name',
        'display_name',
        'first_name',
        'last_name',
        'gender',
        'lang',
        'birthday',
    ];

    /**
     * The attributes that should be hidden for arrays.
     * @var array
     */
    protected $hidden = [
    ];

    /**
     * The attributes that should be cast to native types.
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
    ];

    /**
     * The attributes that should be mutated to dates.
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at',
    ];

    /**
     * Indicates if the model should be timestamped.
     * @var bool
     */
    public $timestamps = false;
}
