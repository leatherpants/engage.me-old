<?php

namespace App\engageme\Users\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\engageme\Users\Models\UsersContacts.
 *
 * @property int $id
 * @property int $sender
 * @property int $receiver
 * @property int $status
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @mixin \Eloquent
 */
class UsersContacts extends Model
{
    protected $model = 'users_contacts';

    protected $guarded = ['id'];

    protected $fillable = [
        'sender',
        'receiver',
        'status',
    ];

    public function scopeWhereSender($query, $model)
    {
        return $query->where('sender', $model->getKey());
    }

    public function scopeWhereRecipient($query, $model)
    {
        return $query->where('receiver', $model->getKey());
    }

    public function scopeAccepted($query, $val)
    {
        return $query->where('status', $val);
    }

    public function scopeBetweenModels($query, $sender, $recipient)
    {
        $query->where(function($queryIn) use ($sender, $recipient) {
            $queryIn->where(function($q) use ($sender, $recipient) {
                $q->whereSender($sender)->whereRecipient($recipient);
            })->orWhere(function($q) use ($sender, $recipient) {
                $q->whereSender($recipient)->whereRecipient($sender);
            });
        });
    }
}
