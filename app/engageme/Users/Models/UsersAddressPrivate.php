<?php

namespace App\engageme\Users\Models;

use Illuminate\Database\Eloquent\Model;
use App\engageme\Countries\Models\Countries;

class UsersAddressPrivate extends Model
{
    protected $table = 'users_address_privates';

    protected $hidden = [
        'created_at',
        'updated_at',
    ];

    public function countryCode()
    {
        return $this->hasOne(Countries::class, 'code', 'country');
    }
}
