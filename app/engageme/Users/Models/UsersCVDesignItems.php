<?php

namespace App\engageme\Users\Models;

use Illuminate\Database\Eloquent\Model;

class UsersCVDesignItems extends Model
{
    protected $table = 'users_cv_design_items';

    protected $hidden = [
        'created_at',
        'updated_at',
    ];
}
