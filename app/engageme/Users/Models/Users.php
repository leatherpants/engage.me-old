<?php

namespace App\engageme\Users\Models;

use App\engageme\Events\Models\Events;
use App\engageme\Support\Models\SupportTickets;
use App\engageme\Companies\Models\CompaniesDetails;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Users extends Authenticatable
{
    /**
     * The table associated with the model.
     * @var string
     */
    protected $table = 'users';

    /**
     * The attributes that are mass assignable.
     * @var array
     */
    protected $fillable = [
        'email',
        'password',
        'slug_name',
        'display_name',
        'first_name',
        'last_name',
        'gender',
        'lang',
        'birthday',
        'occupation',
        'type',
        'verified',
        'isAdmin',
        'isActive',
    ];

    /**
     * The attributes that should be hidden for arrays.
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
        'updated_at',
    ];

    /**
     * The attributes that should be cast to native types.
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
    ];

    /**
     * The attributes that should be mutated to dates.
     * @var array
     */
    protected $dates = [
        'birthday',
        'created_at',
        'updated_at',
    ];

    /**
     * Indicates if the model should be timestamped.
     * @var bool
     */
    public $timestamps = true;

    public function events()
    {
        return $this->hasMany(Events::class, 'user_id')->orderBy('created_at', 'DESC');
    }

    public function photo()
    {
        return $this->hasOne(UsersPhotos::class, 'user_id', 'id');
    }

    public function companyOwner()
    {
        return $this->hasOne(CompaniesDetails::class, 'owner_id', 'id');
    }

    public function address()
    {
        return $this->hasOne(UsersAddressPrivate::class, 'user_id', 'id');
    }

    public function settings()
    {
        return $this->hasOne(UsersSettings::class, 'user_id', 'id');
    }

    public function items()
    {
        return $this->hasOne(UsersCVDesignItems::class, 'user_id', 'id');
    }

    /**
     * @return mixed|string
     */
    public function variousDisplayName()
    {
        if ($this->verified) {
            return $this->getDisplayNameVerification();
        } elseif (!$this->isActive) {
            return $this->getDisplayNameBlocked();
        } else {
            return $this->getDisplayName();
        }
    }

    /**
     * @return string
     */
    public function getDisplayName()
    {
        return $this->display_name;
    }

    /**
     * @return string
     */
    public function getDisplayNameBlocked()
    {
        return $this->display_name.' '.$this->isBlocked();
    }

    /**
     * @return string
     */
    public function getDisplayNameVerification()
    {
        return $this->display_name.' '.$this->verification();
    }

    /**
     * @return false|string
     */
    protected function verification()
    {
        if ($this->verified) {
            return '<i class="check circle icon"></i>';
        }

        return false;
    }

    /**
     * @return false|string
     */
    public function isBlocked()
    {
        if (!$this->isActive) {
            return '<i class="lock icon"></i>';
        }

        return false;
    }

    /**
     * @return bool|string
     */
    public function userBirthday()
    {
        if (!$this->birthday) {
            return false;
        }

        return $this->birthday->format('d.F Y');
    }

    /**
     * @return string
     */
    public function userImage()
    {
        if (empty($this->photo->url)) {
            return 'http://media.engageme.granath/images/users/sandra-granath.jpg';
        }

        return $this->photo->url;
    }

    /**
     * @return array|\Illuminate\Contracts\Translation\Translator|null|string
     */
    public function userLocation()
    {
        if ($this->address !== null) {
            return $this->address->city.', '.$this->address->countryCode->name;
        } else {
            return trans('common.unknown');
        }
    }

    /**
     * @return string
     */
    public function isBetaUser()
    {
        if ($this->settings->beta) {
            return 'beta user';
        } else {
            return 'no beta user';
        }
    }

    public function tickets()
    {
        return $this->hasMany(SupportTickets::class, 'user_id');
    }
}
