<?php

namespace App\engageme\Users\Models;

use Illuminate\Database\Eloquent\Model;

class UsersSettings extends Model
{
    protected $table = 'users_settings';
}
