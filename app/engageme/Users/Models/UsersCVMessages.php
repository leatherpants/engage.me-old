<?php

namespace App\engageme\Users\Models;

use Illuminate\Database\Eloquent\Model;

class UsersCVMessages extends Model
{
    protected $table = 'users_cv_messages';

    protected $hidden = [
        'created_at',
        'updated_at',
    ];
}
