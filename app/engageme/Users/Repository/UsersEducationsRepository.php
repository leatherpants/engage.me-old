<?php

namespace App\engageme\Users\Repository;

use App\engageme\Users\Models\UsersEducations;

class UsersEducationsRepository
{
    /**
     * @var UsersEducations
     */
    protected $model;

    /**
     * UsersEducationsRepository constructor.
     * @param UsersEducations $usersEducations
     */
    public function __construct(UsersEducations $usersEducations)
    {
        $this->model = $usersEducations;
    }

    /**
     * Get all educations by user id.
     * @param int|null $id
     * @param string $lang
     * @return mixed
     */
    public function getEducationsByUserID($id = null, string $lang)
    {
        return $this->model
            ->where('user_id', $id)
            ->where('lang', $lang)
            ->where('education', 0)
            ->get();
    }

    /**
     * Get all further educations by user id.
     * @param int|null $id
     * @param string $lang
     * @return mixed
     */
    public function getFurtherEducationsByUserID($id = null, string $lang)
    {
        return $this->model
            ->where('user_id', $id)
            ->where('lang', $lang)
            ->where('education', 1)
            ->get();
    }
}
