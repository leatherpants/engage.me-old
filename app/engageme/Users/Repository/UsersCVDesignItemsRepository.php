<?php

namespace App\engageme\Users\Repository;

use App\engageme\Users\Models\UsersCVDesignItems;

class UsersCVDesignItemsRepository
{
    /**
     * @var UsersCVDesignItems
     */
    protected $model;

    /**
     * UsersCVDesignItemsRepository constructor.
     * @param UsersCVDesignItems $usersCVDesignItems
     */
    public function __construct(UsersCVDesignItems $usersCVDesignItems)
    {
        $this->model = $usersCVDesignItems;
    }

    /**
     * Create cv photo at sign up.
     * @param array $request
     */
    public function createPhoto(array $request)
    {
        $photo = new UsersCVDesignItems();
        $photo->user_id = $request['user_id'];
        $photo->save();
    }

    /**
     * @param int|null $id
     * @return mixed
     */
    public function getItemsByUserID($id = null)
    {
        return $this->model
            ->where('user_id', $id)
            ->first();
    }
}
