<?php

namespace App\engageme\Users\Repository;

use App\engageme\Users\Models\UsersPhotos;

class UsersPhotosRepository
{
    /**
     * @var UsersPhotos
     */
    private $usersPhotos;

    /**
     * UsersPhotosRepository constructor.
     * @param UsersPhotos $usersPhotos
     */
    public function __construct(UsersPhotos $usersPhotos)
    {
        $this->usersPhotos = $usersPhotos;
    }

    /**
     * Create photo at sign up.
     * @param array $request
     */
    public function createPhoto(array $request)
    {
        $photo = new UsersPhotos();
        $photo->user_id = $request['user_id'];
        $photo->type = $request['type'];
        $photo->url = '';
        $photo->save();
    }
}
