<?php

namespace App\engageme\Users\Repository;

use App\engageme\Users\Models\UsersHobbies;

class UsersHobbiesRepository
{
    /**
     * @var UsersHobbies
     */
    protected $model;

    /**
     * UsersHobbiesRepository constructor.
     * @param UsersHobbies $usersHobbies
     */
    public function __construct(UsersHobbies $usersHobbies)
    {
        $this->model = $usersHobbies;
    }

    /**
     * Get hobbies by user id.
     * @param int|null $id
     * @param string $lang
     * @return mixed
     */
    public function getHobbiesByUserID($id = null, string $lang)
    {
        return $this->model
            ->where('user_id', $id)
            ->where('lang', $lang)
            ->get();
    }
}
