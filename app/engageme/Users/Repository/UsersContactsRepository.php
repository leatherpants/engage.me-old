<?php

namespace App\engageme\Users\Repository;

use App\engageme\Users\Models\Users;
use App\engageme\Users\Models\UsersContacts;

class UsersContactsRepository
{
    /**
     * @var UsersContacts
     */
    protected $model;

    /**
     * @var Users
     */
    protected $users;

    /**
     * UsersContactsRepository constructor.
     * @param Users $users
     * @param UsersContacts $usersContacts
     */
    public function __construct(Users $users,
                                UsersContacts $usersContacts)
    {
        $this->model = $usersContacts;
        $this->users = $users;
    }

    public function checkFriendship($sender, $receiver)
    {
        return $this->model
            ->where(function($qIn) use ($sender, $receiver) {
                $qIn->where(function($q) use ($sender, $receiver) {
                    $q->where('sender', $sender);
                    $q->where('receiver', $receiver);
                })->orWhere(function($q) use ($sender, $receiver) {
                    $q->where('sender', $receiver);
                    $q->where('receiver', $sender);
                });
            })
            ->first();
    }

    public function amountOfContacts($id)
    {
        return $this->model
            ->where(function($qIn) use ($id) {
                $qIn->where(function($q) use ($id) {
                    $q->where('sender', $id);
                })->orWhere(function($q) use ($id) {
                    $q->where('receiver', $id);
                });
            })
            ->join('users', 'users.id', '=', 'users_contacts.receiver')
            ->where('users.isActive', 1)
            ->where('status', 1)
            ->count();
    }

    public function amountOfReceived($id)
    {
        $recipients = $this->model
            ->where('receiver', $id)
            ->where('status', 0)
            ->get(['sender'])
            ->toArray();

        return $this->model
            ->whereIn('receiver', $recipients)
            ->count();
    }

    public function amountOfSent($id)
    {
        $senders = $this->model
            ->where('sender', $id)
            ->where('status', 0)
            ->get(['receiver'])
            ->toArray();

        return $this->model
            ->whereIn('id', $senders)
            ->count();
    }

    public function contacts($id)
    {
        $recipients = $this->model
            ->where('sender', $id)
            ->where('status', 1)
            ->get(['receiver'])
            ->toArray();

        $senders = $this->model
            ->where('receiver', $id)
            ->where('status', 1)
            ->get(['sender'])
            ->toArray();

        $friendsIds = array_merge($recipients, $senders);

        return $this->users
            ->whereIn('id', $friendsIds)
            ->where('isActive', 1)
            ->orderBy('last_name', 'ASC')
            ->orderBy('first_name', 'ASC')
            ->get();
    }

    public function requestsTo($id)
    {
        $senders = $this->model
            ->where('receiver', $id)
            ->where('status', 0)
            ->get(['sender'])
            ->toArray();

        return $this->users
            ->whereIn('id', $senders)
            ->get();
    }

    public function requestsFrom($id)
    {
        $recipients = $this->model
            ->where('sender', $id)
            ->where('status', 0)
            ->get(['receiver'])
            ->toArray();

        return $this->users
            ->whereIn('id', $recipients)
            ->get();
    }

    public function addRequest($sender, $receiver)
    {
        $request = new UsersContacts();
        $request->sender = $sender;
        $request->receiver = $receiver;
        $request->status = 0;
        $request->save();
    }

    public function updateRequest($sender, $receiver)
    {
        $request = $this->checkFriendship($sender, $receiver);
        $request->status = 1;
        $request->save();
    }
}
