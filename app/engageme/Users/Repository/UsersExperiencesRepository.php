<?php

namespace App\engageme\Users\Repository;

use App\engageme\Users\Models\UsersExperiences;

class UsersExperiencesRepository
{
    /**
     * @var UsersExperiences
     */
    protected $model;

    /**
     * UsersExperiencesRepository constructor.
     * @param UsersExperiences $usersExperiences
     */
    public function __construct(UsersExperiences $usersExperiences)
    {
        $this->model = $usersExperiences;
    }

    /**
     * Get experiences for CV by user id.
     * @param int|null $id
     * @param string $lang
     * @param int $parent_id
     * @return mixed
     */
    public function getExperiencesByUserID($id = null, string $lang, int $parent_id)
    {
        return $this->model
            ->where('user_id', $id)
            ->where('lang', $lang)
            ->where('parent_id', $parent_id)
            ->orderBy('begin_date', 'DESC')
            ->orderBy('end_date', 'DESC')
            ->get();
    }

    /**
     * Get all experiences by user id.
     * @param int $id
     * @param string $lang
     * @return mixed
     */
    public function getAllExperiencesByUserID(int $id, string $lang)
    {
        return $this->model
            ->where('user_id', $id)
            ->where('lang', $lang)
            ->get();
    }
}
