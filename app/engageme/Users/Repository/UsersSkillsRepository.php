<?php

namespace App\engageme\Users\Repository;

use App\engageme\Users\Models\UsersSkills;

class UsersSkillsRepository
{
    /**
     * @var UsersSkills
     */
    protected $model;

    /**
     * UsersSkillsRepository constructor.
     * @param UsersSkills $usersSkills
     */
    public function __construct(UsersSkills $usersSkills)
    {
        $this->model = $usersSkills;
    }

    /**
     * Get skills by user id and lang.
     * @param int|null $id
     * @param string $lang
     * @return mixed
     */
    public function getSkillsByUserID($id = null, string $lang)
    {
        return $this->model
            ->where('user_id', $id)
            ->where('lang', $lang)
            ->get();
    }
}
