<?php

namespace App\engageme\Users\Repository;

use App\engageme\Users\Models\UsersCVMessages;

class UsersCVMessagesRepository
{
    /**
     * @var UsersCVMessages
     */
    protected $model;

    /**
     * UsersCVMessagesRepository constructor.
     * @param UsersCVMessages $usersCVMessages
     */
    public function __construct(UsersCVMessages $usersCVMessages)
    {
        $this->model = $usersCVMessages;
    }

    /**
     * @param int|null $id
     * @param string $lang
     * @return mixed
     */
    public function getMessageByUserID($id = null, string $lang)
    {
        return $this->model
            ->where('user_id', $id)
            ->where('lang', $lang)
            ->first();
    }
}
