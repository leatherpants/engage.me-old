<?php

namespace App\engageme\Users\Repository;

use Carbon\Carbon;
use App\engageme\Users\Models\Users;

class UsersRepository
{
    /**
     * const perPage.
     */
    const perPage = 25;

    /**
     * const users.
     */
    const users = 4;

    /**
     * @var Users
     */
    protected $model;

    /**
     * UsersRepository constructor.
     * @param Users $users
     */
    public function __construct(Users $users)
    {
        $this->model = $users;
    }

    /**
     * Create user.
     * @param array $request
     * @return Users
     */
    public function createUser(array $request)
    {
        $user = new Users();
        $user->type = 'all';
        $user->first_name = $request['first_name'];
        $user->last_name = $request['last_name'];
        $user->display_name = $request['first_name'].' '.$request['last_name'];
        $user->slug_name = str_slug($user->display_name);
        $user->email = $request['email'];
        $user->password = bcrypt($request['password']);
        $user->save();

        return $user;
    }

    /**
     * Get all user of the system.
     * @return mixed
     */
    public function getAll()
    {
        return $this->model
            ->orderBy('last_name', 'ASC')
            ->orderBy('first_name', 'ASC')
            ->paginate(self::perPage);
    }

    /**
     * Pagination.
     * @param int $perPage
     * @param int $page
     * @return mixed
     */
    public function getAllPaginated(int $perPage, int $page)
    {
        $offset = ($page - 1) * $perPage;

        return $this->model
            ->orderBy('last_name', 'ASC')
            ->orderBy('first_name', 'ASC')
            ->skip($offset)
            ->take($perPage)
            ->get(); /*TODO*/
    }

    /**
     * Get count of users.
     * @return mixed
     */
    public function getCount()
    {
        return $this->model
            ->count();
    }

    /**
     * Get count of daily registered users.
     * @return mixed
     */
    public function getDailyCount()
    {
        return $this->model
            ->where('created_at', '>=', Carbon::today()->toDateTimeString())
            ->count();
    }

    /**
     * Get count of monthly registered users.
     * @return mixed
     */
    public function getMonthlyCount()
    {
        return $this->model
            ->where(\DB::RAW('YEAR(created_at)'), '=', Carbon::today()->format('Y'))
            ->where(\DB::RAW('MONTH(created_at)'), '=', Carbon::today()->format('m'))
            ->count();
    }

    /**
     * Get count of yearly registered users.
     * @return mixed
     */
    public function getYearlyCount()
    {
        return $this->model
            ->where(\DB::RAW('YEAR(created_at)'), '=', Carbon::today()->format('Y'))
            ->count();
    }

    /**
     * Get data by user id.
     * @param int $id
     * @return mixed
     */
    public function getDataByUserID(int $id = null)
    {
        $columns = [
            '*',
            'users.id',
            'users.email',
            'users.created_at',
            'users.updated_at',
        ];

        return $this->model
            ->leftJoin('users_profiles', 'users_profiles.user_id', '=', 'users.id')
            ->leftJoin('users_address_privates', 'users_address_privates.id', '=', 'users.id')
            ->where('users.id', $id)
            ->first($columns);
    }

    /**
     * Get data by slug.
     * @param string $slug
     * @return mixed
     */
    public function getDataBySlug(string $slug)
    {
        $columns = [
            '*',
            'users.id',
            'users.created_at',
            'users.updated_at',
        ];

        return $this->model
            ->leftJoin('users_profiles', 'users_profiles.user_id', '=', 'users.id')
            ->where('users.slug_name', $slug)
            ->first($columns);
    }

    /**
     * Get latest users.
     * @return \Illuminate\Support\Collection
     */
    public function getLatestUsers()
    {
        return $this->model
            ->orderBy('created_at', 'DESC')
            ->take(self::users)
            ->get();
    }

    /**
     * Searching for other user.
     * @param int $id
     * @param string|array|null $user
     * @return mixed
     */
    public function searchableUser(int $id, $user = null)
    {
        return $this->model
            ->where('slug_name', 'LIKE', '%'.$user.'%')
            ->where('isActive', 1)
            ->whereNotIn('id', [$id])
            ->orderBy('last_name', 'ASC')
            ->orderBy('first_name', 'ASC')
            ->get();
    }

    /**
     * Update data of user.
     * @param array $request
     */
    public function updateUserAccount(array $request)
    {
        $user = Users::find($request['id']);

        if ($user) {
            $user->first_name = $request['first_name'];
            $user->last_name = $request['last_name'];
            $user->slug_name = $request['slug_name'];
            $user->display_name = $request['display_name'];
            $user->gender = $request['gender'];
            $user->birthday = $request['birthday'];
            $user->occupation = $request['occupation'];
            $user->save();
        }
    }

    /**
     * Update language of user.
     * @param array $request
     */
    public function updateUserLanguage(array $request)
    {
        $user = Users::find($request['id']);

        if ($user) {
            $user->lang = $request['lang'];
            $user->save();
        }
    }

    /**
     * Update password of user.
     * @param array $request
     */
    public function updateUserPassword(array $request)
    {
        $user = Users::find($request['id']);

        if ($user) {
            $user->password = bcrypt($request['password']);
            $user->save();
        }
    }
}
