<?php

namespace App\engageme\Users\Repository;

use App\engageme\Users\Models\UsersSocials;

class UsersSocialsRepository
{
    /**
     * @var UsersSocials
     */
    protected $model;

    /**
     * UsersSocialsRepository constructor.
     * @param UsersSocials $usersSocials
     */
    public function __construct(UsersSocials $usersSocials)
    {
        $this->model = $usersSocials;
    }

    /**
     * Get social media links, which will be shown in cv.
     * @param int|null $id
     * @return mixed
     */
    public function getSocialMediaByActiveStatus($id = null)
    {
        return $this->model
            ->where('user_id', $id)
            ->where('active', 1)
            ->orderBy('name', 'ASC')
            ->orderBy('user_name', 'ASC')
            ->get();
    }

    /**
     * Get social media links by user id.
     * @param int $id
     * @return mixed
     */
    public function getSocialsByUserID(int $id)
    {
        return $this->model
            ->where('user_id', $id)
            ->orderBy('name', 'ASC')
            ->orderBy('user_name', 'ASC')
            ->get();
    }
}
