<?php

namespace App\engageme\Users\Repository;

use App\engageme\Users\Models\UsersAddressPrivate;

class UsersAddressPrivateRepository
{
    /**
     * @var UsersAddressPrivate
     */
    protected $model;

    /**
     * UsersAddressPrivateRepository constructor.
     * @param UsersAddressPrivate $usersAddressPrivate
     */
    public function __construct(UsersAddressPrivate $usersAddressPrivate)
    {
        $this->model = $usersAddressPrivate;
    }

    /**
     * @param int|null $id
     * @return mixed
     */
    public function getAddressByUserID($id = null)
    {
        return $this->model
            ->where('user_id', $id)
            ->first();
    }
}
