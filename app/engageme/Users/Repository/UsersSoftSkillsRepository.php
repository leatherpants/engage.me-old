<?php

namespace App\engageme\Users\Repository;

use App\engageme\Users\Models\UsersSoftSkills;

class UsersSoftSkillsRepository
{
    /**
     * @var UsersSoftSkills
     */
    protected $model;

    /**
     * UsersSoftSkillsRepository constructor.
     * @param UsersSoftSkills $usersSoftSkills
     */
    public function __construct(UsersSoftSkills $usersSoftSkills)
    {
        $this->model = $usersSoftSkills;
    }

    /**
     * Get soft skills by user id.
     * @param int|null $id
     * @param string $lang
     * @return mixed
     */
    public function getSoftSkillsByUserID($id = null, string $lang)
    {
        return $this->model
            ->where('user_id', $id)
            ->where('lang', $lang)
            ->get();
    }
}
