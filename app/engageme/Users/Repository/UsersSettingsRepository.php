<?php

namespace App\engageme\Users\Repository;

use App\engageme\Users\Models\UsersSettings;

class UsersSettingsRepository
{
    /**
     * @var UsersSettings
     */
    private $model;

    /**
     * UsersSettingsRepository constructor.
     * @param UsersSettings $settings
     */
    public function __construct(UsersSettings $settings)
    {
        $this->model = $settings;
    }

    /**
     * Create user settings at sign up.
     * @param array $request
     */
    public function createSettings(array $request)
    {
        $settings = new UsersSettings();
        $settings->user_id = $request['user_id'];
        $settings->beta = 0;
        $settings->search = 0;
        $settings->save();
    }
}
