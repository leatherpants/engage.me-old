<?php

namespace App\engageme\Users\Repository;

use App\engageme\Users\Models\UsersProjects;

class UsersProjectsRepository
{
    /**
     * @var UsersProjects
     */
    protected $model;

    /**
     * UsersProjectsRepository constructor.
     * @param UsersProjects $usersProjects
     */
    public function __construct(UsersProjects $usersProjects)
    {
        $this->model = $usersProjects;
    }

    /**
     * Get all autodidactic activities by user id.
     * @param int|null $id
     * @param string $lang
     * @return mixed
     */
    public function getAutodidacticActivitiesByUserID($id = null, string $lang)
    {
        return $this->model
            ->where('user_id', $id)
            ->where('lang', $lang)
            ->where('type', 2)
            ->get();
    }

    /**
     * Get all engagements by user id.
     * @param int|null $id
     * @param string $lang
     * @return mixed
     */
    public function getEngagementsByUserID($id = null, string $lang)
    {
        return $this->model
            ->where('user_id', $id)
            ->where('lang', $lang)
            ->where('type', 1)
            ->get();
    }
}
