<?php

namespace App\engageme\Users\Repository;

use App\engageme\Users\Models\UsersLanguages;

class UsersLanguagesRepository
{
    /**
     * @var UsersLanguages
     */
    protected $model;

    /**
     * UsersLanguagesRepository constructor.
     * @param UsersLanguages $usersLanguages
     */
    public function __construct(UsersLanguages $usersLanguages)
    {
        $this->model = $usersLanguages;
    }

    /**
     * Get languages by user id.
     * @param int|null $id
     * @param string $lang
     * @return mixed
     */
    public function getLanguagesByUserID($id = null, string $lang)
    {
        return $this->model
            ->where('user_id', $id)
            ->where('lang', $lang)
            ->where('active', 1)
            ->get();
    }
}
