<?php

namespace App\engageme\Jobs\Repository;

use App\engageme\Jobs\Models\Jobs;

class JobsRepository
{
    /**
     * @const limited Jobs
     */
    const limitedJobs = 5;

    /**
     * @const jobs
     */
    const jobs = 4;

    /**
     * @var Jobs
     */
    protected $model;

    /**
     * JobsRepository constructor.
     * @param Jobs $jobs
     */
    public function __construct(Jobs $jobs)
    {
        $this->model = $jobs;
    }

    /**
     * Get all jobs.
     * @return mixed
     */
    public function getAll()
    {
        return $this->model
            ->get();
    }

    /**
     * Get jobs by company id.
     * @param int $id
     * @return mixed
     */
    public function getJobsByCompanyID(int $id)
    {
        return $this->model
            ->where('company_id', $id)
            ->get();
    }

    /**
     * Get limited jobs by company id.
     * @param int|null $id
     * @return mixed
     */
    public function getLimitedJobsbyCompanyID(int $id = null)
    {
        return $this->model
            ->where('company_id', $id)
            ->take(self::limitedJobs)
            ->get();
    }

    /**
     * Get job details by company and slug.
     * @param string $company
     * @param string $slug
     * @return mixed
     */
    public function getJobByCompanyAndSlug(string $company, string $slug)
    {
        $columns = [
            'companies.title as company_name',
            'companies.slug as company_slug',
            'jobs.company_id',
            'jobs.title',
            'jobs.desc',
            'jobs.created_at',
            'jobs.updated_at',
        ];

        return $this->model
            ->join('companies', 'companies.id', '=', 'jobs.company_id')
            ->where('companies.slug', $company)
            ->where('jobs.slug', $slug)
            ->first($columns);
    }

    /**
     * Get latest jobs.
     * @return \Illuminate\Database\Eloquent\Collection|\Illuminate\Support\Collection|static[]
     */
    public function getLatestJobs()
    {
        return $this->model
            ->orderBy('created_at', 'DESC')
            ->take(self::jobs)
            ->get();
    }
}
