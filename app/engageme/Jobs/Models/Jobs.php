<?php

namespace App\engageme\Jobs\Models;

use Illuminate\Database\Eloquent\Model;
use App\engageme\Companies\Models\Companies;
use App\engageme\Companies\Models\CompaniesLocations;

class Jobs extends Model
{
    /**
     * The table associated with the model.
     * @var string
     */
    protected $table = 'jobs';

    public function company()
    {
        return $this->hasOne(Companies::class, 'id', 'company_id');
    }

    public function location()
    {
        return $this->hasOne(CompaniesLocations::class, 'company_id', 'company_id');
    }
}
