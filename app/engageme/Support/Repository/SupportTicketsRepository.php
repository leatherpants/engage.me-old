<?php

namespace App\engageme\Support\Repository;

use App\engageme\Support\Models\SupportTickets;

class SupportTicketsRepository
{
    /**
     * @var SupportTickets
     */
    private $model;

    /**
     * SupportTicketsRepository constructor.
     * @param SupportTickets $support
     */
    public function __construct(SupportTickets $support)
    {
        $this->model = $support;
    }

    /**
     * Request will assigned to me.
     * @param array $data
     */
    public function assignToMe(array $data)
    {
        $request = SupportTickets::find($data['id']);

        if ($request) {
            $request->assignee = $data['assignee'];
            $request->save();
        }
    }

    /**
     * Get count of closed tickets.
     * @return mixed
     */
    public function closedTicketsCount()
    {
        return $this->model
            ->where('status', SupportTickets::STATUS_CLOSED)
            ->count();
    }

    /**
     * Create support request by user.
     * @param array $data
     */
    public function createRequest(array $data)
    {
        $support = new SupportTickets();
        $support->user_id = $data['user_id'];
        $support->title = $data['title'];
        $support->content = $data['content'];
        $support->save();
    }

    /**
     * Get all support requests.
     * @return mixed
     */
    public function getAll()
    {
        return $this->model
            ->where('assignee')
            ->where('status', SupportTickets::STATUS_NEW)
            ->orWhere('status', SupportTickets::STATUS_OPEN)
            ->orderBy('created_at', 'DESC')
            ->get();
    }

    /**
     * Get count of support requests by assignee.
     * @param int|null $user_id
     * @return mixed
     */
    public function getAssigneeCount(int $user_id = null)
    {
        return $this->model
            ->where('assignee', $user_id)
            ->count();
    }

    /**
     * Get closed tickets.
     * @return mixed
     */
    public function getClosedTickets()
    {
        return $this->model
            ->where('status', SupportTickets::STATUS_CLOSED)
            ->get();
    }

    /**
     * Get count of support requests.
     * @return mixed
     */
    public function getCount()
    {
        return $this->model
            ->where('status', SupportTickets::STATUS_NEW)
            ->orWhere('status', SupportTickets::STATUS_OPEN)
            ->count();
    }

    /**
     * Get support requests by assignee.
     * @param int|null $user_id
     * @return mixed
     */
    public function getAssignedTicketsByUserID($user_id = null)
    {
        return $this->model
            ->where('assignee', $user_id)
            ->orderBy('created_at', 'DESC')
            ->get();
    }

    /**
     * Get ticket by ID.
     * @param int $id
     * @return mixed
     */
    public function getTicketByID(int $id)
    {
        return $this->model
            ->where('id', $id)
            ->first();
    }

    /**
     * Get count of support tickets by user id.
     * @param int $user_id
     * @return mixed
     */
    public function getCountOfTicketsByUserID(int $user_id)
    {
        return $this->model
            ->where('user_id', $user_id)
            ->count();
    }

    /**
     * Get support tickets by user id.
     * @param int $user_id
     * @return mixed
     */
    public function getTicketsByUserID(int $user_id)
    {
        return $this->model
            ->where('user_id', $user_id)
            ->orderBy('created_at', 'DESC')
            ->get();
    }
}
