<?php

namespace App\engageme\Support\Models;

use App\engageme\Users\Models\Users;
use Illuminate\Database\Eloquent\Model;

class SupportTickets extends Model
{
    const STATUS_NEW = 1;
    const STATUS_OPEN = 2;
    const STATUS_PENDING = 3;
    const STATUS_SOLVED = 4;
    const STATUS_CLOSED = 5;

    const PRIORITY_LOW = 1;
    const PRIORITY_NORMAL = 2;
    const PRIORITY_HIGH = 3;

    protected $table = 'support_tickets';

    public function assignedTo()
    {
        return $this->belongsTo(Users::class, 'assignee');
    }

    public function creator()
    {
        return $this->belongsTo(Users::class, 'user_id');
    }
}
