<?php

namespace App\engageme\News\Repository;

use App\engageme\News\Models\News;

class NewsRepository
{
    /**
     * @var News
     */
    private $model;

    /**
     * NewsRepository constructor.
     * @param News $news
     */
    public function __construct(News $news)
    {
        $this->model = $news;
    }

    /**
     * Get all news.
     * @return mixed
     */
    public function getAll()
    {
        return $this->model
            ->orderBy('created_at', 'DESC')
            ->get();
    }

    /**
     * Get story by id.
     * @param int $id
     * @return mixed
     */
    public function getStoryByID(int $id)
    {
        return $this->model
            ->where('id', $id)
            ->first();
    }

    /**
     * Get story by slug.
     * @param string $slug
     * @return mixed
     */
    public function getStoryBySlug(string $slug)
    {
        return $this->model
            ->where('slug', $slug)
            ->first();
    }

    /**
     * Update story.
     * @param array $data
     */
    public function updateStory(array $data)
    {
        $story = News::find($data['id']);

        if ($story) {
            $story->title = $data['title'];
            $story->slug = $data['slug'];
            $story->content = $data['content'];
            $story->save();
        }
    }
}
