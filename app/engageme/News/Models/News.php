<?php

namespace App\engageme\News\Models;

use App\engageme\Users\Models\Users;
use Illuminate\Database\Eloquent\Model;

class News extends Model
{
    protected $table = 'news';

    public $timestamps = true;

    public function user()
    {
        return $this->hasOne(Users::class, 'id', 'user_id');
    }
}
