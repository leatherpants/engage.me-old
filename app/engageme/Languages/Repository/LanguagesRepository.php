<?php

namespace App\engageme\Languages\Repository;

use App\engageme\Languages\Models\Languages;

class LanguagesRepository
{
    /**
     * @var Languages
     */
    protected $model;

    /**
     * LanguagesRepository constructor.
     * @param Languages $languages
     */
    public function __construct(Languages $languages)
    {
        $this->model = $languages;
    }

    /**
     * Get all languages.
     * @return mixed
     */
    public function getAll()
    {
        return $this->model
            ->get();
    }

    /**
     * Get active languages for user settings.
     * @return mixed
     */
    public function getActiveLanguages()
    {
        return $this->model
            ->where('active', 1)
            ->get();
    }

    /**
     * Update if language is seenable or not for users.
     * @param array $request
     */
    public function updateToggle(array $request)
    {
        Languages::where('code', $request['code'])->update(['active' => $request['active']]);
    }
}
