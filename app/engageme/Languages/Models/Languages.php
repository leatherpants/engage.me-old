<?php

namespace App\engageme\Languages\Models;

use Illuminate\Database\Eloquent\Model;

class Languages extends Model
{
    protected $table = 'languages';

    public $guarded = ['code'];

    public $timestamps = false;

    public function getFlag()
    {
        switch ($this->code) {
            case 'da':
                return 'dk';
            case 'en':
                return 'gb';
            default:
                return $this->code;
        }
    }
}
