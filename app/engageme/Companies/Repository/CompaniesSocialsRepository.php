<?php

namespace App\engageme\Companies\Repository;

use App\engageme\Companies\Models\CompaniesSocials;

class CompaniesSocialsRepository
{
    /**
     * @var CompaniesSocials
     */
    protected $model;

    /**
     * CompaniesSocialsRepository constructor.
     * @param CompaniesSocials $companiesSocials
     */
    public function __construct(CompaniesSocials $companiesSocials)
    {
        $this->model = $companiesSocials;
    }

    /**
     * Get all entries by company id.
     * @param int $company_id
     * @return mixed
     */
    public function getEntriesByCompanyID(int $company_id)
    {
        return $this->model
            ->where('company_id', $company_id)
            ->get();
    }

    /**
     * Get single entry by id.
     * @param int $id
     * @return mixed
     */
    public function getEntryByID(int $id)
    {
        return $this->model
            ->where('id', $id)
            ->first();
    }

    /**
     * Get social media entries of the company by id.
     * @param int|null $id
     * @return mixed
     */
    public function getSocialsByID(int $id = null)
    {
        return $this->model
            ->where('company_id', $id)
            ->orderBy('social_provider', 'ASC')
            ->get();
    }

    /**
     * Save new social media entries.
     * @param array $data
     */
    public function addSocial(array $data)
    {
        $social = new CompaniesSocials();
        $social->company_id = trim($data['company_id']);
        $social->social_provider = trim($data['social_provider']);
        $social->url = trim($data['url']);
        $social->save();
    }

    /**
     * Update social media entries.
     * @param array $data
     */
    public function updateSocial(array $data)
    {
        $social = CompaniesSocials::find($data['id']);

        if ($social) {
            $social->company_id = trim($data['company_id']);
            $social->social_provider = trim($data['social_provider']);
            $social->url = trim($data['url']);
            $social->save();
        }
    }
}
