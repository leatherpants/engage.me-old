<?php

namespace App\engageme\Companies\Repository;

use App\engageme\Companies\Models\Companies;

class CompaniesRepository
{
    /**
     * const companies.
     */
    const companies = 4;

    /**
     * @var Companies
     */
    protected $model;

    /**
     * CompaniesRepository constructor.
     * @param Companies $companies
     */
    public function __construct(Companies $companies)
    {
        $this->model = $companies;
    }

    /**
     * Get all companies of the system.
     * @return mixed
     */
    public function getAll()
    {
        return $this->model
            ->leftJoin('companies_locations', 'companies_locations.company_id', '=', 'companies.id')
            ->leftJoin('countries', 'countries.code', '=', 'companies_locations.country')
            ->get(['*', 'companies.created_at']);
    }

    /**
     * Get company by slug.
     * @param string $slug
     * @return mixed
     */
    public function getCompanyBySlug(string $slug)
    {
        return $this->model
            ->leftJoin('companies_locations', 'companies_locations.company_id', '=', 'companies.id')
            ->where('slug', $slug)
            ->first();
    }

    /**
     * Get latest users.
     * @return \Illuminate\Support\Collection
     */
    public function getLatestCompanies()
    {
        return $this->model
            ->orderBy('created_at', 'DESC')
            ->take(self::companies)
            ->get();
    }
}
