<?php

namespace App\engageme\Companies\Repository;

use App\engageme\Companies\Models\CompaniesEmployees;

class CompaniesEmployeesRepository
{
    /**
     * @var CompaniesEmployees
     */
    private $model;

    /**
     * CompaniesEmployeesRepository constructor.
     * @param CompaniesEmployees $companiesEmployees
     */
    public function __construct(CompaniesEmployees $companiesEmployees)
    {
        $this->model = $companiesEmployees;
    }

    /**
     * Get employees by company id.
     * @param int|null $company_id
     * @return mixed
     */
    public function getEmployeesByCompanyID(int $company_id = null)
    {
        return $this->model
            ->where('company_id', $company_id)
            ->get();
    }

    /**
     * Get count of employees by company id.
     * @param int $company_id
     * @return mixed
     */
    public function getCountOfEmployeesByCompanyID(int $company_id)
    {
        return $this->model
            ->where('company_id', $company_id)
            ->count();
    }
}
