<?php

namespace App\engageme\Companies\Repository;

use App\engageme\Companies\Models\CompaniesLocations;

class CompaniesLocationsRepository
{
    /**
     * @var CompaniesLocations
     */
    protected $model;

    /**
     * CompaniesLocationsRepository constructor.
     * @param CompaniesLocations $companiesLocations
     */
    public function __construct(CompaniesLocations $companiesLocations)
    {
        $this->model = $companiesLocations;
    }

    /**
     * Get all locations.
     * @return mixed
     */
    public function getAll()
    {
        return $this->model
            ->get();
    }

    /**
     * Get location of company by ID.
     * @param int $id
     * @return mixed
     */
    public function getCompanyLocationByID(int $id)
    {
        return $this->model
            ->where('company_id', $id)
            ->first();
    }
}
