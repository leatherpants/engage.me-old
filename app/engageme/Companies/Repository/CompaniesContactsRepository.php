<?php

namespace App\engageme\Companies\Repository;

use App\engageme\Companies\Models\CompaniesContacts;

class CompaniesContactsRepository
{
    /**
     * @var CompaniesContacts
     */
    protected $model;

    /**
     * CompaniesContactsRepository constructor.
     * @param CompaniesContacts $companiesContacts
     */
    public function __construct(CompaniesContacts $companiesContacts)
    {
        $this->model = $companiesContacts;
    }

    /**
     * Get all contact persons.
     * @return mixed
     */
    public function getAll()
    {
        return $this->model
            ->get();
    }
}
