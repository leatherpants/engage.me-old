<?php

namespace App\engageme\Companies\Repository;

use App\engageme\Companies\Models\CompaniesDetails;

class CompaniesDetailsRepository
{
    /**
     * @var CompaniesDetails
     */
    protected $model;

    /**
     * CompaniesDetailsRepository constructor.
     * @param CompaniesDetails $companiesDetails
     */
    public function __construct(CompaniesDetails $companiesDetails)
    {
        $this->model = $companiesDetails;
    }

    /**
     * Get details of company by id.
     * @param int $id
     * @return mixed
     */
    public function getDetailsByID(int $id = null)
    {
        return $this->model
            ->where('company_id', $id)
            ->first();
    }

    /**
     * Get companies by owner id.
     * @param int $id
     * @return mixed
     */
    public function getCompaniesByOwnerID(int $id)
    {
        return $this->model
            ->join('companies', 'companies.id', '=', 'companies_details.company_id')
            ->where('owner_id', $id)
            ->orderBy('title', 'ASC')
            ->get(['slug', 'title']);
    }
}
