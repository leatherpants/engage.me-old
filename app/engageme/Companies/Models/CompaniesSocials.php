<?php

namespace App\engageme\Companies\Models;

use Illuminate\Database\Eloquent\Model;

class CompaniesSocials extends Model
{
    const facebook = 1;
    const twitter = 2;
    const instagram = 3;
    const youtube = 4;
    const linkedin = 5;
    const xing = 6;

    /**
     * The table associated with the model.
     * @var string
     */
    protected $table = 'companies_socials';

    /**
     * The attributes that are mass assignable.
     * @var array
     */
    protected $fillable = [
        'company_id',
        'social_provider',
        'url',
    ];

    /**
     * The attributes that should be hidden for arrays.
     * @var array
     */
    protected $hidden = [
        'created_at',
        'updated_at',
    ];

    /**
     * The attributes that should be mutated to dates.
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at',
    ];

    /**
     * Indicates if the model should be timestamped.
     * @var bool
     */
    public $timestamps = true;

    public function youtubeShortner()
    {
        $url = '';

        if (preg_match('/^(http|https):\/\/(?:www\.)?(youtube\.com)(\/channel\/)(?:.+)?/', $this->url)) {
            $url = str_replace('https://www.youtube.com/channel/', '', $this->url);
        } elseif (preg_match('/^(http|https):\/\/(?:www\.)?(youtube\.com)(\/user\/)(?:.+)?/', $this->url)) {
            $url = str_replace('https://www.youtube.com/user/', '', $this->url);
        }

        return $url;
    }

    /**
     * Fetch social media ids to social media provider.
     * @return string
     */
    public function socialProvider()
    {
        switch ($this->social_provider) {
            case self::facebook:
                return 'facebook';
            case self::twitter:
                return 'twitter';
            case self::instagram:
                return 'instagram';
            case self::youtube:
                return 'youtube';
            case self::linkedin:
                return 'linkedin';
            case self::xing:
                return 'xing';
        }
    }
}
