<?php

namespace App\engageme\Companies\Models;

use App\engageme\Users\Models\Users;
use Illuminate\Database\Eloquent\Model;

class CompaniesEmployees extends Model
{
    public function user()
    {
        return $this->belongsTo(Users::class, 'user_id');
    }
}
