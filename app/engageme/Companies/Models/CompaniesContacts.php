<?php

namespace App\engageme\Companies\Models;

use Illuminate\Database\Eloquent\Model;

class CompaniesContacts extends Model
{
    protected $table = 'companies_contacts';

    public function company()
    {
        return $this->hasOne(Companies::class, 'id', 'company_id');
    }
}
