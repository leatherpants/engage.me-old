<?php

namespace App\engageme\Companies\Models;

use Illuminate\Database\Eloquent\Model;
use App\engageme\Countries\Models\Countries;

class Companies extends Model
{
    /**
     * The table associated with the model.
     * @var string
     */
    protected $table = 'companies';

    public function country()
    {
        return $this->hasOne(Countries::class, 'code', 'country');
    }

    public function details()
    {
        return $this->hasOne(CompaniesDetails::class, 'company_id', 'id');
    }
}
