<?php

namespace App\engageme\Companies\Models;

use App\engageme\Users\Models\Users;
use Illuminate\Database\Eloquent\Model;

class CompaniesDetails extends Model
{
    /**
     * The table associated with the model.
     * @var string
     */
    protected $table = 'companies_details';

    public function Company()
    {
        return $this->belongsTo(Companies::class, 'id', 'company_id');
    }

    public function Owner()
    {
        return $this->hasOne(Users::class, 'id', 'owner_id');
    }
}
