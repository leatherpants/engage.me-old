<?php

namespace App\engageme\Companies\Models;

use Illuminate\Database\Eloquent\Model;
use App\engageme\Countries\Models\Countries;

class CompaniesLocations extends Model
{
    protected $table = 'companies_locations';

    public function countries()
    {
        return $this->hasOne(Countries::class, 'code', 'country');
    }

    public function company()
    {
        return $this->hasOne(Companies::class, 'id', 'company_id');
    }
}
