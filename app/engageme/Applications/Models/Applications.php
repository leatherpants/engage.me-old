<?php

namespace App\engageme\Applications\Models;

use App\engageme\Users\Models\Users;
use Illuminate\Database\Eloquent\Model;
use App\engageme\Companies\Models\Companies;
use App\engageme\Companies\Models\CompaniesContacts;
use App\engageme\Companies\Models\CompaniesLocations;

class Applications extends Model
{
    /**
     * The table associated with the model.
     * @var string
     */
    protected $table = 'applications';

    /**
     * The attributes that are mass assignable.
     * @var array
     */
    protected $fillable = [
        'user_id',
        'lang',
        'company_id',
        'location_id',
        'contact_id',
        'type',
        'slug',
        'title',
        'content',
        'status',
    ];

    /**
     * The attributes that should be hidden for arrays.
     * @var array
     */
    protected $hidden = [
        'updated_at',
    ];

    /**
     * The attributes that should be mutated to dates.
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at',
    ];

    /**
     * Indicates if the model should be timestamped.
     * @var bool
     */
    public $timestamps = false;

    /**
     * Get the company record associated with the application.
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function company()
    {
        return $this->hasOne(Companies::class, 'id', 'company_id');
    }

    /**
     * Get the contact record associated with the application.
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function contact()
    {
        return $this->hasOne(CompaniesContacts::class, 'id', 'contact_id');
    }

    /**
     * Get the location record associated with the application.
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function location()
    {
        return $this->hasOne(CompaniesLocations::class, 'id', 'location_id');
    }

    /**
     * Get the user that owns the applications.
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(Users::class);
    }

    /**
     * Choosing the salutation after the available data.
     * @return string
     */
    public function salutation()
    {
        /*TODO:Rewrite that function for an international version of salutations*/
        if ($this->contact !== null) {
            if ($this->contact->gender == 'm') {
                return 'Sehr geehrter Herr '.$this->contact->last_name.',';
            } elseif ($this->contact->gender == 'f') {
                return 'Sehr geehrte Frau '.$this->contact->last_name.',';
            }
        } else {
            return 'Sehr geehrte Damen und Herren,';
        }
    }
}
