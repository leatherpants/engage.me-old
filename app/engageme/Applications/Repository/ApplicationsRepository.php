<?php

namespace App\engageme\Applications\Repository;

use App\engageme\Companies\Models\Companies;
use App\engageme\Applications\Models\Applications;

class ApplicationsRepository
{
    /**
     * @var Applications
     */
    protected $model;
    /**
     * @var Companies
     */
    private $companies;

    /**
     * ApplicationsRepository constructor.
     * @param Applications $applications
     * @param Companies $companies
     */
    public function __construct(Applications $applications,
                                Companies $companies)
    {
        $this->model = $applications;
        $this->companies = $companies;
    }

    /**
     * Get all applications by user id and lang.
     * @param int $user_id
     * @param string $lang
     * @return mixed
     */
    public function getAllByUserIDLang(int $user_id, string $lang)
    {
        return $this->model
            ->where('user_id', $user_id)
            ->where('lang', $lang)
            ->orderBy('created_at', 'DESC')
            ->get();
    }

    /**
     * Get all applications by user id.
     * @param int $user_id
     * @return mixed
     */
    public function getAllByUserID(int $user_id)
    {
        return $this->model
            ->where('user_id', $user_id)
            ->get();
    }

    /**
     * Get count of applications.
     * @return mixed
     */
    public function getAppCount()
    {
        return $this->model
            ->count();
    }

    /**
     * Get count of all applications by user id.
     * @param int|null $user_id
     * @return mixed
     */
    public function getAppCountByUserID(int $user_id = null)
    {
        return $this->model
            ->where('user_id', $user_id)
            ->count();
    }

    /**
     * Get application by company and slug.
     * @param int|null $user_id
     * @param string $company_slug
     * @param string $slug
     * @return mixed
     */
    public function getApplicationsByCompanyAndSlug($user_id = null, string $company_slug, string $slug)
    {
        $company = $this->companies
            ->where('slug', $company_slug)
            ->first();

        return $this->model
            ->where('company_id', $company->id)
            ->where('user_id', $user_id)
            ->where('slug', $slug)
            ->first();
    }
}
