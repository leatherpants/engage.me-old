@extends('templates.frontend')
@section('title', config('app.name'))
@section('content')
    <div class="ui stackable grid">
        <div class="row">
            <div class="column">
                <div class="ui inverted segment">
                    <h3 class="ui orange inverted dividing header">
                        <i class="bug icon"></i>
                        <span class="content">
                            {{ trans('common.bug') }}
                        </span>
                    </h3>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="column">
                <div class="ui inverted segment">
                    <div class="ui inverted divided list">
                        <div class="item">
                            <i class="chrome icon"></i>
                            <div class="content">
                                <a href="#">xxx</a>
                            </div>
                        </div>
                        <div class="item">
                            <i class="firefox icon"></i>
                            <div class="content">
                                <a href="#">xxx</a>
                            </div>
                        </div>
                        <div class="item">
                            <i class="edge icon"></i>
                            <div class="content">
                                <a href="#">xxx</a>
                            </div>
                        </div>
                        <div class="item">
                            <i class="internet explorer icon"></i>
                            <div class="content">
                                <a href="#">xxx</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection