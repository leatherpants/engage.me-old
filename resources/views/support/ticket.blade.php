@extends('templates.frontend')
@section('title', trans('support.support').' | '.config('app.name'))
@section('content')
    <div class="ui stackable grid">
        <div class="row">
            <div class="column">
                <div class="ui inverted segment">
                    <h3 class="ui orange inverted dividing header">
                        <i class="life ring icon"></i>
                        <span class="content">
                            {{ trans('support.support') }}
                        </span>
                    </h3>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="column">
                <div class="ui stackable grid">
                    <div class="row">
                        <div class="four wide column">
                            <div class="ui inverted segments">
                                <div class="ui inverted nopadding segment">
                                    <img src="{{ Auth::user()->userImage() }}" alt="{{ Auth::user()->getDisplayName() }}" class="ui fluid image">
                                </div>
                                <div class="ui inverted nopadding segment">
                                    <a href="{{ route('support') }}" class="ui orange blocked labeled icon fluid button">
                                        <i class="left arrow icon"></i>
                                        {{ trans('support.support') }}
                                    </a>
                                </div>
                                <div class="ui inverted segment">
                                    <div class="ui list">
                                        <div class="item">
                                            <div class="content">
                                                <i class="time icon"></i>
                                                {{ $ticket->created_at->diffForHumans() }}
                                            </div>
                                        </div>
                                        <div class="item">
                                            <div class="content">
                                                <i class="user icon"></i>
                                                @if ($ticket->assignee === null)
                                                    {{ trans('support.unassigned') }}
                                                @else
                                                    {{ $ticket->assignedTo->getDisplayName() }} ({{ trans('support.assigned') }})
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="twelve wide column">
                            <div class="ui inverted segment">
                                <h3 class="ui orange inverted dividing header">
                                    <i class="ticket icon"></i>
                                    <span class="content">
                                        {{ $ticket->title }}
                                    </span>
                                </h3>
                                <div class="ui inverted comments">
                                    <div class="comment">
                                        <div class="content">
                                            {{ $ticket->content }}
                                        </div>
                                        <div class="actions">
                                            <a href="">
                                                <i class="reply icon"></i>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection