@extends('templates.frontend')
@section('title', trans('support.support').' | '.config('app.name'))
@section('content')
    <div class="ui stackable grid">
        <div class="row">
            <div class="column">
                <div class="ui inverted segments">
                    <div class="ui inverted segment">
                        <h3 class="ui orange inverted dividing header">
                            <i class="life ring icon"></i>
                            <span class="content">
                                {{ trans('support.create_ticket') }}
                            </span>
                        </h3>
                    </div>
                    <div class="ui clearing inverted segment">
                        @include('_partials.message.error')
                        <form action="{{ route('support.create') }}" method="post" class="ui inverted form">
                            {{ csrf_field() }}
                            <div class="field{{ $errors->has('title') ? ' error' : '' }}">
                                <label for="title">{{ trans('support.title') }}</label>
                                <input type="text" name="title" id="title" placeholder="{{ trans('support.title') }}" value="{{ old('title') }}">
                            </div>
                            <div class="field{{ $errors->has('content') ? ' error' : '' }}">
                                <label for="content">{{ trans('support.content') }}</label>
                                <textarea name="content" id="content" cols="30" rows="10" placeholder="{{ trans('support.content') }}">{{ old('content') }}</textarea>
                            </div>
                            <button type="submit" class="ui right floated orange labeled icon button">
                                <i class="save icon"></i>
                                {{ trans('common.send') }}
                            </button>
                            <a href="{{ route('support') }}" class="ui right floated orange labeled icon button">
                                <i class="left arrow icon"></i>
                                {{ trans('support.support') }}
                            </a>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection