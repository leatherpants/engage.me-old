@extends('templates.frontend')
@section('title', trans('support.support').' | '.config('app.name'))
@section('content')
    <div class="ui stackable grid">
        <div class="row">
            <div class="column">
                <div class="ui inverted segments">
                    <div class="ui inverted segment">
                        <h3 class="ui orange inverted dividing header">
                            <i class="life ring icon"></i>
                            <span class="content">
                                {{ trans('support.support') }}
                            </span>
                        </h3>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="column">
                <div class="ui stackable grid">
                    <div class="row">
                        <div class="four wide column">
                            <div class="ui inverted segments">
                                <div class="ui inverted nopadding segment">
                                    <img src="{{ Auth::user()->userImage() }}" alt="{{ Auth::user()->getDisplayName() }}" class="ui fluid image">
                                </div>
                                <div class="ui inverted nopadding segment">
                                    <a href="{{ route('support.create') }}" class="ui orange blocked labeled icon fluid button">
                                        <i class="add icon"></i>
                                        {{ trans('support.create_ticket') }}
                                    </a>
                                    <a href="{{ route('help') }}" class="ui orange blocked labeled icon fluid button">
                                        <i class="left arrow icon"></i>
                                        {{ trans('common.help') }}
                                    </a>
                                </div>
                                <div class="ui inverted segment">
                                    <div class="ui inverted list">
                                        <div class="item">
                                            <div class="content">
                                                <div class="header">{{ trans('support.your_tickets') }}</div>
                                                {{ $countOfTickets }}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="twelve wide column">
                            <div class="ui inverted segment">
                                @if (count($tickets) > 0)
                                <div class="ui inverted divided items">
                                @foreach($tickets as $ticket)
                                    <div class="item">
                                        <div class="content">
                                            <a href="{{ route('support.ticket', ['id' => $ticket->id]) }}" class="header">{{ $ticket->title }}</a>
                                            <div class="meta">
                                                <span class="ui orange label">
                                                    {{ $ticket->created_at->diffForHumans() }}
                                                </span>
                                                @if ($ticket->assignee)
                                                    <span class="ui right floated orange label">{{ $ticket->assignedTo->getDisplayName() }}</span>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                                </div>
                                @else
                                <div class="ui orange inverted icon message">
                                    <i class="info circle icon"></i>
                                    <div class="content">
                                        <p>{{ trans('error.no_tickets_created_by_user') }}</p>
                                    </div>
                                </div>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection