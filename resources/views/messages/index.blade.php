@extends('templates.frontend')
@section('title', trans('common.messages').' | '.config('app.name'))
@section('content')
    <div class="ui stackable grid">
        <div class="row">
            <div class="column">
                <div class="ui inverted segment">
                    <h3 class="ui orange inverted dividing header">
                        <i class="comments outline icon"></i>
                        <span class="content">
                            {{ trans('common.messages') }}
                        </span>
                    </h3>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="four wide column">
                <div class="ui inverted segment">
                    <div class="ui inverted divided list">
                        <a href="#" class="item">
                            <img src="http://media.engageme.granath/images/users/jessika-granath.jpg" alt="Jessika Granath" class="ui avatar image">
                            <div class="middle aligned content">
                                Jessika Granath
                                <div class="description">
                                    10 days ago
                                </div>
                            </div>
                        </a>
                        <a href="#" class="item">
                            <img src="http://media.engageme.granath/images/users/anna-kaplun.jpg" alt="Anna Kaplun" class="ui avatar image">
                            <div class="middle aligned content">
                                Anna Kaplun
                                <div class="description">
                                    10 days ago
                                </div>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
            <div class="twelve wide column">
                <div class="ui inverted segment">
                    <h4 class="ui orange inverted dividing header">
                        <img src="http://media.engageme.granath/images/users/jessika-granath.jpg" alt="" class="ui circular image">
                        <span class="content">
                            Jessika Granath
                            <span class="sub header">
                                Simulation driver
                            </span>
                        </span>
                    </h4>
                    <div class="ui inverted middle aligned divided list">
                        <div class="item">
                            <div class="right floated content">
                                <img src="http://media.engageme.granath/images/users/sandra-granath.jpg" alt="Sandra Granath" class="ui avatar image">
                            </div>
                            <div class="right floated content">
                                Hi sister. How are you?
                                <div class="description">
                                    10 days ago
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <img src="http://media.engageme.granath/images/users/jessika-granath.jpg" alt="Jessika Granath" class="ui avatar image">
                            <div class="content">
                                I'm fine, sister. How are you?
                                <div class="description">
                                    10 days ago
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection