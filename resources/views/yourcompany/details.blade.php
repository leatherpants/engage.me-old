@extends('templates.frontend')
@section('title', trans('common.company.your_company').' | '.config('app.name'))
@section('content')
    <div class="ui stackable grid">
        <div class="row">
            <div class="column">
                <div class="ui inverted tall stacked segment">
                    <h3 class="ui orange inverted dividing header">
                        {{ trans('common.company.your_company') }}
                        <span class="sub header">
                            {{ $company->title }}
                        </span>
                    </h3>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="twelve wide column">
                <div class="ui inverted tall stacked segment">
                    <h3 class="ui orange inverted dividing header">
                        <i class="settings icon"></i>
                        <span class="content">
                            {{ trans('common.settings') }}
                        </span>
                    </h3>
                    <form action="#" method="post" class="ui inverted form">
                        <div class="field">
                            <label for="content">{{ trans('news.content') }}</label>
                            <textarea name="content" id="content" cols="30" rows="10" placeholder="{{ trans('news.content') }}">{{ $details->content }}</textarea>
                        </div>
                    </form>
                </div>
            </div>
            <div class="four wide column">
                @include('yourcompany._partials.sidebar')
            </div>
        </div>
    </div>
@endsection