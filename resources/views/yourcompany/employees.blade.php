@extends('templates.frontend')
@section('title', trans('common.company.your_company').' | '.config('app.name'))
@section('content')
    <div class="ui stackable grid">
        <div class="row">
            <div class="column">
                <div class="ui inverted tall stacked segment">
                    <h3 class="ui orange inverted dividing header">
                        {{ trans('common.company.your_company') }}
                        <span class="sub header">
                            {{ $company->title }}
                        </span>
                    </h3>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="twelve wide column">
                <div class="ui inverted tall stacked segment">
                    <h3 class="ui orange inverted dividing header">
                        <i class="users icon"></i>
                        <span class="content">
                            {{ trans('common.employees') }}
                        </span>
                    </h3>
                    <div class="ui three stackable cards">
                        @foreach($employees as $employee)
                            <div class="card">
                                <div class="image">
                                    <img src="{{ $employee->user->photo->url }}" alt="{{ $employee->user->getDisplayName() }}">
                                </div>
                                <div class="content">
                                    <div class="header">
                                        {{ $employee->user->getDisplayName() }}
                                    </div>
                                    <div class="meta">
                                        Position
                                    </div>
                                </div>
                                <div class="extra content">
                                    <div class="ui red fluid icon button">
                                        <i class="delete icon"></i>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
            <div class="four wide column">
                @include('yourcompany._partials.sidebar')
            </div>
        </div>
    </div>
@endsection