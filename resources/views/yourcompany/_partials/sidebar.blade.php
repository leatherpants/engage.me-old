@if (Request::is('yourcompany/*'))
<div class="ui inverted tall stacked segment">
    <h3 class="ui image header">
        <img src="{{ $company->image }}" alt="{{ $company->title }}" class="image">
        <span class="content">
            {{ $company->title }}
        </span>
    </h3>
</div>
<div class="ui inverted tall stacked segment">
    <a href="{{ route('yourcompany.details', ['slug' => $company->slug]) }}" class="ui orange fluid labeled icon button">
        <i class="settings icon"></i>
        {{ trans('common.settings') }}
    </a>
</div>
@endif
<div class="ui inverted tall stacked segment">
    <h3 class="ui orange inverted dividing centered header">
        {{ trans('common.company.your_company') }}
    </h3>
    <div class="ui inverted divided list">
        @foreach($companies as $company)
            <a href="{{ route('yourcompany.company', ['slug' => $company->slug]) }}" class="item">
                {{ $company->title }}
            </a>
        @endforeach
    </div>
    @if (Request::is('yourcompany/*'))
    <a href="{{ route('yourcompany') }}" class="ui orange fluid labeled icon button">
        <i class="building icon"></i>
        {{ trans('common.company.your_company') }}
    </a>
    @endif
</div>