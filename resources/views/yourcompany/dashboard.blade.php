@extends('templates.frontend')
@section('title', trans('common.company.your_company').' | '.config('app.name'))
@section('content')
    <div class="ui stackable grid">
        <div class="row">
            <div class="column">
                <div class="ui inverted tall stacked segment">
                    <h3 class="ui orange inverted dividing header">
                        {{ trans('common.company.your_company') }}
                        <span class="sub header">
                            {!! $user->getDisplayNameVerification() !!}
                        </span>
                    </h3>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="twelve wide column">
                <div class="ui inverted tall stacked segment">
                    <h3 class="ui orange inverted dividing header">
                        <i class="line chart icon"></i>
                        <span class="content">
                            {{ trans('common.statistics') }}
                        </span>
                    </h3>
                    <div class="ui stackable three column grid">
                        <div class="column">
                            <h4 class="ui orange inverted dividing header">
                                <i class="list icon"></i>
                                <span class="content">
                                    {{ trans('common.vacancies') }}
                                </span>
                            </h4>
                            <h5 class="ui inverted centered header" style="margin-top: 0">
                                You're not searching.
                            </h5>
                            <div class="ui orange fluid icon labeled button">
                                <i class="list icon"></i>
                                {{ trans('common.vacancies') }}
                            </div>
                        </div>
                        <div class="column">
                            <h4 class="ui orange inverted dividing header">
                                <i class="users icon"></i>
                                <span class="content">
                                    {{ trans('common.employees') }}
                                </span>
                            </h4>
                            <h5 class="ui inverted centered header" style="margin-top: 0">
                                1.000.000
                            </h5>
                            <div class="ui orange fluid icon labeled button">
                                <i class="users icon"></i>
                                {{ trans('common.employees') }}
                            </div>
                        </div>
                        <div class="column">
                            <h4 class="ui orange inverted dividing header">
                                <i class="list icon"></i>
                                <span class="content">
                                    {{ trans('common.reviews') }}
                                </span>
                            </h4>
                            <h5 class="ui inverted centered header" style="margin-top: 0">
                                0
                            </h5>
                            <div class="ui orange fluid icon labeled button">
                                <i class="list icon"></i>
                                {{ trans('common.reviews') }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="four wide column">
                @include('yourcompany._partials.sidebar')
            </div>
        </div>
    </div>
@endsection