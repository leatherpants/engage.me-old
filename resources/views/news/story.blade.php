@extends('templates.frontend')
@section('title', trans('common.blog').' | '.config('app.name'))
@section('content')
    <div class="ui stackable grid">
        <div class="row">
            <div class="column">
                <div class="ui inverted segments">
                    <div class="ui inverted segment">
                        <h3 class="ui orange inverted dividing header">
                            <i class="write icon"></i>
                            <span class="content">
                                {{ $story->title }}
                            </span>
                        </h3>
                    </div>
                    <div class="ui inverted segment">
                        <div class="ui grid">
                            <div class="row">
                                <div class="sixteen wide right aligned column">
                                    <div class="ui icon label">
                                        <i class="folder outline icon"></i>
                                        <span>News</span>
                                    </div>
                                    <a href="{{ route('profile', ['slug_name' => $story->user->slug_name]) }}" class="ui image label">
                                        <img src="{{ $story->user->userImage() }}" alt="{{ $story->user->first_name }}">
                                        {{ $story->user->first_name }}
                                    </a>
                                    <div class="ui icon label">
                                        <i class="calendar icon"></i>
                                        {{ $story->created_at->format('d.m.Y') }}
                                    </div>
                                </div>
                                <div class="sixteen wide column">
                                    {!! $story->content !!}
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="ui inverted segment">
                        <div class="ui grid">
                            <div class="row">
                                <div class="right aligned column">
                                    <a href="{{ route('blog') }}" title="{{ trans('common.back_posts') }}">
                                        <i class="angle double left icon"></i>
                                        {{ trans('common.back_posts') }}
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection