@extends('templates.frontend')
@section('title', trans('common.blog').' | '.config('app.name'))
@section('content')
    <div class="ui stackable grid">
        <div class="row">
            <div class="column">
                <div class="ui inverted segments">
                    <div class="ui inverted segment">
                        <h3 class="ui orange inverted dividing header">
                            <i class="write icon"></i>
                            <span class="content">
                                {{ trans('common.blog') }}
                            </span>
                        </h3>
                    </div>
                    <div class="ui inverted segment">
                        <div class="ui inverted items">
                        @foreach($stories as $story)
                            <div class="item">
                                <div class="content">
                                    <a href="{{ route('blog.story', ['slug' => $story->slug]) }}" title="{{ $story->title }}" class="header">
                                        {{ $story->title }}
                                    </a>
                                    <div class="extra">
                                        <div class="ui icon label">
                                            <i class="folder outline icon"></i>
                                            <span>News</span>
                                        </div>
                                        <a href="{{ route('profile', ['slug' => $story->user->slug_name]) }}" class="ui image label">
                                            <img src="{{ $story->user->userImage() }}" alt="{{ $story->user->first_name }}">
                                            {{ $story->user->first_name }}
                                        </a>
                                        <div class="ui label">
                                            <i class="calendar icon"></i> {{ $story->created_at->format('d.m.Y') }}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection