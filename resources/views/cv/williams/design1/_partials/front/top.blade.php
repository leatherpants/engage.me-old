<div class="front-segment front-top">
    <h3 class="front-header">
        <span style="font-size: 32px;">{{ $data['user']['display_name'] }}</span><br><br>
        @if ($data['address']['street'])
            {{ $data['address']['street'] }} |
        @endif
        @if ($data['address']['zip_code'] && $data['address']['city'])
            {{ $data['address']['zip_code'] }} {{ $data['address']['city'] }}<br>
        @endif
        @if ($data['address']['phone'] && $data['address']['mobile'])
            {{ $data['address']['phone'] }} | {{ $data['address']['mobile'] }}<br>
        @endif
        {{ $data['user']['email'] }}
    </h3>
</div>