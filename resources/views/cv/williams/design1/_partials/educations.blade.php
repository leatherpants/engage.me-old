<div class="segment">
    <h3 class="segment-header">
        @if (count($data['educations']) == 1)
            {{ trans('cv.education') }}
        @else
            {{ trans('cv.educations') }}
        @endif
    </h3>
    <div class="segment-content">
        <div class="entry">
            @foreach($data['educations'] as $education)
                <div class="entry-item">
                    <div class="entry-date">
                        @if ($education['begin_date'] !== null && $education['end_date'] !== null)
                            {{ date('m/Y', strtotime($education['begin_date'])) }} - {{ date('m/Y', strtotime($education['end_date'])) }}
                        @elseif ($education['begin_date'] !== null && $education['end_date'] == null)
                            {{ trans('common.since') }} {{ date('m/Y', strtotime($education['begin_date'])) }}
                        @endif
                    </div>
                    <div class="entry-header">
                        {{ $education['degree'] }}
                    </div>
                    <div class="entry-meta">
                        {{ $education['name'] }}@if (!empty($education['city'])), {{ $education['city'] }}@endif
                        {{ $education['subject'] }}
                    </div>
                    @if ($education['notes'])
                    <div class="entry-desc">
                        {!! $education['notes'] !!}
                    </div>
                    @endif
                </div>
            @endforeach
        </div>
    </div>
</div>