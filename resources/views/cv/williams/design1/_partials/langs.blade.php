<div class="segment">
    <h3 class="segment-header">
        @if (count($data['langs']) == 1)
            {{ trans('cv.language') }}
        @else
            {{ trans('cv.languages') }}
        @endif
    </h3>
    <div class="segment-content">
        <div class="list">
            @foreach($data['langs'] as $lang)
                <div class="list-item">
                    <div class="right">
                        @if (!empty($lang['level']))
                            {{ $lang['level'] }}
                        @else
                            {{ trans('lang.rating.native') }}
                        @endif
                    </div>
                    <div class="list-entry">
                        {{ trans('lang.' . $lang['name']) }}
                    </div>
                </div>
            @endforeach
        </div>
    </div>
</div>