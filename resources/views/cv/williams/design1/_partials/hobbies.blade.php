<div class="segment">
    <h3 class="segment-header">@if (count($data['hobbies']) == 1){{ trans('cv.hobby') }}@else{{ trans('cv.hobbies') }}@endif</h3>
    <div class="segment-content">
        <div class="entry">
        @foreach($data['hobbies'] as $hobby)
            <div class="entry-item">
                <div class="entry-header">{{ $hobby['name'] }}</div>
                @if ($hobby['desc'])
                <div class="entry-desc">
                    {!! $hobby['desc'] !!}
                </div>
                @endif
                @if ($hobby['url'])
                <div class="entry-link"><a href="{{ $hobby['url'] }}" target="_blank">{{ $hobby['url'] }}</a></div>
                @endif
            </div>
        @endforeach
        </div>
    </div>
</div>