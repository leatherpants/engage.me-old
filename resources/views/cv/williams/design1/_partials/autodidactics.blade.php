<div class="segment">
    <h3 class="segment-header">
        @if (count($data['autodidactics']) == 1)
            {{ trans('cv.autodidactic') }}
        @else
            {{ trans('cv.autodidactics') }}
        @endif
    </h3>
    <div class="segment-content">
        <div class="entry">
            @foreach($data['autodidactics'] as $autodidactic)
                <div class="entry-item">
                    <div class="entry-header">{{ $autodidactic['name'] }}</div>
                    @if ($autodidactic['desc'])
                        <div class="entry-desc">
                            {!! $autodidactic['desc'] !!}
                        </div>
                    @endif
                </div>
            @endforeach
        </div>
    </div>
</div>