<div class="segment">
    <h3 class="segment-header">
        @if (count($data['softs']) == 1)
            {{ trans('cv.soft_skill') }}
        @else
            {{ trans('cv.soft_skills') }}
        @endif
    </h3>
    <div class="segment-content">
        <div class="list">
            @foreach($data['softs'] as $skill)
                <div class="list-item">
                    <div class="list-entry">
                        {{ $skill['name'] }}
                    </div>
                </div>
            @endforeach
        </div>
    </div>
</div>