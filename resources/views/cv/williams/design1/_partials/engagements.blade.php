<div class="segment">
    <h3 class="segment-header">
        @if (count($data['engagements']) == 1)
            {{ trans('cv.engagement') }}
        @else
            {{ trans('cv.engagements') }}
        @endif
    </h3>
    <div class="segment-content">
        <div class="entry">
            @foreach($data['engagements'] as $engagement)
                <div class="entry-item">
                    <div class="entry-header">{{ $engagement['name'] }}</div>
                    @if ($engagement['desc'])
                        <div class="entry-desc">
                            {!! $engagement['desc'] !!}
                        </div>
                    @endif
                </div>
            @endforeach
        </div>
    </div>
</div>