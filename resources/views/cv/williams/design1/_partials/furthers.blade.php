<div class="segment">
    <h3 class="segment-header">
        @if (count($data['furthers']) == 1)
            {{ trans('cv.further_education') }}
        @else
            {{ trans('cv.further_educations') }}
        @endif
    </h3>
    <div class="segment-content">
        <div class="entry">
            @foreach($data['furthers'] as $education)
                <div class="entry-item">
                    <div class="entry-date">
                        @if ($education['begin_date'])
                            @if ($education['begin_date'] && $education['end_date'])
                                {{ date('m/Y', strtotime($education['begin_date'])) }} - {{ date('m/Y', strtotime($education['end_date'])) }}
                            @else
                                {{ trans('cv.since') }} {{ date('m/Y', strtotime($education['begin_date'])) }}
                            @endif
                        @endif
                    </div>
                    <div class="entry-header">
                        {{ $education['degree'] }}
                    </div>
                    <div class="entry-meta">
                        @if(!$education['city'])
                            {{ $education['name'] }}
                        @else
                            {{ $education['name'] }}, {{ $education['city'] }}
                        @endif
                    </div>
                    <div class="entry-desc">
                        {!! $education['notes'] !!}
                    </div>
                </div>
            @endforeach
        </div>
    </div>
</div>