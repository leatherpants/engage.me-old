<div class="segment">
    <h3 class="segment-header">{{ trans('cv.experience') }}</h3>
    <div class="segment-content">
        <div class="entry">
            @if (count($data) > 0)
                @foreach($data['experiences'] as $experience)
                    <div class="entry-item">
                        @if ($experience['begin_date'])
                            <div class="entry-date">
                                @if ($experience['begin_date'] && !$experience['end_date'])
                                    {{ trans('cv.since') }}
                                @endif
                                {{ date('m/Y', strtotime($experience['begin_date'])) }}
                                @if ($experience['end_date'])
                                    - {{ date('m/Y', strtotime($experience['end_date'])) }}
                                @endif
                            </div>
                        @endif
                        @if ($experience['name'])
                            <div class="entry-header">
                                {{ $experience['name'] }}
                            </div>
                        @endif
                        @if ($experience['title'])
                            <div class="entry-meta">
                                {{ $experience['title'] }}@if (!empty($experience['city']))@if (!empty($experience['name'])),@endif {{ $experience['city'] }}@endif
                            </div>
                        @endif
                        @if ($experience['desc'])
                            <div class="entry-desc">
                                {!! $experience['desc'] !!}
                            </div>
                        @endif
                        @if (!empty($experience['parent']))
                            @foreach($experience['parent'] as $parent)
                                @if ($parent->name)
                                    <div class="entry-header">
                                        {{ $parent->name }}
                                    </div>
                                @endif
                                @if ($parent->title)
                                    <div class="entry-meta">
                                        {{ $parent->title }}@if (!empty($parent->city))@if (!empty($parent->name)),@endif {{ $parent->city }}@endif
                                    </div>
                                @endif
                                @if ($parent->desc)
                                    <div class="entry-desc">
                                        {!! $parent->desc !!}
                                    </div>
                                @endif
                            @endforeach
                        @endif
                    </div>
                @endforeach
            @else
                {{--TODO:better error message--}}
                <div class="entry-item">
                    <div class="entry-header">I'm missing some experiences of you.</div>
                </div>
                <div class="entry-desc">
                    Please <a href="#">add</a> some experiences for your CV.
                </div>
            @endif
        </div>
    </div>
</div>