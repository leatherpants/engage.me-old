<div class="segment">
    <h3 class="segment-header">
        @if (count($data['skills']) == 1)
            {{ trans('cv.skill') }}
        @else
            {{ trans('cv.skills') }}
        @endif
    </h3>
    <div class="segment-content">
        <div class="list">
            @foreach($data['skills'] as $skill)
                <div class="list-item">
                    <div class="right">
                        {{ $skill['notice'] }}
                    </div>
                    <div class="list-entry">
                        {{ $skill['name'] }}
                    </div>
                    <div class="list-desc">{{ $skill['desc'] }}</div>
                    <div class="rating-bar">
                        @foreach(range(1,5) as $rate)
                            <span class="rating{{ ($rate <= $skill['rating']) ? ' selected' : '' }}"></span>
                        @endforeach
                    </div>
                </div>
            @endforeach
        </div>
    </div>
</div>