<div class="segment">
    <h3 class="segment-header">{{ trans('cv.social_media') }}</h3>
    <div class="segment-content">
        <div class="social">
            @foreach($data['socials'] as $social)
                <div class="social-item">
                    <a href="{{ $social['url'] }}" target="_blank" class="social-entry">
                        <i class="{{ $social['name'] }}"></i>
                        {{ $social['user_name'] }}
                    </a>
                </div>
            @endforeach
        </div>
    </div>
</div>