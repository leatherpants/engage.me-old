@extends('templates.cv')
@section('title', trans('cv.cv'))
@section('content')
    <div class="cv">
        <div class="row">
            <div class="column-12 cv-inner front-inner">
                <div class="front-segment front-top">
                    <h3 class="front-header">{{ $data['title'] }}</h3>
                </div>
                <div class="front-segment front-middle front-blank">
                    <div class="front-content">
                        <p>{{ $data['salutation'] }}</p>
                        {!! $data['content'] !!}
                        <p>Mit freundlichen Grüßen</p>
                        <p>{{ $data['user'] }}</p>
                    </div>
                </div>
                <div class="front-segment front-bottom">
                    <h3 class="front-header">
                        {{ $data['companyname'] }}<br>
                        {{ $data['contactname'] }}<br><br>
                        {{ $data['companystreet'] }}<br>
                        {{ $data['companyzipcode'] }} {{ $data['companycity'] }}
                    </h3>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('style')
    <link rel="stylesheet" href="{{ mix('assets/themes/williams/design.css') }}">
@endsection