@extends('templates.blank')
@section('title', trans('common.cv'))
@section('content')
    <div class="cv">
        <div class="row">
            <div class="column-12 cv-inner front-inner">
                @include('cv.williams.design1._partials.front.top')
                <div class="front-segment front-middle">
                    <div class="front-image">
                        <h3 class="front-header">piedi per terra e a testa bassa / down to earth and head down</h3>
                        <div>
                            <img src="{{ $data['item']['user_image'] }}" alt="{{ $data['user']['display_name'] }}">
                        </div>
                        @if ($data['user']['birthday'])
                        <h3 class="front-header">{{ trans('cv.born_on', ['birthday' => date('d.m.Y', strtotime($data['user']['birthday']))]) }}</h3>
                        @endif
                    </div>
                </div>
                <div class="front-segment front-bottom">
                    <h3 class="front-header">{{ $data['message']['message'] }}</h3>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('style')
    <link rel="stylesheet" href="{{ mix('assets/themes/williams/design.css') }}">
@endsection