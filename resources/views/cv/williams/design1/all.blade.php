@extends('templates.cv')
@section('title', trans('cv.cv'))
@section('content')
    <div class="cv">
        <div class="row">
            <div class="column-12 cv-inner front-inner">
                <div class="front-segment front-top">
                    <h3 class="front-header">
                        <span style="font-size: 32px;">{{ $front['user']['display_name'] }}</span><br><br>
                        @if ($front['address']['street'])
                        {{ $front['address']['street'] }} |
                        @endif
                        @if ($front['address']['zip_code'] && $front['address']['city'])
                        {{ $front['address']['zip_code'] }} {{ $front['address']['city'] }}<br>
                        @endif
                        @if ($front['address']['phone'] && $front['address']['mobile'])
                            {{ $front['address']['phone'] }} | {{ $front['address']['mobile'] }}<br>
                        @endif{{ $front['user']['email'] }}
                    </h3>
                </div>
                <div class="front-segment front-middle">
                    <div class="front-image">
                        <h3 class="front-header">{{ $letter['title'] }}</h3>
                        <div>
                            <img src="{{ $front['item']['user_image'] }}" alt="{{ $front['user']['display_name'] }}">
                        </div>
                        @if ($front['user']['birthday'])
                            <h3 class="front-header">{{ trans('cv.born_on', ['birthday' => date('d.m.Y', strtotime($front['user']['birthday']))]) }}</h3>
                        @endif
                    </div>
                </div>
                <div class="front-segment front-bottom">
                    <h3 class="front-header">{{ $front['message']['message'] }}</h3>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="column-12 cv-inner front-inner">
                <div class="front-segment front-top">
                    <h3 class="front-header">{{ $letter['title'] }}</h3>
                </div>
                <div class="front-segment front-middle front-blank">
                    <div class="front-content">
                        <p>{{ $letter['salutation'] }}</p>
                        {!! $letter['content'] !!}
                        <p>Mit freundlichen Grüßen</p>
                        <p>{{ $letter['user'] }}</p>
                    </div>
                </div>
                <div class="front-segment front-bottom">
                    <h3 class="front-header">
                        {{ $letter['companyname'] }}<br>
                        {{ $letter['contactname'] }}<br><br>
                        {{ $letter['companystreet'] }}<br>
                        {{ $letter['companyzipcode'] }} {{ $letter['companycity'] }}
                    </h3>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="column-12 cv-inner">
                @include('cv.williams.design1._partials.experiences')
                @include('cv.williams.design1._partials.skills')
                @include('cv.williams.design1._partials.educations')
            </div>
        </div>
        <div class="row">
            <div class="column-12 cv-inner">
                <div class="segment message">
                    <h3 class="segment-header segment-headline">{{ $front['message']['message'] }}</h3>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="column-12 cv-inner">
                @include('cv.williams.design1._partials.furthers')
                @include('cv.williams.design1._partials.autodidactics')
                @include('cv.williams.design1._partials.engagements')
                @include('cv.williams.design1._partials.langs')
                @include('cv.williams.design1._partials.softskills')
                @include('cv.williams.design1._partials.hobbies')
                @include('cv.williams.design1._partials.socialmedia')
            </div>
        </div>
    </div>
@endsection
@section('style')
    <link rel="stylesheet" href="{{ mix('assets/themes/williams/design.css') }}">
@endsection