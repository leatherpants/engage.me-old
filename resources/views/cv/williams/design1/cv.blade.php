@extends('templates.cv')
@section('title', trans('cv.cv'))
@section('content')
    <div class="cv">
        <div class="row">
            <div class="column-12 cv-inner">
@include('cv.williams.design1._partials.experiences')
@include('cv.williams.design1._partials.skills')
@include('cv.williams.design1._partials.educations')
            </div>
        </div>
        <div class="row">
            <div class="column-12 cv-inner">
                <div class="segment message">
                    <h3 class="segment-header segment-headline">{{ $data['message']['message'] }}</h3>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="column-12 cv-inner">
@include('cv.williams.design1._partials.furthers')
@include('cv.williams.design1._partials.autodidactics')
@include('cv.williams.design1._partials.engagements')
@include('cv.williams.design1._partials.langs')
@include('cv.williams.design1._partials.softskills')
@include('cv.williams.design1._partials.hobbies')
@include('cv.williams.design1._partials.socialmedia')
            </div>
        </div>
    </div>
@endsection
@section('style')
    <link rel="stylesheet" href="{{ mix('assets/themes/williams/design.css') }}">
@endsection