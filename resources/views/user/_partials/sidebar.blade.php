<div class="ui tall stacked inverted segments">
    <div class="ui inverted orange segment">
        <h4 class="ui header">
            <i class="user icon"></i>
            <span class="center aligned content">
                {!! $user->getDisplayNameVerification() !!}
                @if ($user->occupation)
                    <span class="sub header">{{ $user->occupation }}</span>
                @endif
            </span>
        </h4>
    </div>
    <div class="ui inverted nopadding segment">
        <img src="{{ $user->userImage() }}" alt="{{ $user->getDisplayName() }}" class="ui fluid image">
    </div>
    <div class="ui inverted nopadding segment">
        {!! $contact !!}
    </div>
</div>