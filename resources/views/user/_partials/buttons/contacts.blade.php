<a href="{{ route('contacts') }}" class="ui fluid orange labeled icon contact button" style="margin-left: auto;margin-right: auto;">
    <i class="users icon"></i>
    {{ trans('common.contacts') }}
</a>