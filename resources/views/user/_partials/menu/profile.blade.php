<div class="ui inverted tall stacked segment">
    <div class="ui inverted secondary pointing four item menu">
        <a href="{{ route('profile', ['slug' => $user->slug_name]) }}" class="{{ Request::is($user->slug_name) ? 'active ' : '' }}item">
            Home
        </a>
        <a href="{{ route('profile.details', ['slug' => $user->slug_name]) }}" class="{{ Request::is($user->slug_name.'/details') ? 'active ' : '' }}item">
            Details
        </a>
        <a href="{{ route('profile.profiles', ['slug' => $user->slug_name]) }}" class="{{ Request::is($user->slug_name.'/profiles') ? 'active ' : '' }}item">
            Profiles
        </a>
        <a href="{{ route('profile.stats', ['slug' => $user->slug_name]) }}" class="{{ Request::is($user->slug_name.'/stats') ? 'active ' : '' }}item">
            Stats
        </a>
    </div>
</div>