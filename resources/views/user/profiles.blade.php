@extends('templates.frontend')
@section('title', $user->getDisplayName().' | '.config('app.name'))
@section('content')
    <div class="ui stackable grid">
        <div class="row">
            <div class="four wide column">
                @include('user._partials.sidebar')
            </div>
            <div class="twelve wide column">
                @include('user._partials.menu.profile')
                <div class="ui inverted tall stacked segment">
                    <h3 class="ui orange inverted dividing header">
                        {{ trans('common.social') }}
                    </h3>
                    <div class="ui inverted relaxed divided list">
                        @foreach($socials as $social)
                            <div class="item">
                                <i class="large {{ $social->name }} middle aligned icon"></i>
                                <div class="middle aligned content">
                                    <a href="{{ $social->url }}" target="_blank" rel="noopener" class="header">
                                        {{ $social->user_name }}
                                    </a>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection