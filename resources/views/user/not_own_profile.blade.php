@extends('templates.frontend')
@section('title', $user->getDisplayName().' | '.config('app.name'))
@section('content')
    <div class="ui stackable grid">
        <div class="row">
            <div class="five wide column">
                <div class="ui inverted tall stacked segments">
                    <div class="ui inverted center aligned segment">
                        <img src="{{ $user->userImage()}}" alt="{{ $user->getDisplayName() }}" class="ui centered rounded profile image">
                        {!! $contact !!}
                    </div>
                </div>
            </div>
            <div class="eleven wide column">
                <div class="ui inverted tall stacked segment">
                    <h1 class="ui orange inverted dividing header">
                        <i class="user icon"></i>
                        <span class="content">
                            {!! $user->getDisplayNameVerification() !!}
                            @if ($user->occupation)
                                <span class="sub header">{{ $user->occupation }}</span>
                            @endif
                        </span>
                    </h1>
                </div>
                @if (count($experiences)>0)
                <div class="ui inverted tall stacked segment">
                    <h3 class="ui orange inverted dividing header">
                        {{ trans('cv.experience') }}
                    </h3>
                    <div class="ui inverted divided items">
                        @foreach($experiences as $experience)
                            <div class="item">
                                <div class="content">
                                    <div class="orange header">
                                        {{ $experience->title }}
                                    </div>
                                    <div class="orange meta">
                                        <span>{{ $experience->name }}</span>
                                    </div>
                                    <div class="orange description">
                                        {!! $experience->desc !!}
                                    </div>
                                    @if ($experience->url)
                                        <div class="orange extra">
                                            <span>{{ trans('common.website') }}</span>
                                            <a href="{{ $experience->url }}" target="_blank">{{ $experience->url }}</a>
                                        </div>
                                    @endif
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
                @endif
                @if (count($socials)>0)
                <div class="ui inverted tall stacked segment">
                    <h3 class="ui orange inverted dividing header">
                        {{ trans('common.social') }}
                    </h3>
                    <div class="ui inverted orange relaxed divided horizontal list">
                        @foreach($socials as $social)
                            <div class="item">
                                <div class="middle aligned content">
                                    <a href="{{ $social->url }}" target="_blank" title="{{ ucfirst($social->name) }}: {{ $social->user_name }}" class="header">
                                        <i class="large orange {{ $social->name }} middle aligned icon"></i>
                                    </a>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
                @endif
                <div class="ui inverted tall stacked segment">
                    <h3 class="ui orange inverted dividing header">
                        {{ trans('common.statistics') }}
                    </h3>
                    <div class="ui inverted orange relaxed divided list">
                        <div class="item">
                            <i class="large sign in middle aligned icon"></i>
                            <div class="middle aligned content">
                                <div class="description">{{ trans('stats.joined', ['time' => $user->created_at->diffForHumans()]) }}</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection