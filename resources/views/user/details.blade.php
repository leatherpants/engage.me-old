@extends('templates.frontend')
@section('title', $user->getDisplayName().' | '.config('app.name'))
@section('content')
    <div class="ui stackable grid">
        <div class="row">
            <div class="four wide column">
                @include('user._partials.sidebar')
            </div>
            <div class="twelve wide column">
                @include('user._partials.menu.profile')
                <div class="ui inverted tall stacked segment">
                    <h3 class="ui orange inverted dividing header">
                        {{ trans('cv.experience') }}
                    </h3>
                    <div class="ui inverted divided items">
                        @foreach($experiences as $experience)
                            <div class="item">
                                <div class="content">
                                    <div class="orange header">
                                        {{ $experience->title }}
                                    </div>
                                    <div class="orange meta">
                                        <span>{{ $experience->name }}</span>
                                    </div>
                                    <div class="orange description">
                                        {!! $experience->desc !!}
                                    </div>
                                    @if ($experience->url)
                                    <div class="orange extra">
                                        <span>{{ trans('common.website') }}</span>
                                        <a href="{{ $experience->url }}" target="_blank" rel="noopener">{{ $experience->url }}</a>
                                    </div>
                                    @endif
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection