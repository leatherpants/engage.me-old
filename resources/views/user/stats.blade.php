@extends('templates.frontend')
@section('title', $user->getDisplayName().' | '.config('app.name'))
@section('content')
    <div class="ui stackable grid">
        <div class="row">
            <div class="four wide column">
                @include('user._partials.sidebar')
            </div>
            <div class="twelve wide column">
                @include('user._partials.menu.profile')
                <div class="ui inverted tall stacked segment">
                    <h3 class="ui orange inverted dividing header">
                        {{ trans('common.statistics') }}
                    </h3>
                    <div class="ui inverted relaxed divided list">
                        <div class="item">
                            <i class="large sign in middle aligned icon"></i>
                            <div class="middle aligned content">
                                <div class="description">{{ trans('stats.joined', ['time' => $user->created_at->diffForHumans()]) }}</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection