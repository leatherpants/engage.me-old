@extends('templates.frontend')
@section('title', config('app.name'))
@section('content')
    <div class="ui stackable grid">
        <div class="row">
            <div class="column">
                <div class="ui inverted tall stacked segment">
                    <h1 class="ui inverted orange dividing center aligned header">
                        {{ trans('common.about_us') }}
                    </h1>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="column">
                <div class="ui inverted tall stacked segments">
                    <div class="ui inverted segment">
                        <p>engage.me is founded in 2017</p>
                    </div>
                    <div class="ui inverted center aligned segment">
                        <h2 class="ui inverted orange dividing header">{{ trans('common.statistics') }}</h2>
                        <div class="ui inverted relaxed divided list">
                            <div class="item">
                                <div class="content">
                                    {{ $users }} {{ trans('common.users') }}
                                </div>
                            </div>
                            <div class="item">
                                <div class="content">
                                    xxx Languages
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection