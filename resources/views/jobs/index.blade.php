@extends('templates.frontend')
@section('title', trans('common.jobs').' | '.config('app.name'))
@section('content')
    <div class="ui stackable grid">
        <div class="row">
            <div class="column">
                <div class="ui inverted tall stacked segment">
                    <h3 class="ui orange inverted dividing header">
                        <i class="newspaper icon"></i>
                        <span class="content">
                            {{ trans('common.jobs') }}
                        </span>
                    </h3>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="column">
                <div class="ui inverted tall stacked segment">
                    @if (!count($jobs))
                    <div class="ui orange icon message">
                        <i class="info circle icon"></i>
                        <span class="content">
                            {{ trans('error.no_jobs') }}
                        </span>
                    </div>
                    @else
                        <div class="ui inverted divided items">
                            @foreach($jobs as $job)
                                <div class="item">
                                    <div class="content">
                                        <a href="{{ route('jobs.view', ['company' => $job->company->slug, 'title' => $job->slug]) }}" class="header">{{ $job->title }}</a>
                                        <div class="meta">
                                            <a href="{{ route('companies.view', ['slug' => $job->company->slug]) }}">{{ $job->company->title }}</a>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
@endsection