@extends('templates.frontend')
@section('title', trans('common.jobs').' | '.config('app.name'))
@section('content')
    <div class="ui stackable grid">
        <div class="row">
            <div class="column">
                <div class="ui inverted segment">
                    <h3 class="ui inverted orange dividing header">
                        <i class="newspaper icon"></i>
                        <span class="content">
                            {{ $job->title }}
                        </span>
                    </h3>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="column">
                <div class="ui inverted segment">
                    <div class="ui stackable grid">
                        <div class="row">
                            <div class="twelve wide column">
                                {!! $job->desc !!}
                            </div>
                            <div class="four wide column">
                                <div class="ui divided list">
                                    <div class="item">
                                        <i class="building icon"></i>
                                        <div class="content">
                                            <a href="{{ route('companies.view', ['slug' => $job->company_slug]) }}">{{ $job->company_name }}</a>
                                        </div>
                                    </div>
                                    <div class="item">
                                        <i class="road icon"></i>
                                        <div class="content">
                                            {{ $job->location->city }}, {{ trans('countries.'.strtolower($job->location->countries->name)) }}
                                        </div>
                                    </div>
                                    <div class="item">
                                        <i class="time icon"></i>
                                        <div class="content">
                                            {{ $job->created_at->diffForHumans() }}
                                        </div>
                                    </div>
                                    @if ($job->created_at != $job->updated_at)
                                    <div class="item">
                                        <i class="write icon"></i>
                                        <div class="content">
                                            {{ $job->updated_at->diffForHumans() }}
                                        </div>
                                    </div>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection