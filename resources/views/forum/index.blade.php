@extends('templates.frontend')
@section('title', trans('common.forum').' | '.config('app.name'))
@section('content')
    <div class="ui stackable grid">
        <div class="row">
            <div class="column">
                <div class="ui inverted segments">
                    <div class="ui inverted clearing segment">
                        <h3 class="ui inverted orange dividing header">
                            <i class="list icon"></i>
                            <span class="content">
                                {{ trans('common.forum')  }}
                            </span>
                        </h3>
                        <a href="{{ route('help') }}" class="ui right floated orange labeled icon button">
                            <i class="help circle outline icon"></i>
                            Back to help
                        </a>
                        <a href="#" class="ui right floated orange labeled icon button">
                            <i class="settings icon"></i>
                            {{ trans('common.settings') }}
                        </a>
                        <a href="#" class="ui right floated orange labeled icon button">
                            <i class="plus icon"></i>
                            {{ trans('forum.add_discussion') }}
                        </a>
                    </div>
                    <div class="ui inverted segment">
                        <div class="ui grid">
                            <div class="row">
                                <div class="four wide column">
                                    <div class="ui inverted segment">
                                        <div class="ui inverted fluid vertical menu">
                                            <div class="item">
                                                <div class="header">Products</div>
                                                <div class="menu">
                                                    <a class="item">Enterprise</a>
                                                    <a class="item">Consumer</a>
                                                </div>
                                            </div>
                                            <div class="item">
                                                <div class="header">CMS Solutions</div>
                                                <div class="menu">
                                                    <a class="item">Rails</a>
                                                    <a class="item">Python</a>
                                                    <a class="item">PHP</a>
                                                </div>
                                            </div>
                                            <div class="item">
                                                <div class="header">Hosting</div>
                                                <div class="menu">
                                                    <a class="item">Shared</a>
                                                    <a class="item">Dedicated</a>
                                                </div>
                                            </div>
                                            <div class="item">
                                                <div class="header">Support</div>
                                                <div class="menu">
                                                    <a class="item">E-mail Support</a>
                                                    <a class="item">FAQs</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="twelve wide column">
                                    <div class="ui inverted segment">
                                        <div class="ui divided items">
                                            <div class="item">
                                                <div class="image">
                                                    <img src="/images/wireframe/image.png">
                                                </div>
                                            </div>
                                            <div class="item">
                                                <div class="image">
                                                    <img src="/images/wireframe/image.png">
                                                </div>
                                            </div>
                                            <div class="item">
                                                <div class="image">
                                                    <img src="/images/wireframe/image.png">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection