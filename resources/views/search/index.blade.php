@extends('templates.frontend')
@section('title', trans('common.search').' | '.config('app.name'))
@section('content')
    <div class="ui stackable grid">
        <div class="row">
            <div class="column">
                <div class="ui inverted tall stacked segment">
                    <h3 class="ui orange inverted dividing header">
                        <i class="search icon"></i>
                        <span class="content">
                            {{ trans('common.search') }}
                        </span>
                    </h3>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="column">
                <div class="ui inverted tall stacked segment">
                    <form action="{{ route('search.results') }}" method="get" class="ui inverted form">
                        <div class="field">
                            <label for="user">
                                {{ trans('common.user') }}
                            </label>
                            <input type="text" name="q" id="user" placeholder="{{ trans('common.user') }}" value="{{ old('q') }}">
                        </div>
                        <button type="submit" class="ui icon labeled button">
                            <i class="search icon"></i>
                            {{ trans('common.search') }}
                        </button>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection