@extends('templates.frontend')
@section('title', trans('common.search').' | '.config('app.name'))
@section('content')
    <div class="ui stackable grid">
        <div class="row">
            <div class="column">
                <div class="ui inverted tall stacked segment">
                    <h3 class="ui orange inverted dividing header">
                        <i class="search icon"></i>
                        <span class="content">
                            {{ trans('common.search') }}
                        </span>
                    </h3>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="twelve wide column">
                <div class="ui inverted tall stacked segment">
                    <div class="ui inverted divided items">
                    @foreach($results as $result)
                        <div class="item">
                            <a href="{{ route('profile', ['slug' => $result->slug_name]) }}" class="image">
                                <img src="{{ $result->userImage() }}" alt="{{ $result->getDisplayName() }}">
                            </a>
                            <div class="content">
                                <a href="{{ route('profile', ['slug' => $result->slug_name]) }}" class="header">
                                    {!! $result->getDisplayNameVerification() !!}
                                </a>
                                @if ($result->occupation)
                                <div class="meta">
                                    <span>{{ $result->occupation }}</span>
                                </div>
                                @endif
                                <div class="extra">
                                    <div class="ui right floated icon button">
                                        <i class="mail icon"></i>
                                    </div>
                                    <div class="ui right floated icon button">
                                        <i class="icons">
                                            <i class="user icon"></i>
                                            <i class="corner add icon"></i>
                                        </i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                    </div>
                </div>
            </div>
            <div class="four wide column">
                <div class="ui inverted tall stacked segment">
                    <a href="{{ route('search') }}" class="ui fluid icon labeled button">
                        <i class="search icon"></i>
                        {{ trans('another_search') }}
                    </a>
                </div>
            </div>
        </div>
    </div>
@endsection