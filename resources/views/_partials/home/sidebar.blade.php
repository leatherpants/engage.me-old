<div class="ui inverted tall stacked segments">
    <div class="ui inverted nopadding segment">
        <img src="{{ Auth::user()->userImage() }}" alt="{{ Auth::user()->getDisplayName() }}" class="ui fluid image">
        <div class="ui orange top left attached label">{!! Auth::user()->getDisplayNameVerification() !!}</div>
    </div>
    <div class="ui inverted nopadding segment">
        <div class="fluid ui vertical blocked buttons">
            @if (Auth::user()->type == 'all' || Auth::user()->type == 'company')
                <a href="{{ route('yourcompany') }}" class="ui orange blocked labeled icon button">
                    <i class="building icon"></i>
                    {{ trans('common.company.your_company') }}
                </a>
            @endif
        </div>
    </div>
</div>