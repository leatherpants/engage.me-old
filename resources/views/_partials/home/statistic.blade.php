<div class="ui inverted tall stacked segment">
    <h3 class="ui orange inverted dividing header">
        <i class="line chart icon"></i>
        <span class="content">
            {{ trans('common.statistics') }}
        </span>
    </h3>
    @if ($request)
        <a href="{{ route('contacts') }}" class="ui labeled button">
            <div class="ui orange blocked labeled icon button">
                <i class="users icon"></i>
                {{ $contacts }}
                {{ trans_choice('common.num_contacts', $contacts) }}
            </div>
            <div class="ui basic orange left pointing label">
                <i class="address book icon"></i>
                {{ $request }}
            </div>
        </a>
    @else
        <a href="{{ route('contacts') }}" class="ui orange fluid blocked labeled icon button">
            <i class="{{ ($contacts > 1) ? 'users' : 'user' }} icon"></i>
            {{ $contacts }} {{ trans_choice('common.num_contacts', $contacts) }}
        </a>
    @endif
    <a href="{{ route('applications') }}" class="ui orange fluid blocked labeled icon button">
            <i class="list icon"></i>
            {{ $applications }} {{ trans_choice('common.num_applications', $applications) }}
    </a>
    <a href="{{ route('messages') }}" class="ui orange fluid blocked labeled icon button">
        <i class="comments outline icon"></i>
        {{ trans('common.mail') }}
    </a>
</div>