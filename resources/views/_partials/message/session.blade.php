@if (session()->has('success'))
    <div class="ui inverted icon message">
        <i class="close icon"></i>
        <i class="info circle icon"></i>
        <div class="content">
            <p>{{ session()->get('success') }}</p>
        </div>
    </div>
@endif
