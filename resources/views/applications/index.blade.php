@extends('templates.frontend')
@section('title', trans('common.applications').' | '.config('app.name'))
@section('content')
    <div class="ui stackable grid">
        <div class="row">
            <div class="column">
                <div class="ui inverted tall stacked segments">
                    <div class="ui inverted right aligned segment">
                        <h3 class="ui orange inverted dividing header">
                            <i class="list icon"></i>
                            <span class="content">
                                {{ trans('common.applications') }}
                            </span>
                        </h3>
                        <a href="{{ route('applications.write') }}" class="ui floated orange icon labeled button">
                            <i class="write icon"></i>
                            {{ trans('applications.create') }}
                        </a>
                        <a href="{{ route('cv.cv') }}" target="_blank" class="ui floated orange icon labeled button">
                            <i class="file icon"></i>
                            {{ trans('cv.cv') }}
                        </a>
                        <a href="{{ route('cv.frontpage') }}" target="_blank" class="ui floated orange icon labeled button">
                            <i class="file icon"></i>
                            {{ trans('common.frontpage') }}
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="column">
                <div class="ui inverted segment">
                    <table class="ui inverted striped unstackable table">
                        <thead>
                            <tr>
                                <th>{{ trans('applications.title') }}</th>
                                <th>{{ trans('applications.company') }}</th>
                                <th>{{ trans('applications.type.type') }}</th>
                                <th>{{ trans('applications.status') }}</th>
                                <th>{{ trans('common.created') }}</th>
                                <th>{{ trans('applications.actions') }}</th>
                            </tr>
                        </thead>
                        <tbody>
                        @if (count($apps) > 0)
                            @foreach($apps as $app)
                                <tr>
                                    <td>{{ $app->title }}</td>
                                    <td>{{ $app->company->title }}</td>
                                    <td>{{ $app->type }}</td>
                                    <td>
                                        @if ($app->status == 1)
                                        Sent
                                        @elseif ($app->status == 2)
                                        Check
                                        @elseif ($app->status == 3)
                                        Declined
                                        @endif
                                    </td>
                                    <td>{{ $app->created_at->diffForHumans() }}</td>
                                    <td>
                                        <a href="{{ route('applications.view', ['company' => $app->company->slug, 'title' => $app->slug]) }}" class="ui orange icon button">
                                            <i class="unhide icon"></i>
                                        </a>
                                    </td>
                                </tr>
                            @endforeach
                        @else
                            <tr>
                                <td colspan="6" class="ui center aligned">{{ trans('error.no_applications') }}</td>
                            </tr>
                        @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection