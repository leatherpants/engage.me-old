@extends('templates.frontend')
@section('title', trans('common.application').' | '.config('app.name'))
@section('content')
    <div class="ui stackable grid">
        <div class="row">
            <div class="column">
                <div class="ui inverted tall stacked segment">
                    <h3 class="ui orange inverted dividing header">
                        {{ $app->title }}
                        <div class="sub header">
                            {{ $app->company->title }}
                        </div>
                    </h3>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="twelve wide column">
                <div class="ui inverted stacked segment"></div>
            </div>
            <div class="four wide column">
                <div class="ui inverted stacked segment">
                    <div class="ui inverted secondary pointing one item menu">
                        <span class="item">
                            <i class="info circle icon"></i>
                            {{ trans('applications.infos') }}
                        </span>
                    </div>
                    <div class="ui inverted list">
                        <div class="item">
                            <i class="building icon"></i>
                            <div class="content">
                                {{ $app->company->title }}
                            </div>
                        </div>
                        <div class="item">
                            <i class="road icon"></i>
                            <div class="content">
                                {{ $app->location->street }}
                                <div class="description">
                                    {{ $app->location->zip_code }} {{ $app->location->city }}
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <i class="info icon"></i>
                            <div class="content">
                                <a href="{{ route('companies.view', ['slug' => $app->company->slug]) }}">
                                    {{ trans('applications.site') }}
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="ui inverted secondary pointing one item menu">
                        <span class="item">
                            <i class="icon"></i>
                            {{ trans('applications.actions') }}
                        </span>
                    </div>
                    <div class="fluid ui vertical buttons">
                        <a href="{{ route('cv.all.letter', ['company' => $app->company->slug, 'slug' => $app->slug]) }}" target="_blank" class="ui orange labeled icon button">
                            <i class="file icon"></i>
                            {{ trans('common.all') }}
                        </a>
                        <a href="{{ route('cv.letter', ['company' => $app->company->slug, 'slug' => $app->slug]) }}" target="_blank" class="ui orange labeled icon button">
                            <i class="file icon"></i>
                            {{ trans('common.application') }}
                        </a>
                        <a href="{{ route('cv.cv') }}" target="_blank" class="ui orange labeled icon button">
                            <i class="file icon"></i>
                            {{ trans('common.cv') }}
                        </a>
                        <a href="{{ route('cv.frontpage') }}" target="_blank" class="ui orange labeled icon button">
                            <i class="file icon"></i>
                            {{ trans('common.frontpage') }}
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection