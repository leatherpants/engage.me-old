@extends('templates.frontend')
@section('title', trans('common.application').' | '.config('app.name'))
@section('content')
    <div class="ui stackable grid">
        <div class="row">
            <div class="column">
                <div class="ui inverted tall stacked segment">
                    <h3 class="ui orange inverted dividing header">
                        <i class="write icon"></i>
                        <span class="content">
                            {{ trans('applications.write') }}
                        </span>
                    </h3>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="column">
                <div class="ui inverted tall stacked segment">
                    <form action="" method="post" class="ui inverted form">
                        {{ csrf_field() }}
                        <div class="three fields">
                            <div class="field">
                                <label for="company">{{ trans('common.company.company') }}</label>
                                <select name="company_id" id="company">
                                    <option value="0">{{ trans('applications.choose_company') }}</option>
                                    @foreach($companies as $company)
                                        <option value="{{ $company->id }}">{{ $company->title }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="field">
                                <label for="location">{{ trans('common.company.location') }}</label>
                                <select name="location_id" id="location">
                                    <option value="0">{{ trans('applications.choose_location') }}</option>
                                    @foreach($locations as $location)
                                        <option value="{{ $location->id }}">{{ $location->zip_code }} {{ $location->city }} ({{ strtoupper($location->country) }}) {{ $location->company->title }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="field">
                                <label for="contact">{{ trans('common.company.contact') }}</label>
                                <select name="contact_id" id="contact">
                                    <option value="0">{{ trans('applications.choose_contact') }}</option>
                                    @foreach($contacts as $contact)
                                        <option value="{{ $contact->id }}">{{ $contact->name }} ({{ $contact->company->title }})</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="field">
                            <label for="title">{{ trans('applications.title') }}</label>
                            <input type="text" name="title" id="title" placeholder="{{ trans('applications.title') }}" value="{{ old('title') }}">
                        </div>
                        <div class="field">
                            <label for="content">{{ trans('applications.content') }}</label>
                            <textarea name="content" id="content" cols="30" rows="10" placeholder="{{ trans('applications.content') }}">{{ old('content') }}</textarea>
                        </div>
                        <div class="field">
                            <label for="type">{{ trans('applications.type.type') }}</label>
                            <select name="type" id="type">
                                <option value="mail">{{ trans('applications.type.mail') }}</option>
                                <option value="form">{{ trans('applications.type.form') }}</option>
                            </select>
                        </div>
                        <button class="ui orange labeled icon button">
                            <i class="save icon"></i>
                            {{ trans('common.save') }}
                        </button>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection