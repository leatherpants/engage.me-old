@extends('templates.blank')
@section('content')
    <div class="ui stackable grid">
        <div class="row">
            <div class="center aligned column">
                <h1 class="ui dividing header">
                    404
                </h1>
                <p class="ui header">
                    Sorry, that page doesn’t exist!
                </p>
            </div>
        </div>
    </div>
@endsection