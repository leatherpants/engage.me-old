<a href="{{ route('contacts.received') }}" class="ui fluid orange labeled icon contact button">
    <i class="user icon"></i>
    {{ trans('common.requests.received') }}
</a>