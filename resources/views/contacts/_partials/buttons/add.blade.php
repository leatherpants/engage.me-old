<a href="{{ route('contacts.add', ['userid' => $id]) }}" class="ui fluid orange labeled icon contact button">
    <i class="user icon"></i>
    {{ trans('common.contact.add') }}
</a>