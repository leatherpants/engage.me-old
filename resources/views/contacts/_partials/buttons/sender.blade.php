<a href="{{ route('contacts.sent') }}" class="ui fluid orange labeled icon contact button">
    <i class="user icon"></i>
    {{ trans('common.requests.sent') }}
</a>