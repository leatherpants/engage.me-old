<div class="ui inverted tall stacked segment" style="height: 100%;">
    <div class="ui inverted secondary pointing one item menu">
        <span class="item">
            <i class="line chart icon"></i>
            {{ trans('common.statistics') }}
        </span>
    </div>
    <div class="ui inverted list">
        <div class="item">
            <i class="users icon"></i>
            <div class="content">
                <div class="header">
                    {{ trans('common.contacts') }}
                </div>
                <div class="description">
                    {{ $amountOfContacts }}
                </div>
            </div>
        </div>
        <div class="item">
            <i class="users icon"></i>
            <div class="content">
                <div class="header">
                    {{ trans('common.requests.sent') }}
                </div>
                <div class="description">
                    {{ $amountOfSent }}
                </div>
            </div>
        </div>
        <div class="item">
            <i class="users icon"></i>
            <div class="content">
                <div class="header">
                    {{ trans('common.requests.received') }}
                </div>
                <div class="description">
                    {{ $amountOfReceived }}
                </div>
            </div>
        </div>
    </div>
</div>