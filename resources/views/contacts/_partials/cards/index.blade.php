<div class="ui card">
    <div class="image">
        <img src="{{ $contact->userImage() }}" alt="{{ $contact->getDisplayName() }}">
    </div>
    <div class="content">
        @if (!$contact->isActive)
        <div class="header">{!! $contact->getDisplayNameVerification() !!}</div>
        @else
        <a href="{{ route('profile', ['slug' => $contact->slug_name]) }}" class="header">{!! $contact->getDisplayNameVerification() !!}</a>
        @endif
        <div class="meta">
            <span class="date">Joined in 2017</span>
        </div>
    </div>
    <div class="extra content">
        <div class="ui two buttons">
            <a title="{{ trans('common.edit') }}" class="ui icon green button">
                <i class="edit icon"></i>
            </a>
            <a href="{{ route('contacts.delete', ['userid' => $contact->id]) }}" title="{{ trans('common.delete') }}" class="ui icon red button">
                <i class="delete icon"></i>
            </a>
        </div>
    </div>
</div>