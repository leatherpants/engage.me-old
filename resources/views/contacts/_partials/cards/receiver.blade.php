<div class="ui card">
    <div class="image">
        <img src="{{ $contact->userImage() }}" alt="{{ $contact->getDisplayName() }}">
    </div>
    <div class="content">
        <a href="{{ route('profile', ['slug' => $contact->slug_name]) }}" class="header">{!! $contact->getDisplayNameVerification() !!}</a>
        <div class="meta">
            <span class="date">Joined in {{ date('Y', strtotime($contact->created_at)) }}</span>
        </div>
    </div>
    <div class="extra content">
        <div class="ui two buttons">
            <a href="{{ route('contacts.accept', ['userid' => $contact->id]) }}" title="{{ trans('common.accept') }}" class="ui icon green button">
                <i class="checkmark icon"></i>
            </a>
            <a class="ui icon red button">
                <i class="remove icon"></i>
            </a>
        </div>
    </div>
</div>