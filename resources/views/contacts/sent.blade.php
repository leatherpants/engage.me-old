@extends('templates.frontend')
@section('title', trans('common.contacts').' | '.config('app.name'))
@section('content')
    <div class="ui stackable grid">
        <div class="row">
            <div class="column">
                <div class="ui inverted tall stacked segment">
                    <h3 class="ui orange inverted dividing header">
                        <i class="users icon"></i>
                        <span class="content">
                            {{ trans('common.contacts') }}
                        </span>
                    </h3>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="twelve wide column">
                <div class="ui inverted tall stacked segment">
                    <div class="ui inverted secondary pointing three item menu">
                        <a href="{{ route('contacts') }}" class="{{ Request::is('contacts') ? 'active ' : '' }}item">
                            {{ trans('common.contacts') }}
                        </a>
                        <a href="{{ route('contacts.sent') }}" class="{{ Request::is('contacts/sent') ? 'active ' : '' }}item">
                            {{ trans('common.requests.sent') }}
                        </a>
                        <a href="{{ route('contacts.received') }}" class="{{ Request::is('contacts/received') ? 'active ' : '' }}item">
                            {{ trans('common.requests.received') }}
                        </a>
                    </div>
                    @if ($amountOfSent > 0)
                    <div class="ui stackable three cards">
                        @foreach ($requestsTo as $contact)
                            @include('contacts._partials.cards.sender')
                        @endforeach
                    </div>
                    @else
                    <div class="ui inverted icon message">
                        <i class="warning circle icon"></i>
                        <div class="content">
                            <div class="header">
                                {{ trans('common.requests.sent') }}
                            </div>
                            <p>{{ trans('error.no_sent_requests') }}</p>
                        </div>
                    </div>
                    @endif
                </div>
            </div>
            <div class="four wide column">
                @include('contacts._partials.statistic')
            </div>
        </div>
    </div>
@endsection