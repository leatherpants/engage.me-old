@extends('templates.frontend')
@section('title', trans('common.notifications').' | '.config('app.name'))
@section('content')
    <div class="ui stackable grid">
        <div class="row">
            <div class="column">
                <div class="ui inverted tall stacked segment">
                    <h3 class="ui orange inverted dividing header">
                        <i class="bell icon"></i>
                        <span class="content">
                            {{ trans('common.notifications') }}
                        </span>
                    </h3>
                    <!--
                    <div class="ui orange icon message">
                        <i class="info circle icon"></i>
                        <div class="content">
                            <p>{{ trans('error.no_notifications') }}</p>
                        </div>
                    </div>
                    -->
                    <div class="ui inverted divided list">
                    @foreach($notifications as $notification)
                        <div class="item">
                            <i class="large user circle middle aligned icon"></i>
                            <div class="content">
                                <div class="header">You have been created your <a href="{{ route('profile', ['slug' => $notification->user->slug_name]) }}">profile</a> on engage.me. Thank you!</div>
                                <div class="description">{{ $notification->created_at->diffForHumans() }}</div>
                            </div>
                        </div>
                    @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection