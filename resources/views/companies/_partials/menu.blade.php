<div class="ui inverted segment">
    <div class="ui inverted secondary pointing menu">
        <a href="{{ route('companies.view', ['slug' => $company->slug]) }}" class="{{ Request::is('company/'.$company->slug) ? 'active ' : '' }}item">
            {{ trans('common.vacancies') }}
        </a>
        <a href="{{ route('companies.employees', ['slug' => $company->slug]) }}" class="{{ Request::is('company/'.$company->slug.'/employees') ? 'active ' : '' }}item">
            {{ trans('common.employees') }}
        </a>
        <a class="item disabled">
            {{ trans('common.recompanys') }}
        </a>
    </div>
</div>