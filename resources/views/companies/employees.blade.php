@extends('templates.frontend')
@section('title', $company->title.' | '.config('app.name'))
@section('content')
    <div class="ui stackable grid">
        <div class="row">
            <div class="sixteen wide column">
                <div class="ui segments">
                    <div class="ui inverted segment">
                        <h3 class="ui orange inverted dividing header">
                            {{ $company->title }}
                        </h3>
                    </div>
                    <div class="ui inverted tall stacked segment">
                        <div class="ui stackable three column grid">
                            <div class="column">
                                <div class="ui inverted list">
                                    <div class="item">
                                        <i class="large building middle aligned icon"></i>
                                        <div class="content">
                                            <div class="header">{{ $company->street }}</div>
                                            <div class="description">{{ $company->zip_code }} {{ $company->city }}</div>
                                        </div>
                                    </div>
                                    @if ($company->url)
                                        <div class="item">
                                            <i class="large external middle aligned icon"></i>
                                            <div class="content">
                                                <a href="{{ $company->url }}" target="_blank" rel="noopener" class="header">{{ $company->url }}</a>
                                            </div>
                                        </div>
                                    @endif
                                    @if ($company->email)
                                        <div class="item">
                                            <i class="large at middle aligned icon"></i>
                                            <div class="content">
                                                <div class="header">{{ $company->email }}</div>
                                            </div>
                                        </div>
                                    @endif
                                    @if ($company->phone)
                                        <div class="item">
                                            <i class="large phone middle aligned icon"></i>
                                            <div class="content">
                                                <div class="header">{{ $company->phone }}</div>
                                            </div>
                                        </div>
                                    @endif
                                </div>
                            </div>
                            <div class="column">
                                @if ($socials !== null)
                                    <div class="ui inverted middle aligned list">
                                        @foreach($socials as $social)
                                            @if ($social->social_provider == 1)
                                                <div class="item">
                                                    <i class="large facebook middle aligned icon"></i>
                                                    <div class="content">
                                                        <a href="{{ $social->url }}" target="_blank" class="header">{{ str_replace('https://www.facebook.com/', '', $social->url) }}</a>
                                                    </div>
                                                </div>
                                            @endif
                                            @if ($social->social_provider == 2)
                                                <div class="item">
                                                    <i class="large twitter middle aligned icon"></i>
                                                    <div class="content">
                                                        <a href="{{ $social->url }}" target="_blank" class="header">{{ str_replace('https://www.twitter.com/', '', $social->url) }}</a>
                                                    </div>
                                                </div>
                                            @endif
                                            @if ($social->social_provider == 3)
                                                <div class="item">
                                                    <i class="large instagram middle aligned icon"></i>
                                                    <div class="content">
                                                        <a href="{{ $social->url }}" target="_blank" class="header">{{ str_replace('https://www.instagram.com/', '', $social->url) }}</a>
                                                    </div>
                                                </div>
                                            @endif
                                            @if ($social->social_provider == 4)
                                                <div class="item">
                                                    <i class="large youtube middle aligned icon"></i>
                                                    <div class="content">
                                                        <a href="{{ $social->url }}" target="_blank" class="header">{{ $social->youtubeShortner() }}</a>
                                                    </div>
                                                </div>
                                            @endif
                                            @if ($social->social_provider == 5)
                                                <div class="item">
                                                    <i class="large linkedin middle aligned icon"></i>
                                                    <div class="content">
                                                        <a href="{{ $social->url }}" target="_blank" class="header">{{ str_replace('https://www.linkedin.com/', '', $social->url) }}</a>
                                                    </div>
                                                </div>
                                            @endif
                                        @endforeach
                                    </div>
                                @endif
                            </div>
                            <div class="column">
                                <img src="{{ $company->image }}" alt="{{ $company->title }}" class="ui centered small logo image">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="sixteen wide column">
                <div class="ui inverted segments">
                    <div class="ui inverted segment">
                        <h3 class="ui orange inverted dividing header">{{ trans('common.about_us') }}</h3>
                    </div>
                    <div class="ui inverted segment">
                        <div class="ui stackable grid">
                            <div class="row">
                                @isset($detail)
                                    <div class="twelve wide column">
                                        <div class="ui inverted nopadding segment">
                                            @if ($detail)
                                                {!! $detail->content !!}
                                            @endif
                                        </div>
                                    </div>
                                @endisset
                                @isset ($company->details->owner_id)
                                    <div class="four wide column">
                                        <div class="ui inverted nopadding center aligned segment">
                                            <img src="{{ $company->details->owner->userImage() }}" class="ui image" alt="">
                                            <p>{!! trans('common.company.managed_by', ['url' => route('profile', ['slug' => $company->details->owner->slug_name ]), 'display_name' => $company->details->owner->getDisplayName()]) !!}</p>
                                        </div>
                                    </div>
                                @endisset
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="column">
                <div class="ui segments">
                    @include('companies._partials.menu')
                    <div class="ui inverted segment">
                        <h3 class="ui orange inverted dividing header">{{ trans('common.employees') }}</h3>
                    </div>
                    <div class="ui inverted tall stacked segment">
                        <div class="ui inverted divided list">
                        @foreach($employees as $employee)
                            <div class="item">
                                <img src="{{ $employee->user->userImage() }}" alt="{{ $employee->user->getDisplayName() }}" class="ui avatar image">
                                <div class="content">
                                    <a href="{{ route('profile', ['slug' => $employee->user->slug_name]) }}" class="header">
                                        {{ $employee->user->getDisplayName() }}
                                    </a>
                                    Position
                                </div>
                            </div>
                        @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection