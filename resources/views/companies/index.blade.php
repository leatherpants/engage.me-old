@extends('templates.frontend')
@section('title', trans('common.companies').' | '.config('app.name'))
@section('content')
    <div class="ui stackable grid">
        <div class="row">
            <div class="column">
                <div class="ui inverted tall stacked segment">
                    <h3 class="ui inverted dividing orange header">
                        <i class="building icon"></i>
                        <span class="content">
                            {{ trans('common.companies') }}
                        </span>
                    </h3>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="column">
                <div class="ui inverted tall stacked segment">
                    <div class="ui inverted divided items">
                    @foreach($companies as $company)
                        <div class="item">
                            @if ($company->image)
                            <div class="ui tiny image">
                                <img src="{{ $company->image }}" alt="{{ $company->title }}">
                            </div>
                            @endif
                            <div class="content">
                                <a href="{{ route('companies.view', ['slug' => $company->slug]) }}" class="header">
                                    {{ $company->title }}
                                </a>
                                <div class="meta">
                                    {{ $company->city }}, {{ trans('countries.'.strtolower($company->name)) }}
                                </div>
                            </div>
                        </div>
                    @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection