@extends('templates.blank')
@section('title', config('app.name'))
@section('content')
    <div class="ui middle aligned center aligned grid">
        <div class="column">
            <h2 class="ui orange image header">
                <img src="{{ asset('assets/logo.png') }}" alt="{{ config('app.name') }}" class="image logo">
            </h2>
            <form action="{{ route('signup') }}" method="post" class="ui large form">
                {{ csrf_field() }}
                <div class="ui left aligned stacked segment">
                    <div class="{{ $errors->has('first_name') ? 'error ' : '' }}field">
                        <label for="first_name">{{ trans('common.first_name') }}</label>
                        <div class="ui left icon input">
                            <i class="user icon"></i>
                            <input type="text" name="first_name" id="first_name" placeholder="{{ trans('common.first_name') }}" value="{{ old('first_name') }}">
                        </div>
                    </div>
                    <div class="{{ $errors->has('last_name') ? 'error ' : '' }}field">
                        <label for="last_name">{{ trans('common.last_name') }}</label>
                        <div class="ui left icon input">
                            <i class="user icon"></i>
                            <input type="text" name="last_name" id="last_name" placeholder="{{ trans('common.last_name') }}" value="{{ old('last_name') }}">
                        </div>
                    </div>
                    <div class="{{ $errors->has('email') ? 'error ' : '' }}field">
                        <label for="email">{{ trans('common.email') }}</label>
                        <div class="ui left icon input">
                            <i class="user icon"></i>
                            <input type="email" name="email" id="email" placeholder="{{ trans('common.email') }}" value="{{ old('email') }}">
                        </div>
                    </div>
                    <div class="{{ $errors->has('password') ? 'error ' : '' }}field">
                        <label for="password">{{ trans('common.password') }}</label>
                        <div class="ui left icon input">
                            <i class="lock icon"></i>
                            <input type="password" name="password" id="password" placeholder="{{ trans('common.password') }}" autocomplete="off">
                        </div>
                    </div>
                    <button type="submit" class="ui fluid large orange submit button">{{ trans('common.signup') }}</button>
                </div>
            </form>
            @if (count($errors)>0)
                <div class="ui error message stacked segment">
                    @include('_partials.message.error')
                </div>
            @endif
        </div>
    </div>
@endsection
@section('style')
    <style type="text/css">
        body {
            background-color: #DADADA;
        }
        body > .grid {
            height: 100%;
        }
        .image {
            margin-top: -100px;
        }
        .column {
            max-width: 450px;
        }
        .image.logo {
            margin-right: 0!important;
            width: 12em!important;
        }
    </style>
@endsection
@section('script')
    <script type="text/javascript">
        $('.ui.checkbox')
            .checkbox()
        ;
    </script>
@endsection