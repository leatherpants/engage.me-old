@extends('templates.blank')
@section('title', config('app.name'))
@section('content')
    <div class="ui middle aligned center aligned grid">
        <div class="column">
            <h2 class="ui orange image header">
                <img src="{{ asset('assets/logo.png') }}" alt="{{ config('app.name') }}" class="image logo">
            </h2>
            <form action="{{ route('login') }}" method="post" class="ui large form">
                {{ csrf_field() }}
                <div class="ui left aligned stacked segment">
                    <div class="{{ $errors->has('email') ? 'error ' : '' }}field">
                        <label for="email">{{ trans('common.email') }}</label>
                        <div class="ui left icon input">
                            <i class="user icon"></i>
                            <input type="email" name="email" id="email" placeholder="{{ trans('common.email') }}">
                        </div>
                    </div>
                    <div class="{{ $errors->has('password') ? 'error ' : '' }}field">
                        <label for="password">{{ trans('common.password') }}</label>
                        <div class="ui left icon input">
                            <i class="lock icon"></i>
                            <input type="password" name="password" id="password" placeholder="{{ trans('common.password') }}" autocomplete="off">
                        </div>
                    </div>
                    <div class="field">
                        <div class="ui checkbox">
                            <input type="checkbox" name="remember" id="remember" {{ old('remember') ? ' checked' : '' }} tabindex="0" class="hidden">
                            <label for="remember">{{ trans('common.remember_me') }}</label>
                        </div>
                    </div>
                    <button type="submit" class="ui fluid large orange submit button">{{ trans('common.signin') }}</button>
                </div>
            </form>
            @if (count($errors)>0)
                <div class="ui error message stacked segment">
                    @include('_partials.message.error')
                </div>
            @endif

            <div class="ui message stacked segment">
                New to us? <a href="{{ route('signup') }}">Sign Up</a>
            </div>
        </div>
    </div>
@endsection
@section('style')
    <style type="text/css">
        body {
            background-color: #DADADA;
        }
        body > .grid {
            height: 100%;
        }
        .image {
            margin-top: -100px;
        }
        .column {
            max-width: 450px;
        }
        .image.logo {
            margin-right: 0!important;
            width: 12em!important;
        }
    </style>
@endsection
@section('script')
    <script type="text/javascript">
        $('.ui.checkbox')
            .checkbox()
        ;
    </script>
@endsection