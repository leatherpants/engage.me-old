@extends('templates.frontend')
@section('title', trans('common.help').' | '.config('app.name'))
@section('content')
    <div class="ui stackable grid">
        <div class="row">
            <div class="column">
                <div class="ui inverted segments">
                    <div class="ui inverted segment">
                        <h3 class="ui orange inverted dividing header">
                            <i class="help circle outline icon"></i>
                            <span class="content">
                                {{ trans('common.help') }}
                            </span>
                        </h3>
                    </div>
                    <div class="ui inverted segment">
                        <form action="#" class="ui inverted disabled form">
                            <div class="field">
                                <label for="help">Hi, how can we help you?</label>
                                <div class="ui fluid action input">
                                    <input type="text" id="help" placeholder="{{ trans('common.search') }}">
                                    <button class="ui orange labeled icon button">
                                        <i class="search icon"></i>
                                        {{ trans('common.search') }}
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="ui inverted segment">
                        <div class="ui orange five item inverted labeled icon menu">
                            <a class="item">
                                <i class="user icon"></i>
                                {{ trans('common.user') }}
                            </a>
                            <a href="{{ route('bug') }}" class="item">
                                <i class="bug icon"></i>
                                {{ trans('common.bugs') }}
                            </a>
                            <a class="item">
                                <i class="lock icon"></i>
                                {{ trans('common.policies') }}
                            </a>
                            <a href="{{ route('support') }}" class="item">
                                <i class="life ring icon"></i>
                                {{ trans('support.support') }}
                            </a>
                            <a href="{{ route('forum') }}" class="item">
                                <i class="list icon"></i>
                                {{ trans('common.forum') }}
                            </a>
                        </div>
                    </div>
                    <div class="ui inverted segment">
                        <h4 class="ui orange inverted dividing header">
                            <i class="help circle outline icon"></i>
                            <span class="content">
                                xxx
                            </span>
                        </h4>
                        <div class="ui inverted items">
                            <div class="item">
                                <div class="content">
                                    <a class="header">Cute Dog</a>
                                    <div class="description">
                                        <p>xxx</p>
                                        <p>xxx</p>
                                    </div>
                                    <div class="extra">
                                        <i class="green check icon"></i>
                                        121 Votes
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection