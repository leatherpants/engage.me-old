<div class="ui inverted vertical fluid menu">
    <div class="item">
        <div class="menu">
            <a href="{{ route('backend.settings') }}" class="item">{{ trans('common.settings') }}</a>
        </div>
    </div>
    <div class="item">
        <div class="menu">
            <a href="{{ route('backend.languages') }}" class="item">{{ trans('common.language') }}</a>
        </div>
        <div class="menu">
            <a href="{{ route('backend.settings') }}" class="item">{{ trans('common.system') }}</a>
        </div>
    </div>
</div>