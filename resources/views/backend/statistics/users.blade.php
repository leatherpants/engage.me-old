@extends('templates.backend')
@section('title', trans('common.backend').' | '.config('app.name'))
@section('content')
    <div class="ui stackable grid">
        <div class="row">
            <div class="column">
                <div class="ui inverted segment">
                    <h3 class="ui inverted dividing orange header">
                        <i class="line chart icon"></i>
                        <span class="content">
                            {{ trans('stats.statistics') }}
                            <span class="sub header">{{ trans('common.users') }}</span>
                        </span>
                    </h3>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="column">
                <div class="ui inverted segment">
                    <div class="ui four column stackable grid">
                        <div class="column">{{ trans('stats.total') }} <div class="ui orange label">{{ $total }}</div></div>
                        <div class="column">{{ trans('stats.yearly') }} <div class="ui orange label">{{ $yearly }}</div></div>
                        <div class="column">{{ trans('stats.monthly') }} <div class="ui orange label">{{ $monthly }}</div></div>
                        <div class="column">{{ trans('stats.daily') }} <div class="ui orange label">{{ $daily }}</div></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="column">
                <div class="ui inverted segment"></div>
            </div>
        </div>
    </div>
@endsection