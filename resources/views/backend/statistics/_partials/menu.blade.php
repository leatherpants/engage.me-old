<div class="ui inverted divided list">
    <div class="item">
        <i class="list icon"></i>
        <div class="content">
            <div class="header">{{ trans('common.applications') }}</div>
        </div>
    </div>
    <div class="item">
        <i class="building icon"></i>
        <div class="content">
            <div class="header">{{ trans('common.company.company') }}</div>
        </div>
    </div>
    <div class="item">
        <i class="newspaper icon"></i>
        <div class="content">
            <div class="header">{{ trans('common.jobs') }}</div>
        </div>
    </div>
    <div class="item">
        <i class="users icon"></i>
        <a href="{{ route('backend.statistics.users') }}" class="content">
            <div class="header">{{ trans('stats.users') }}</div>
        </a>
    </div>
</div>