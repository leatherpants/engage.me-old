@extends('templates.backend')
@section('title', trans('common.backend').' | '.config('app.name'))
@section('content')
    <div class="ui stackable grid">
        <div class="row">
            <div class="column">
                <div class="ui inverted segment">
                    <h3 class="ui inverted dividing orange header">
                        <i class="line chart icon"></i>
                        <span class="content">
                            {{ trans('stats.statistics') }}
                        </span>
                    </h3>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="column">
                <div class="ui inverted segment">
                    @include('backend.statistics._partials.menu')
                </div>
            </div>
        </div>
    </div>
@endsection