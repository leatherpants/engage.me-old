<div class="ui inverted segment">
    <h3 class="ui orange inverted dividing header">
        <i class="line chart icon"></i>
        <span class="content">
                            {{ trans('common.statistics') }}
                        </span>
    </h3>
    <div class="ui orange inverted statistics">
        <div class="statistic">
            <div class="value">
                <i class="building icon"></i>
                {{ number_format($companies->count(), 0, '.', '.') }}
            </div>
            <a href="{{ route('backend.companies') }}" class="label">
                {{ trans('common.companies') }}
            </a>
        </div>
        <div class="statistic">
            <div class="value">
                <i class="users icon"></i>
                {{ number_format($users, 0, '.', '.') }}
            </div>
            <a href="{{ route('backend.users') }}" class="label">
                {{ trans('common.users') }}
            </a>
        </div>
        <div class="statistic">
            <div class="value">
                <i class="newspaper icon"></i>
                {{ number_format($jobs->count(), 0, '.', '.') }}
            </div>
            <div class="label">
                {{ trans('common.jobs') }}
            </div>
        </div>
        <div class="statistic">
            <div class="value">
                <i class="list icon"></i>
                {{ $applications }}
            </div>
            <div class="label">
                {{ trans('common.applications') }}
            </div>
        </div>
        <div class="statistic">
            <div class="value">
                <i class="calendar icon"></i>
                0
            </div>
            <div class="label">
                {{ trans('common.events') }}
            </div>
        </div>
        <div class="statistic">
            <div class="value">
                <i class="comments outline icon"></i>
                0
            </div>
            <div class="label">
                {{ trans('common.feedbacks') }}
            </div>
        </div>
        <div class="statistic">
            <div class="value">
                <i class="bug icon"></i>
                0
            </div>
            <div class="label">
                {{ trans('common.bugs') }}
            </div>
        </div>
        <div class="statistic">
            <div class="value">
                <i class="life ring icon"></i>
                {{ $support }}
            </div>
            <div class="label">
                {{ trans('support.support') }}
            </div>
        </div>
    </div>
</div>