<div class="four wide column">
    <div class="ui inverted segment">
        <h3 class="ui inverted orange dividing header">
            <i class="users icon"></i>
            <span class="content">
                {{ trans('common.latest_users') }}
            </span>
        </h3>
        <div class="ui inverted divided list">
            @foreach($latests['users'] as $user)
                <div class="item">
                    <div class="content">
                        {{ $user->getDisplayName() }}
                        <div class="sub description">
                            {{ $user->created_at->diffForHumans() }}
                            <a href="{{ route('profile', ['slug_name' => $user->slug_name]) }}" target="_blank" title="{{ $user->getDisplayName() }}"><i class="external icon"></i></a>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
        <a href="{{ route('backend.users') }}" class="ui orange fluid labeled icon button">
            <i class="users icon"></i>
            {{ trans('common.view_all_users') }}
        </a>
    </div>
</div>
<div class="four wide column">
    <div class="ui inverted segment" style="height: 100%">
        <h3 class="ui inverted orange dividing header">
            <i class="building icon"></i>
            <span class="content">
                {{ trans('common.latest_companies') }}
            </span>
        </h3>
        <div class="ui inverted divided list">
            @if (!$latests['companies'])
            @foreach($latests['companies'] as $company)
                <div class="item">
                    <div class="content">
                        {{ $company->title }}
                        <div class="sub description">
                            {{ $company->created_at->diffForHumans() }}
                            <a href="{{ route('companies.view', ['slug_name' => $company->slug]) }}" target="_blank" title="{{ $company->title }}"><i class="external icon"></i></a>
                        </div>
                    </div>
                </div>
            @endforeach
            @endif
        </div>
        <a href="{{ route('backend.companies') }}" class="ui orange fluid labeled icon button">
            <i class="building icon"></i>
            {{ trans('common.view_all_companies') }}
        </a>
    </div>
</div>
<div class="four wide column">
    <div class="ui inverted segment" style="height: 100%">
        <h3 class="ui inverted orange dividing header">
            <i class="newspaper icon"></i>
            <span class="content">
                {{ trans('common.latest_jobs') }}
            </span>
        </h3>
        <div class="ui inverted divided list">
            @if (!$latests['jobs'])
            @foreach($latests['jobs'] as $job)
                <div class="item">
                    <div class="content">
                        {{ $job->title }}
                        <div class="sub description">
                            {{ $job->created_at->diffForHumans() }}
                            <a href="{{ route('jobs.view', ['company' => $job->company->slug, 'slug' => $job->slug]) }}" target="_blank" title="{{ $job->title }}"><i class="external icon"></i></a>
                        </div>
                    </div>
                </div>
            @endforeach
            @endif
        </div>
        <a href="#" class="ui orange fluid labeled icon button">
            <i class="newspaper icon"></i>
            {{ trans('common.view_all_jobs') }}
        </a>
    </div>
</div>
<div class="four wide column">
    <div class="ui inverted segment" style="height: 100%">
        <h3 class="ui inverted orange dividing header">
            <i class="calendar icon"></i>
            <span class="content">
                {{ trans('common.latest_events') }}
            </span>
        </h3>
        <a href="#" class="ui orange fluid labeled icon button">
            <i class="calendar icon"></i>
            {{ trans('common.view_all_events') }}
        </a>
    </div>
</div>