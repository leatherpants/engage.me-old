@extends('templates.backend')
@section('title', trans('common.backend').' | '.config('app.name'))
@section('content')
    <div class="ui stackable grid">
        <div class="row">
            <div class="column">
                <div class="ui inverted segment">
                    <h3 class="ui orange inverted dividing header">
                        <i class="life ring icon"></i>
                        <span class="content">
                            {{ trans('support.support') }}
                        </span>
                    </h3>
                </div>
                @include('backend.support._partials.infobar')
                <div class="ui inverted segment">
                    <div class="ui stackable grid">
                        <div class="row">
                            <div class="two wide column">
                                @include('backend.support._partials.sidebar')
                            </div>
                            <div class="fourteen wide column">
                                <div class="ui inverted divided list">
                                    @foreach($tickets as $ticket)
                                        <div class="item">
                                            <div class="content">
                                                <a href="{{ route('backend.support.ticket', ['id' => $ticket->id]) }}" class="header">
                                                    {{ $ticket->title }}
                                                </a>
                                                Created {{ $ticket->created_at->diffForHumans() }} by {{ $ticket->creator->getDisplayName() }}
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection