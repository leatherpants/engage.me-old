<div class="ui inverted fluid vertical menu">
    <div class="item">
        <div class="header"><i class="help circle icon"></i>{{ trans('common.help') }}</div>
        <div class="menu">
            <a class="item">{{ trans('common.categories') }}</a>
        </div>
    </div>
    <div class="item">
        <div class="header"><i class="reply all icon"></i>{{ trans('common.forum') }}</div>
        <div class="menu">
            <a class="item">{{ trans('common.categories') }}</a>
        </div>
    </div>
    <div class="item">
        <div class="header"><i class="life ring icon"></i>{{ trans('support.support') }}</div>
        <div class="menu">
            <a href="{{ route('backend.support') }}" class="{{ Request::is('backend/support') ? 'active ' : ''  }}item">{{ trans('support.all_tickets') }}</a>
            <a href="{{ route('backend.support.assigned') }}" class="{{ Request::is('backend/support/assigned') ? 'active ' : ''  }}item">{{ trans('support.your_tickets') }}</a>
            <a href="{{ route('backend.support.closed') }}" class="{{ Request::is('backend/support/closed') ? 'active ' : ''  }}item">{{ trans('support.closed_tickets') }}</a>
        </div>
    </div>
</div>