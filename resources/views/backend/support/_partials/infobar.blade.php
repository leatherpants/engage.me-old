<div class="ui inverted segment">
    <div class="ui stackable grid">
        <div class="one wide column">
            <div class="ui orange icon label">
                <i class="info circle icon"></i>
            </div>
        </div>
        <div class="two wide column">
            {{ trans('support.all_tickets') }}
            <div class="ui orange label">
                {{ $requestsCount }}
            </div>
        </div>
        <div class="two wide column">
            {{ trans('support.your_ticket') }}
            <div class="ui orange label">
                {{ $assignedCount }}
            </div>
        </div>
        <div class="two wide column">
            {{ trans('support.closed_tickets') }}
            <div class="ui orange label">
                {{ $closedTickets }}
            </div>
        </div>
    </div>
</div>