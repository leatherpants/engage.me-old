@extends('templates.backend')
@section('title', trans('common.backend').' | '.config('app.name'))
@section('content')
    <div class="ui stackable grid">
        <div class="row">
            <div class="column">
                <div class="ui inverted segment">
                    <h3 class="ui orange inverted dividing header">
                        <i class="life ring icon"></i>
                        <span class="content">
                            {{ trans('support.support') }}
                        </span>
                    </h3>
                </div>
                <div class="ui inverted segment">
                    <div class="ui stackable grid">
                        <div class="row">
                            <div class="two wide column">
                                @include('backend.support._partials.sidebar')
                            </div>
                            <div class="fourteen wide column">
                                <h3 class="ui orange inverted dividing header">
                                    <i class="ticket icon"></i>
                                    <span class="content">
                                        #{{ $ticket->id }}. {{ $ticket->title }}
                                        <span class="sub header">
                                            {{ $ticket->created_at->diffForHumans() }} -
                                            {{ $ticket->creator->display_name }}
                                            <a href="{{ route('profile', ['slug_name' => $ticket->creator->slug_name]) }}" target="_blank" title="{{ $ticket->creator->display_name }}">
                                                <i class="user icon"></i>
                                            </a>
                                            <a href="{{ route('backend.users.view', ['slug_name' => $ticket->creator->slug_name]) }}" title="{{ $ticket->creator->display_name }}">
                                                <i class="settings icon"></i>
                                            </a>
                                        </span>
                                    </span>
                                </h3>
                                <div class="ui right floated buttons">
                                    <div class="ui orange button">
                                        <i class="share icon"></i>
                                        {{ trans('support.assign') }}
                                    </div>
                                    <div class="ui orange button">
                                        <i class="reply all icon"></i>
                                        {{ trans('common.assignToAnother') }}
                                    </div>
                                    <div class="ui red button">
                                        <i class="close icon"></i>
                                        {{ trans('common.close') }}
                                    </div>
                                </div>
                                <div class="ui inverted icon message">
                                    <i class="info circle icon"></i>
                                    <p>{{ $ticket->content }}</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection