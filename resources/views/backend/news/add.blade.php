@extends('templates.backend')
@section('title', trans('common.backend').' | '.config('app.name'))
@section('content')
    <div class="ui stackable grid">
        <div class="row">
            <div class="column">
                <div class="ui inverted segment">
                    <h3 class="ui orange inverted dividing header">
                        <i class="write icon"></i>
                        <span class="content">
                            {{ trans('news.write') }}
                        </span>
                    </h3>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="column">
                <div class="ui inverted segment">
                    <form action="" class="ui inverted form">
                        <div class="field">
                            <label for="title">{{ trans('news.title') }}</label>
                            <input type="text" name="title" id="title" placeholder="{{ trans('news.title') }}" value="{{ old('title') }}">
                        </div>
                        <div class="field">
                            <label for="content">{{ trans('news.content') }}</label>
                            <textarea name="content" id="content" cols="30" rows="10" placeholder="{{ trans('news.content') }}">{{ old('content') }}</textarea>
                        </div>
                        <button class="ui orange labeled icon button">
                            <i class="save icon"></i>
                            {{ trans('common.save') }}
                        </button>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection