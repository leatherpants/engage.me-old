@extends('templates.backend')
@section('title', trans('common.backend').' | '.config('app.name'))
@section('content')
    <div class="ui stackable grid">
        <div class="row">
            <div class="column">
                <div class="ui inverted clearing segment">
                    <h3 class="ui orange inverted dividing header">
                        <i class="write icon"></i>
                        <span class="content">
                            {{ trans('common.news') }}
                        </span>
                    </h3>
                    <a href="{{ route('backend.news.add') }}" class="ui right floated orange icon labeled button">
                        <i class="write icon"></i>
                        {{ trans('news.create') }}
                    </a>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="column">
                <table class="ui striped inverted table">
                    <thead>
                        <tr>
                            <th>Topic</th>
                            <th>Author</th>
                            <th>{{ trans('common.language') }}</th>
                            <th>{{ trans('common.created') }}</th>
                            <th>{{ trans('common.actions') }}</th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach($stories as $story)
                    <tr>
                        <td>{{ $story->title }}</td>
                        <td>
                            <img src="{{ $story->user->userImage() }}" alt="{{ $story->user->getDisplayName() }}" class="ui avatar image">
                            <span>{!! $story->user->getDisplayNameVerification() !!}</span>
                        </td>
                        <td>
                            {{ trans('lang.'.$story->lang) }}
                        </td>
                        <td>
                            {{ $story->created_at->diffForHumans() }}
                        </td>
                        <td>
                            <a href="{{ route('backend.news.edit', ['id' => $story->id]) }}" class="ui orange icon button">
                                <i class="edit icon"></i>
                            </a>
                        </td>
                    </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection