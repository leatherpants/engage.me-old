@extends('templates.backend')
@section('title', trans('common.backend').' | '.config('app.name'))
@section('content')
    <div class="ui stackable grid">
        <div class="row">
            <div class="column">
                <div class="ui inverted segment">
                    <h3 class="ui orange inverted dividing header">
                        <i class="translate icon"></i>
                        <span class="content">
                            {{ trans('common.language') }}
                        </span>
                    </h3>
                </div>
                <div class="ui inverted segment">
                    <div class="ui grid">
                        <div class="row">
                            <div class="two wide column">
                                @include('backend.settings._partials.sidebar')
                            </div>
                            <div class="fourteen wide column">
                                <div class="ui inverted divided list">
                                    @foreach($languages as $language)
                                        <div class="item">
                                            <div class="right floated content">
                                            @if ($language->active == 1)
                                                <form action="{{ route('backend.languages') }}" method="post">
                                                    {{ csrf_field() }}
                                                    <input type="hidden" name="code" value="{{ $language->code }}">
                                                    <input type="hidden" name="active" value="{{ $language->active }}">
                                                    <button class="ui inverted icon button">
                                                        <i class="green circle check icon"></i>
                                                    </button>
                                                </form>
                                            @else
                                                <form action="{{ route('backend.languages') }}" method="post">
                                                    {{ csrf_field() }}
                                                    <input type="hidden" name="code" value="{{ $language->code }}">
                                                    <input type="hidden" name="active" value="{{ $language->active }}">
                                                    <button class="ui inverted icon button">
                                                        <i class="red circle remove icon"></i>
                                                    </button>
                                                </form>
                                            @endif
                                            </div>
                                            <div class="content">
                                                <i class="{{ $language->getFlag() }} flag"></i> {{ trans('lang.'.$language->name) }}
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection