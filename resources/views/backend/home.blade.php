@extends('templates.backend')
@section('title', trans('common.backend').' | '.config('app.name'))
@section('content')
    <div class="ui stackable grid">
        <div class="row">
            <div class="column">
                @include('backend._partials.statistics')
            </div>
        </div>
        <div class="row">
            @include('backend._partials.latest')
        </div>
        <div class="row">
            <div class="four wide column">
                <div class="ui inverted segment">
                    <h3 class="ui orange inverted dividing header">
                        Developments
                    </h3>
                    <div class="ui divided list">
                        <div class="item">
                            <i class="external icon"></i>
                            <div class="content">
                                <a href="http://beta.engageme.granath/" target="_blank">engage.me BETA</a>
                            </div>
                        </div>
                        <div class="item">
                            <i class="external icon"></i>
                            <div class="content">
                                <a href="http://hr.engageme.granath/" target="_blank">engage.me HR</a>
                            </div>
                        </div>
                        <div class="item">
                            <i class="external icon"></i>
                            <div class="content">
                                <a href="http://support.engageme.granath/" target="_blank">engage.me Support</a>
                            </div>
                        </div>
                        <div class="item">
                            <i class="external icon"></i>
                            <div class="content">
                                <a href="http://vue.engage.me/" target="_blank">engage.me VUE</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="twelve wide column">
                <div class="ui inverted segment">
                    <a href="{{ route('backend.help') }}" class="ui orange icon button" style="margin: 0!important;">
                        <i class="massive help circle outline icon" style="height: auto;"></i>
                    </a>
                    <a href="{{ route('backend.settings') }}" class="ui orange icon button" style="margin: 0">
                        <i class="massive settings icon" style="height: auto"></i>
                    </a>
                </div>
            </div>
        </div>
    </div>
@endsection