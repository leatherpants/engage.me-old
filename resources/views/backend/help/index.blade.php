@extends('templates.backend')
@section('title', trans('common.backend').' | '.config('app.name'))
@section('content')
    <div class="ui stackable grid">
        <div class="row">
            <div class="column">
                <div class="ui inverted segment">
                    <h3 class="ui orange inverted dividing header">
                        <i class="help circle outline icon"></i>
                        <span class="content">
                            {{ trans('common.help') }}
                        </span>
                    </h3>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="two wide column">
                <div class="ui inverted segment">
                    @include('backend.support._partials.sidebar')
                </div>
            </div>
        </div>
    </div>
@endsection