@extends('templates.backend')
@section('title', trans('common.backend').' | '.config('app.name'))
@section('content')
    <div class="ui stackable grid">
        <div class="row">
            <div class="column">
                <div class="ui inverted segment">
                    <h3 class="ui orange inverted dividing header">
                        <i class="user icon"></i>
                        <span class="content">
                            {!! $user->getDisplayNameVerification() !!}
                            <span class="sub header">{{ trans('support.support') }}</span>
                        </span>
                    </h3>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="four wide column">
                @include('backend.user._partials.sidebar')
            </div>
            <div class="twelve wide column">
                <div class="ui inverted segment">
                    <h3 class="ui orange inverted dividing header">
                        <i class="life ring icon"></i>
                        <span class="content">
                            {{ trans('support.support') }}
                        </span>
                    </h3>
                    <div class="ui inverted divided list">
                        @if ($tickets->count())
                        @foreach($tickets as $ticket)
                            <div class="item">
                                <div class="right floated content">
                                    <a href="{{ route('backend.support.ticket', ['id' => $ticket->id]) }}">
                                        <i class="external icon"></i>
                                    </a>
                                </div>
                                <div class="content">
                                    <div class="header">
                                        {{ $ticket->title }}
                                    </div>
                                    {{ $ticket->created_at->diffForHumans() }}
                                </div>
                            </div>
                        @endforeach
                        @else
                            <div class="item">
                                <div class="content">
                                    {{ trans('error.no_tickets') }}
                                </div>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection