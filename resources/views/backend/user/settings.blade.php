@extends('templates.backend')
@section('title', trans('common.backend').' | '.config('app.name'))
@section('content')
    <div class="ui stackable grid">
        <div class="row">
            <div class="column">
                <div class="ui inverted segment">
                    <h3 class="ui orange inverted dividing header">
                        <i class="user icon"></i>
                        <span class="content">
                            {!! $user->getDisplayNameVerification() !!}
                            <div class="sub header">{{ trans('common.settings') }}</div>
                        </span>
                    </h3>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="four wide column">
                @include('backend.user._partials.sidebar')
            </div>
            <div class="twelve wide column">
                <div class="ui inverted segments">
                    <div class="ui inverted segment">
                        <div class="ui inverted form">
                            <div class="field">
                                <label for="lang">{{ trans('common.language') }}</label>
                                <i class="{{ $user->lang }} flag"></i> {{ trans('lang.'.$user->lang) }}
                            </div>
                            <div class="two fields">
                                <div class="field">
                                @if ($user->isActive == 1)
                                    <a class="ui red fluid blocked labeled icon button">
                                        <i class="lock icon"></i>
                                        Lock user {{--TODO--}}
                                    </a>
                                @else
                                    <a href="" class="ui green fluid blocked labeled icon button">
                                        <i class="unlock icon"></i>
                                        Unlock user {{--Todo--}}
                                    </a>
                                @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection