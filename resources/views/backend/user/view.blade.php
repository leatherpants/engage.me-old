@extends('templates.backend')
@section('title', trans('common.backend').' | '.config('app.name'))
@section('content')
    <div class="ui stackable grid">
        <div class="row">
            <div class="column">
                <div class="ui inverted segment">
                    <h3 class="ui orange inverted dividing header">
                        <i class="user icon"></i>
                        <span class="content">
                            {!! $user->getDisplayNameVerification() !!}
                            <span class="sub header">
                                {{ trans('common.settings') }}
                            </span>
                        </span>
                    </h3>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="four wide column">
                @include('backend.user._partials.sidebar')
            </div>
            <div class="twelve wide column">
                <div class="ui inverted segments">
                    <div class="ui inverted segment">
                        <div class="ui stackable three column grid">
                            <div class="column">
                                <i class="user icon"></i>ID
                                {{ $user->id }}
                            </div>
                            <div class="column">
                                <i class="time icon"></i>
                                {{ ucfirst(trans('common.registered_ago', ['ago' => $user->created_at->diffForHumans()])) }}
                            </div>
                            <div class="column">
                                <i class="time icon"></i>
                                {{ ucfirst(trans('common.updated_ago', ['ago' => $user->updated_at->diffForHumans()])) }}
                            </div>
                        </div>
                    </div>
                    <div class="ui inverted segment">
                        <div class="ui stackable grid">
                            <div class="eight wide column">
                                <form action="#" class="ui inverted form">
                                    <div class="field">
                                        <label for="user">{{ trans('common.display_name') }} <small>({{ $user->getDisplayName() }})</small></label>
                                        <div class="two fields">
                                            <div class="field">
                                                <div class="ui left icon input">
                                                    <i class="user icon"></i>
                                                    <input type="text" name="first_name" id="first_name" placeholder="{{ trans('common.first_name') }}" value="{{ $user->first_name }}">
                                                </div>
                                            </div>
                                            <div class="field">
                                                <div class="ui left icon input">
                                                    <i class="user icon"></i>
                                                    <input type="text" name="last_name" id="last_name" placeholder="{{ trans('common.last_name') }}" value="{{ $user->last_name }}">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div class="eight wide column">
                                <form action="#" class="ui inverted form">
                                    <div class="field">
                                        <label for="type">{{ trans('common.account_type') }}</label>
                                        <select name="type" id="type">
                                            <option value="all"{{ $user->type == 'all' ? 'selected' : '' }}>{{ trans('common.type.all') }}</option>
                                            <option value="company"{{ $user->type == 'company' ? 'selected' : '' }}>{{ trans('common.type.company') }}</option>
                                            <option value="user"{{ $user->type == 'user' ? 'selected' : '' }}>{{ trans('common.type.user') }}</option>
                                        </select>
                                    </div>
                                </form>
                            </div>
                            <div class="eight wide column">
                                <form action="#" class="ui inverted form">
                                    <div class="field">
                                        <label for="birthday">{{ trans('common.birthday') }} <small>({{ $user->userBirthday() }})</small></label>
                                        <div class="ui left icon input">
                                            <i class="birthday icon"></i>
                                            <input type="text" name="birthday" id="birthday" placeholder="{{ trans('common.birthday') }}" value="{{ ($user->birthday !== null) ? date('d.m.Y', strtotime($user->birthday)) : '' }}">
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div class="eight wide column">
                                <form action="#" class="ui inverted form">
                                    <div class="field">
                                        <label for="location">{{ trans('common.location') }} <small>({{ $user->userLocation() }})</small></label>
                                        <div class="ui left icon input">
                                            <i class="map icon"></i>
                                            <input type="text" name="location" id="location" placeholder="{{ trans('common.location') }}" value="{{ $user->city }}">
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="ui inverted segment">
                        <div class="ui stackable grid">
                            <div class="sixteen wide column">
                                <form action="#" class="ui inverted form">
                                    <div class="field">
                                        <label for="email">{{ trans('common.email') }}</label>
                                        <div class="ui left icon input">
                                            <i class="at icon"></i>
                                            <input type="text" name="email" id="email" placeholder="{{ trans('common.email') }}" value="{{ $user->email }}">
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div class="eight wide column">
                                <button data-toggle="modal" data-target="#reset_password" class="ui reset orange labeled icon fluid button">
                                    <i class="lock icon"></i>
                                    Forgot password
                                </button>
                                @include('backend.user._partials.modal.reset_password')
                            </div>
                            <div class="eight wide column">
                                <button data-toggle="modal" data-target="#verification" class="ui veri orange labeled icon fluid button">
                                    <i class="print icon"></i> {{ trans('common.verificationmail') }}
                                </button>
                                @include('backend.user._partials.modal.verification')
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')
<script type="text/javascript">
    $(document).ready(function () {
        $('#reset_password')
            .modal('attach events', '.reset.button', 'show')
        ;

        $('#verification')
            .modal('attach events', '.veri.button', 'show')
        ;
    });
</script>
@endsection