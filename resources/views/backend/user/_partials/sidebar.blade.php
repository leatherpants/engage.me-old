<div class="ui inverted segments">
    <div class="ui inverted nopadding segment">
        <img src="{{ $user->userImage() }}" alt="{{ $user->getDisplayName() }}" class="ui image">
    </div>
    <div class="ui inverted nopadding segment">
        <div class="fluid ui vertical blocked buttons">
            <a href="{{ route('backend.users') }}" class="ui orange fluid labeled icon button">
                <i class="users icon"></i>
                {{ trans('common.users') }}
            </a>
            <a href="{{ route('backend.users.view', ['slug' => $user->slug_name]) }}" class="ui orange fluid labeled icon button">
                <i class="user icon"></i>
                {{ $user->getDisplayName() }}
            </a>
            <a href="{{ route('backend.users.settings', ['slug' => $user->slug_name]) }}" class="ui orange fluid labeled icon button">
                <i class="settings icon"></i>
                {{ trans('common.settings') }}
            </a>
            <a href="{{ route('backend.users.support', ['slug' => $user->slug_name]) }}" class="ui orange fluid labeled icon button">
                <i class="life ring icon"></i>
                {{ trans('support.support') }}
            </a>
        </div>
    </div>
</div>