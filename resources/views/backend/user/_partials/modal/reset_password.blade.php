<div id="reset_password" class="ui modal">
    <i class="close icon"></i>
    <div class="header">
        Reset password mail
    </div>
    <div class="content">
        <p>
            <strong>{!! $user->getDisplayName() !!}</strong> want to reset the account password.
        </p>
        <p>
            Do you want to sent a <strong>Reset password mail</strong> to <strong>{{ $user->email }}</strong>?
        </p>
    </div>
    <div class="actions">
        <button type="button" class="ui negative labeled icon button" data-dismiss="modal">
            <i class="remove icon"></i>
            {{ trans('common.no') }}
        </button>
        <button type="button" class="ui positive labeled icon button">
            <i class="check icon"></i>
            {{ trans('common.yes') }}
        </button>
    </div>
</div>