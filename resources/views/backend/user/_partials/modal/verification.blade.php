<div id="verification" class="ui modal">
    <i class="close icon"></i>
    <div class="header">
        Verification mail for registration
    </div>
    <div class="content">
        <p>
            <strong>{!! $user->getDisplayName() !!}</strong> hasn't received any verification mail.
        </p>
        <p>
            Do you want to send another verification mail to <strong>{{ $user->email }}</strong>?
        </p>
    </div>
    <div class="actions">
        <button type="button" class="ui negative labeled icon button" data-dismiss="modal">
            <i class="remove icon"></i>
            {{ trans('common.no') }}
        </button>
        <button type="button" class="ui positive labeled icon button">
            <i class="check icon"></i>
            {{ trans('common.yes') }}
        </button>
    </div>
</div>