@extends('templates.backend')
@section('title', trans('common.backend').' | '.config('app.name'))
@section('content')
    <div class="ui stackable grid">
        <div class="row">
            <div class="column">
                <div class="ui inverted segment">
                    <h3 class="ui orange inverted dividing header">
                        <i class="users icon"></i>
                        <span class="content">
                            {{ trans('common.users') }}
                        </span>
                    </h3>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="column">
                <table class="ui striped inverted table">
                    <thead>
                        <tr>
                            <th></th>
                            <th>{{ trans('common.display_name') }}</th>
                            <th>{{ trans('common.group') }}</th>
                            <th>{{ trans('common.account_type') }}</th>
                            <th>{{ trans('common.registered') }}</th>
                            <th>{{ trans('common.actions') }}</th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach($users as $user)
                        <tr>
                            <td><img src="{{ $user->userImage() }}" alt="{{ $user->getDisplayName() }}" class="ui mini centered image"></td>
                            <td>
                                {!! $user->variousDisplayName() !!}<br>
                                <small>{{ $user->email }}</small>
                            </td>
                            <td>
                                @if ($user->isAdmin == 1)
                                    {{ trans('common.admin') }}
                                @else
                                    {{ trans('common.user') }}
                                @endif
                            </td>
                            <td>
                                {{ trans('common.type.'.$user->type) }}<br>
                                <small>{{ $user->isBetaUser() }}</small>
                            </td>
                            <td>
                                {{ $user->created_at->diffForHumans() }}
                            </td>
                            <td>
                                <div class="ui orange buttons">
                                    <a href="{{ route('profile', ['slug' => $user->slug_name]) }}" target="_blank" class="ui icon button">
                                        <i class="user icon"></i>
                                    </a>
                                    <a href="{{ route('backend.users.view', ['slug' => $user->slug_name]) }}" class="ui icon button">
                                        <i class="edit icon"></i>
                                    </a>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        <div class="row">
            <div class="right aligned column">
                {{ $users->links() }}
            </div>
        </div>
    </div>
@endsection