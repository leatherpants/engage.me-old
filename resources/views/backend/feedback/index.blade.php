@extends('templates.backend')
@section('title', trans('common.backend').' | '.config('app.name'))
@section('content')
    <div class="ui stackable grid">
        <div class="row">
            <div class="column">
                <div class="ui clearing inverted segment">
                    <h3 class="ui orange inverted dividing header">
                        <i class="comments outline icon"></i>
                        <span class="content">
                            {{ trans('common.feedback') }}
                        </span>
                    </h3>
                    <form action="" class="ui right floated form">
                        <div class="field">
                            <select name="status">
                                <option value="">{{ trans('common.status.all') }}</option>
                                <option value="">{{ trans('common.status.completed') }}</option>
                                <option value="">{{ trans('common.status.open') }}</option>
                            </select>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="column">
                <table class="ui striped inverted table">
                    <thead>
                        <tr>
                            <th>Topic</th>
                            <th>Author</th>
                            <th>Created</th>
                            <th>{{ trans('common.actions') }}</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>xxx</td>
                            <td>
                                <img src="http://media.engageme.granath/images/sandra-granath.jpg" alt="Sandra Granath" class="ui avatar image">
                                <span>Sandra Granath</span>
                            </td>
                            <td>
                                10 minutes ago
                            </td>
                            <td>
                                <div class="ui buttons">
                                    <div class="ui icon green button">
                                        <i class="check icon"></i>
                                    </div>
                                    <div class="ui icon orange button">
                                        <i class="edit icon"></i>
                                    </div>
                                </div>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection