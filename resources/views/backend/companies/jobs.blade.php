@extends('templates.backend')
@section('title', trans('common.backend').' | '.config('app.name'))
@section('content')
    <div class="ui stackable grid">
        <div class="row">
            <div class="column">
                <div class="ui inverted segment">
                    <h3 class="ui orange inverted dividing header">
                        <i class="newspaper icon"></i>
                        <span class="content">
                            {{ trans('common.jobs_by', ['company' => $company->title]) }}
                        </span>
                    </h3>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="four wide column">
                <div class="ui inverted segment">
                    <a href="{{ route('backend.companies.company', ['slug' => $company->slug]) }}" class="ui orange fluid labeled icon button">
                        <i class="left arrow icon"></i>
                        {{ trans('common.company.company') }}
                    </a>
                </div>
                <div class="ui inverted segment">
                    <h3 class="ui orange inverted dividing header">
                        <i class="line chart icon"></i>
                        <span class="content">
                            {{ trans('common.statistics') }}
                        </span>
                    </h3>
                    <div class="ui inverted divided list">
                        <div class="item">
                            <div class="content">
                                open
                            </div>
                        </div>
                        <div class="item">
                            <div class="content">
                                closed
                            </div>
                        </div>
                        <div class="item">
                            <div class="content">
                                total
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="twelve wide column">
                <div class="ui inverted segment">
                    <table class="ui inverted striped table">
                        <thead>
                            <tr>
                                <th>{{ trans('common.job') }}</th>
                                <th>{{ trans('applications.type.type') }}</th>
                                <th>{{ trans('company.location.location') }}</th>
                                <th>{{ trans('common.created') }}</th>
                                <th>{{ trans('common.actions') }}</th>
                            </tr>
                        </thead>
                        <tbody>
                        @foreach($jobs as $job)
                            <tr>
                                <td>{{ $job->title }}</td>
                                <td></td>
                                <td>{{ $job->location->city }} <i class="{{ $job->location->country }} flag"></i></td>
                                <td>{{ $job->created_at->diffForHumans() }}</td>
                                <td>
                                    <button class="ui orange tiny icon button">
                                        <i class="unhide icon"></i>
                                    </button>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection