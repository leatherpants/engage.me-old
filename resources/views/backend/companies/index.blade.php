@extends('templates.backend')
@section('title', trans('common.backend').' | '.config('app.name'))
@section('content')
    <div class="ui stackable grid">
        <div class="row">
            <div class="column">
                <div class="ui inverted segment">
                    <h3 class="ui orange inverted dividing header">
                        <i class="building icon"></i>
                        <span class="content">
                            {{ trans('common.companies') }}
                        </span>
                    </h3>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="column">
                <table class="ui striped inverted table">
                    <thead>
                        <tr>
                            <th>{{ trans('common.name') }}</th>
                            <th>{{ trans('common.registered') }}</th>
                            <th>{{ trans('common.actions') }}</th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach($companies as $company)
                        <tr>
                            <td>{{ $company->title }}</td>
                            <td>{{ $company->created_at->diffForHumans() }}</td>
                            <td>
                                <div class="ui orange buttons">
                                    <a href="{{ route('companies.view', ['slug' => $company->slug]) }}" target="_blank" class="ui icon button">
                                        <i class="building icon"></i>
                                    </a>
                                    <a href="{{ route('backend.companies.company', ['slug' => $company->slug]) }}" class="ui icon button">
                                        <i class="edit icon"></i>
                                    </a>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection