<div class="ui inverted segment">
    <h4 class="ui orange inverted dividing header">
        {{ trans('common.jobs') }}
    </h4>
    @if (!count($jobs))
    <p>{{ $company->title }} has no vacancies at the moment.</p>
    @else
        <table class="ui inverted striped table">
            <thead>
                <tr>
                    <th>{{ trans('common.job') }}</th>
                    <th>{{ trans('company.location.location') }}</th>
                    <th>{{ trans('common.created') }}</th>
                </tr>
            </thead>
            <tbody>
            @foreach($jobs as $job)
                <tr>
                    <td>{{ $job->title }}</td>
                    <td>{{ $job->location->city }} <i class="{{ $job->location->country }} flag"></i></td>
                    <td>{{ $job->created_at->diffForHumans() }}</td>
                </tr>
            @endforeach
            </tbody>
        </table>
    @endif
    <a href="{{ route('backend.company.jobs', ['slug' => $company->slug]) }}" class="ui orange labeled icon button">
        <i class="newspaper icon"></i>
        {{ trans('common.jobs') }}
    </a>
</div>