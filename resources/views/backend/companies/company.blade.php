@extends('templates.backend')
@section('title', trans('common.backend').' | '.config('app.name'))
@section('content')
    <div class="ui stackable grid">
        <div class="row">
            <div class="column">
                <div class="ui inverted segment">
                    <h3 class="ui orange inverted dividing header">
                        <i class="building icon"></i>
                        <span class="content">
                            {{ $company->title }}
                        </span>
                    </h3>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="twelve wide column">
                <div class="ui inverted tall stacked segment">
                    <h4 class="ui orange inverted dividing header">
                        {{ trans('company.description') }}
                    </h4>
                    @if ($company->details->content)
                        {!! $company->details->content !!}
                    @else
                        <p>{{ trans('common.no_desc') }}</p>
                    @endif
                    <div class="ui orange labeled icon button">
                        <i class="edit icon"></i>
                        {{ trans('company.details.edit') }}
                    </div>
                </div>
                @include('backend.companies._partials.jobs')
            </div>
            <div class="four wide column">
                @if ($company->details->owner_id)
                <div class="ui inverted segment">
                    <h4 class="ui orange inverted dividing header">
                        {{ trans('company.profile_owner') }}
                    </h4>
                    <div>
                        <img src="{{ $company->details->owner->userImage() }}" alt="{{ $company->details->owner->getDisplayName() }}" class="ui avatar image">
                        <span>{!! $company->details->owner->getDisplayNameVerification() !!}</span>
                    </div>
                    <a href="{{ route('profile', ['slug' => $company->details->owner->slug_name]) }}" target="_blank" class="ui orange fluid labeled icon button" style="margin-top: 1em;">
                        <i class="user icon"></i>
                        {{ trans('common.user') }}
                    </a>
                </div>
                @endif
                <div class="ui inverted segment">
                    <a href="{{ route('companies.view', ['slug' => $company->slug]) }}" target="_blank" class="ui orange fluid labeled icon button">
                        <i class="building icon"></i>
                        {{ trans('common.company.company') }}
                    </a>
                </div>
                <div class="ui inverted segment">
                    <h4 class="ui orange inverted dividing header">
                        {{ trans('company.location.location') }}
                    </h4>
                    <div class="ui inverted list">
                        <div class="item">
                            <div class="content">
                                {{ trans('company.location.address') }}
                                <div class="description">
                                    {{ $location->street }}<br>
                                    {{ $location->zip_code }} {{ $location->city }}
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="content">
                                {{ trans('company.location.country') }}
                                <div class="description">
                                    <i class="{{ $location->country }} flag"></i> {{ trans('countries.'.strtolower($location->countries->name)) }}
                                </div>
                            </div>
                        </div>
                    </div>
                    <a href="{{ route('backend.company.location', ['slug' => $company->slug]) }}" class="ui orange fluid labeled icon button">
                        <i class="building icon"></i>
                        {{ trans('company.location.edit') }}
                    </a>
                </div>
                <div class="ui inverted segment">
                    <h4 class="ui orange inverted dividing header">
                        {{ trans('company.socialmedia.socialmedia') }}
                    </h4>
                    @if (!count($socials))
                        <div class="ui inverted message">
                            <p class="center aligned">{{ trans('error.no_socialmedia') }}</p>
                        </div>
                    @else
                    <div class="ui list">
                    @foreach($socials as $social)
                        <div class="item">
                            <i class="{{ $social->socialProvider() }} icon"></i>
                            <div class="content">
                                {{ $social->url }}
                                <div class="description"><a onclick="GetSocialEntry({{ $social->id }})">{{ trans('common.social_media.edit') }}</a></div>
                            </div>
                        </div>
                    @endforeach
                    </div>
                    <div class="social_content"></div>
                    @endif
                    <button type="button" data-toggle="modal" data-target="#social-add" class="ui orange test fluid labeled icon button">
                        <i class="edit icon"></i>
                        {{ trans('company.socialmedia.edit') }}
                    </button>
                </div>
            </div>
        </div>
    </div>
    @include('backend.companies.modal.social-add')
    @include('backend.companies.modal.social-edit')
@endsection
@section('script')
    <script type="text/javascript">
        $('#social-add')
            .modal('attach events', '.test.button', 'show')
        ;

        function addSocialEntry() {
            var company_id = $('#company_id').val();
            var social_provider = $('#social_provider').val();
            var url = $("#url").val();

            $.post('/api/social/add', {
                company_id: company_id,
                social_provider: social_provider,
                url: url
            }, function (data, status) {
                $("#social-add").modal('hide');

                $('#company_id').val("");
                $('#social_provider').val("");
                $('#url').val("");
            });
        }

        function GetSocialEntry(id) {
            $("#hidden_id").val(id);
            $.get('/api/social/' + id, {},
            function (data, status) {
                var social = data;

                $("#id").val(social.id);
                $("#company_id").val(social.company_id);
                $("#social_provider").val(social.social_provider);
                $("#update_url").val(social.url);
            });
            $("#social-edit").modal('show');
        }

        function updateSocialEntry() {
            var company_id = $('#update_company_id').val();
            var social_provider = $('#update_social_provider').val();
            var url = $("#update_url").val();

            var id = $("#hidden_id").val();

            $.post('/api/social/' + id + '/update', {
                id: id,
                company_id: company_id,
                social_provider: social_provider,
                url: url
            },
            function (data, status) {
                $("#social-edit").modal('hide');
            });
        }

    </script>
@endsection