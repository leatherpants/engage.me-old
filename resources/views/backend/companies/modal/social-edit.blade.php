<div class="ui inverted modal" id="social-edit">
    <i class="close icon"></i>
    <div class="header">
        {{ trans('common.social_media.edit') }}
    </div>
    <div class="content">
        <form method="post" class="ui form">
            {{ csrf_field() }}
            <input type="hidden" name="company_id" id="update_company_id" value="{{ $company->id }}">
            <div class="field">
                <label for="update_social_provider">{{ trans('common.social_provider') }}</label>
                <select name="social_provider" id="update_social_provider">
                    <option value="1">Facebook</option>
                    <option value="2">Twitter</option>
                </select>
            </div>
            <div class="field">
                <label for="update_url">{{ trans('common.url') }}</label>
                <input type="url" id="update_url" placeholder="{{ trans('common.url') }}">
            </div>
        </form>
    </div>
    <div class="actions">
        <button type="button" class="ui cancel labeled icon button" data-dismiss="modal">
            <i class="remove icon"></i>
            Nope
        </button>
        <button type="button" class="ui positive labeled icon button" onclick="updateSocialEntry()">
            <i class="checkmark icon"></i>
            {{ trans('common.save') }}
        </button>
        <input type="hidden" id="hidden_id">
    </div>
</div>