<div class="ui modal" id="social-add">
    <i class="close icon"></i>
    <div class="header">
        {{ trans('common.social_media.add') }}
    </div>
    <div class="content">
        <form method="post" class="ui form">
            {{ csrf_field() }}
            <input type="hidden" name="company_id" id="company_id" value="{{ $company->id }}">
            <div class="field">
                <label for="social_provider">{{ trans('common.social_provider') }}</label>
                <select name="social_provider" id="social_provider">
                    <option value="1">Facebook</option>
                    <option value="2">Twitter</option>
                </select>
            </div>
            <div class="field">
                <label for="url">{{ trans('common.url') }}</label>
                <input type="url" name="url" id="url" placeholder="{{ trans('common.url') }}">
            </div>
        </form>
    </div>
    <div class="actions">
        <button type="button" class="ui cancel labeled icon button" data-dismiss="modal">
            <i class="remove icon"></i>
            Nope
        </button>
        <button type="button" class="ui positive labeled icon button" onclick="addSocialEntry()">
            <i class="checkmark icon"></i>
            {{ trans('common.save') }}
        </button>
    </div>
</div>