@extends('templates.backend')
@section('title', trans('common.backend').' | '.config('app.name'))
@section('content')
    <div class="ui stackable grid">
        <div class="row">
            <div class="column">
                <div class="ui inverted tall stacked segment">
                    <h3 class="ui orange inverted dividing header">
                        <i class="building icon"></i>
                        <span class="content">
                            {{ $company->title }}
                            <div class="sub header">
                                {{ trans('company.location.location') }}
                            </div>
                        </span>
                    </h3>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="column">
                <div class="ui inverted tall stacked segment">
                    <form action="{{ route('backend.company.location', ['slug' => $company->slug]) }}" method="post" class="ui inverted form">
                        {{ csrf_field() }}
                        <div class="field">
                            <label for="street">{{ trans('company.location.street') }}</label>
                            <input type="text" name="street" id="street" placeholder="{{ trans('company.location.street') }}" value="{{ $location->street }}">
                        </div>
                        <div class="three fields">
                            <div class="four wide field">
                                <label for="zip_code">{{ trans('company.location.zip_code') }}</label>
                                <input type="text" name="zip_code" id="zip_code" placeholder="{{ trans('company.location.zip_code') }}" value="{{ $location->zip_code }}">
                            </div>
                            <div class="eight wide field">
                                <label for="city">{{ trans('company.location.city') }}</label>
                                <input type="text" name="city" id="city" placeholder="{{ trans('company.location.city') }}" value="{{ $location->city }}">
                            </div>
                            <div class="four wide field">
                                <label for="country">{{ trans('company.location.country') }}</label>
                                <select name="country" id="country">
                                @foreach($countries as $country)
                                    <option value="{{ $country->code }}" {{ ($country->code == $location->country) ? ' selected' : '' }}>{{ trans('countries.'.strtolower($country->name)) }}</option>
                                @endforeach
                                </select>
                            </div>
                        </div>
                        <button class="ui orange labeled icon button">
                            <i class="save icon"></i>
                            {{ trans('common.save') }}
                        </button>
                        <a href="{{ route('backend.companies.company', ['slug' => $company->slug]) }}" class="ui orange labeled icon button">
                            <i class="left arrow icon"></i>
                            {{ trans('common.company.company') }}
                        </a>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection