<div class="ui sidebar inverted workin vertical menu left">
    <div class="item">
        <div class="header">
            ENGAGE ME
        </div>
    </div>
    <div class="item">
        <div class="menu">
            <a href="{{ route('backend.companies') }}" class="item">{{ trans('common.companies') }}</a>
            <a href="{{ route('backend') }}" class="item">{{ trans('common.events') }}</a>
            <a href="{{ route('backend.feedback') }}" class="item">{{ trans('common.feedback') }}</a>
            <a href="{{ route('backend.news') }}" class="item">{{ trans('common.news') }}</a>
            <a href="{{ route('backend.users') }}" class="item">{{ trans('common.users') }}</a>
            <a href="{{ route('backend.statistics') }}" class="item">{{ trans('common.statistics') }}</a>
        </div>
    </div>
    <div class="item">
        <div class="menu">
            <a href="{{ route('profile', ['slug_name' => Auth::user()->slug_name]) }}" class="item">{{ Auth::user()->first_name }}</a>
            <a href="{{ route('index') }}" class="item">{{ trans('common.frontend') }}</a>
            <a href="{{ route('signout') }}" class="item">{{ trans('common.signout') }}</a>
        </div>
    </div>
</div>