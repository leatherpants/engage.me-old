<div class="ui sidebar vertical left inverted menu">
    <div class="ui item center aligned container">
        <a href="{{ route('profile', ['slug' => Auth::user()->slug_name]) }}">
            <img src="{{ Auth::user()->userImage() }}" class="ui image" alt="{{ Auth::user()->getDisplayName() }}">
            <p>{!! Auth::user()->getDisplayNameVerification() !!}</p>
        </a>
    </div>
    <div class="item">
        <div class="menu">
            <a href="{{ route('contacts') }}" class="{{ (Request::is('contacts') || Request::is('contacts/*')) ? 'active ' : '' }}item">
                <i class="users icon"></i>
                {{ trans('common.contacts') }}
            </a>
            <a href="{{ route('notifications') }}" class="{{ (Request::is('notifications')) ? 'active ' : '' }}item">
                <i class="bell icon"></i>
                {{ trans('common.notifications') }}
            </a>
            <a href="{{ route('settings') }}" class="{{ (Request::is('settings') || Request::is('settings/*')) ? 'active ' : '' }}item">
                <i class="setting icon"></i>
                {{ trans('common.settings') }}
            </a>
            @if (Auth::user()->isAdmin)
            <a href="{{ route('backend') }}" class="item">
                <i class="dashboard icon"></i>
                {{ trans('common.backend') }}
            </a>
            @endif
            <a href="{{ route('signout') }}" class="item">
                <i class="sign out icon"></i>
                {{ trans('common.signout') }}
            </a>
        </div>
    </div>
    <div class="item">
        <div class="menu">
            <a href="{{ route('companies') }}" class="{{ (Request::is('company') || Request::is('company/*')) ? 'active ' : '' }}item">
                <i class="building icon"></i>
                {{ trans('common.companies') }}
            </a>
        </div>
    </div>
    <div class="item">
        <div class="menu">
            <a href="{{ route('jobs') }}" class="{{ Request::is('jobs') || Request::is('jobs/*') ? 'active ' : '' }}item">
                <i class="write icon"></i>
                {{ trans('common.jobs') }}
            </a>
        </div>
    </div>
    @if (Auth::user()->type == 'user' || Auth::user()->type == 'all')
    <div class="item">
        <div class="menu">
            <a href="{{ route('applications') }}" class="{{ Request::is('applications') || Request::is('applications/*') ? 'active ' : '' }}item">
                <i class="list icon"></i>
                {{ trans('common.applications') }}
            </a>
        </div>
    </div>
    @endif
</div>