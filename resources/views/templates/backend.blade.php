<!DOCTYPE html>
<html lang="{{ App::getLocale() }}">
<head>
    <title>@yield('title')</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="theme-color" content="#F2711C">
    <link rel="stylesheet" href="{{ asset('assets/semantic.min.css') }}" type="text/css">
    <link rel="stylesheet" href="{{ mix('assets/style.css') }}" type="text/css">
    <style type="text/css">
        .ui.menu .item img.logo {
            width: 10em!important;
        }
        .main.container {
            margin-top: 4em;
        }
        .main.fluid.container {
            padding: 0 1em;
        }
        .fixed.inverted.menu .toc.item {
            display: none;
        }
        @media only screen and (max-width: 700px) {
            .fixed.inverted.menu .item,
            .fixed.inverted.menu .menu {
                display: none;
            }
            .fixed.inverted.menu .item.header {
                display: block;
            }
            .fixed.inverted.menu .toc.item {
                display: block;
            }
        }
    </style>
    @yield('style')
</head>
<body>
@include('templates._partials.mobile_menu_backend')
<div class="pusher">
    <div class="ui top fixed inverted menu">
        <div class="ui fluid container">
            <a class="toc icon item">
                <i class="sidebar icon"></i>
            </a>
            <a href="{{ route('backend') }}" class="header item">
                <img src="{{ asset('assets/logo.png') }}" alt="{{ config('app.name') }}" class="logo">
            </a>
            <a href="{{ route('backend.companies') }}" class="{{ (Request::is('backend/companies') || Request::is('backend/companies/*')) ? 'active orange ' : '' }}item">
                {{ trans('common.companies') }}
            </a>
            <a href="#" class="item">
                {{ trans('common.events') }}
            </a>
            <a href="{{ route('backend.feedback') }}" class="{{ (Request::is('backend/feedback') || Request::is('backend/feedback/*')) ? 'active orange ' : '' }}item">
                {{ trans('common.feedback') }}
            </a>
            <a href="{{ route('backend.news') }}" class="{{ (Request::is('backend/news') || Request::is('backend/news/*')) ? 'active orange ' : '' }}item">
                {{ trans('common.news') }}
            </a>
            <a href="{{ route('backend.users') }}" class="{{ (Request::is('backend/users') || Request::is('backend/users/*')) ? 'active orange ' : '' }}item">
                {{ trans('common.users') }}
            </a>
            <a href="{{ route('backend.statistics') }}" class="{{ (Request::is('backend/statistics') || Request::is('backend/statistics/*')) ? 'active orange ' : '' }}item">
                {{ trans('common.statistics') }}
            </a>
            <div class="right menu">
                <a href="{{ route('home') }}" class="item">
                    {{ trans('common.frontend') }}
                </a>
                <a href="{{ route('signout') }}" class="item">
                    {{ trans('common.signout') }}
                </a>
            </div>
        </div>
    </div>
    <div class="ui main fluid container">
        @yield('content')
    </div>
</div>
<script src="{{ asset('assets/jquery.min.js') }}"></script>
<script src="{{ asset('assets/semantic.min.js') }}"></script>
<script>
    $(document)
        .ready(function() {

            // create sidebar and attach to menu open
            $('.ui.sidebar')
                .sidebar('attach events', '.toc.item')
            ;

        })
    ;
</script>
@yield('script')
</body>
</html>