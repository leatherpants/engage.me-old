<!DOCTYPE html>
<html lang="{{ App::getLocale() }}">
<head>
    <title>@yield('title')</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <script>window.Laravel = { csrfToken: '{{ csrf_token() }}' }</script>
    <link rel="stylesheet" href="{{ asset('assets/semantic.min.css') }}" type="text/css">
    <link rel="stylesheet" href="{{ mix('assets/style.css') }}" type="text/css">
    <style type="text/css">
        .ui.menu .item img.logo {
            width: 10em!important;
        }
        .main.container {
            margin-top: 7em;
        }
        .fixed.inverted.menu .toc.item {
            display: none;
        }
        .ui.footer.segment {
            margin: 5em 0 0;
            padding: 5em 0;
        }
        @media only screen and (max-width: 700px) {
            .fixed.inverted.menu .item,
            .fixed.inverted.menu .menu {
                display: none;
            }
            .fixed.inverted.menu .item.header {
                display: block;
            }
            .fixed.inverted.menu .toc.item {
                display: block;
            }
        }
    </style>
    @yield('style')
</head>
<body>
@include('templates._partials.mobile_menu')
<div class="pusher">
    <div class="ui top fixed inverted menu">
        <div class="ui container">
        <a class="toc icon item">
            <i class="sidebar orange icon"></i>
        </a>
        <a href="{{ route('home') }}" class="header item">
            <img src="{{ asset('assets/logo.png') }}" alt="{{ config('app.name') }}" class="logo">
        </a>
        <a href="{{ route('companies') }}" title="{{ trans('common.companies') }}" class="{{ (Request::is('company') || Request::is('company/*')) ? 'active ' : '' }}icon item">
            <i class="building {{ (Request::is('company') || Request::is('company/*')) ? 'orange ' : '' }}icon"></i>
        </a>
        <a href="{{ route('jobs') }}" title="{{ trans('common.jobs') }}" class="{{ (Request::is('jobs') || Request::is('jobs/*')) ? 'active ' : '' }}icon item">
            <i class="newspaper {{ (Request::is('jobs') || Request::is('jobs/*')) ? 'orange ' : '' }}icon"></i>
        </a>
        @if (Auth::user()->type == 'user' || Auth::user()->type == 'all')
        <a href="{{ route('applications') }}" title="{{ trans('common.applications') }}" class="{{ (Request::is('applications') || Request::is('applications/*')) ? 'active ' : '' }}icon item">
            <i class="list {{ (Request::is('applications') || Request::is('applications/*')) ? 'orange ' : '' }}icon"></i>
        </a>
        @endif
        <a href="#" title="{{ trans('common.events') }}" class="icon item">
            <i class="calendar icon"></i>
        </a>
        <div class="right menu">
            <div class="item">
                <form action="{{ route('search.results') }}" method="get" class="ui form">
                    <div class="ui icon input search">
                        <input type="text" name="q" placeholder="{{ trans('common.search') }}">
                        <i class="search link icon"></i>
                    </div>
                </form>
            </div>
            <a href="{{ route('profile', ['slug' => Auth::user()->slug_name]) }}" class="{{ (Request::is(Auth::user()->slug_name) || Request::is(Auth::user()->slug_name.'/*')) ? 'active ' : '' }}item">
                <img src="{{ Auth::user()->userImage() }}" alt="{{ Auth::user()->getDisplayName() }}" class="ui avatar image">
                {{ Auth::user()->first_name }}
            </a>
            <a href="{{ route('messages') }}" class="{{ (Request::is('messages') || Request('messages/*')) ? 'active ' : '' }}icon item">
                <i class="comments outline {{ (Request::is('messages') || Request::is('messages/*')) ? 'orange ' : '' }} icon"></i>
            </a>
            <a href="{{ route('notifications') }}" class="{{ (Request::is('notifications') || Request('notifications/*')) ? 'active ' : '' }}icon item">
                <i class="bell outline {{ (Request::is('notifications') || Request::is('notifications/*')) ? 'orange ' : '' }} icon"></i>
            </a>
            <a href="{{ route('settings') }}" title="{{ trans('common.settings') }}" class="{{ (Request::is('settings') || Request('settings/*')) ? 'active ' : '' }}icon item">
                <i class="settings {{ (Request::is('settings') || Request::is('settings/*')) ? 'orange ' : '' }} icon"></i>
            </a>
            @if (Auth::user()->isAdmin)
                <a href="{{ route('backend') }}" class="icon item">
                    <i class="dashboard icon"></i>
                </a>
            @endif
            <a href="{{ route('signout') }}" class="item">
                {{ trans('common.signout') }}
            </a>
        </div>
        </div>
    </div>
    <div class="ui main container">
        @yield('content')
    </div>
    <div class="ui inverted vertical footer segment">
        <div class="ui center aligned container">
            <div class="ui stackable inverted divided grid">
                <div class="three wide column">
                    <h4 class="ui inverted header">{{ config('app.name') }}</h4>
                    <div class="ui inverted link list">
                        <a href="{{ route('aboutus') }}" class="item">{{ trans('common.about_us') }}</a>
                        <a href="#" class="item">Careers</a>
                        <a href="{{ route('blog') }}" class="item">Blog</a>
                        <a href="{{ route('help') }}" class="item">Help</a>
                    </div>
                </div>
                <div class="three wide column">
                    <h4 class="ui inverted header">Group 2</h4>
                    <div class="ui inverted link list">
                        <a href="#" class="item">Contact</a>
                        <a href="#" class="item">Feedback</a>
                        <a href="#" class="item">Link Three</a>
                        <a href="#" class="item">Link Four</a>
                    </div>
                </div>
                <div class="three wide column">
                    <h4 class="ui inverted header">Social</h4>
                    <div class="ui inverted link list">
                        <a href="#" class="item">Facebook</a>
                        <a href="#" class="item">Twitter</a>
                        <a href="#" class="item">LinkedIn</a>
                        <a href="#" class="item">Xing</a>
                    </div>
                </div>
                <div class="seven wide column">
                    <h4 class="ui inverted header">Footer Header</h4>
                    <p>Extra space for a call to action inside the footer that could help re-engage users.</p>
                </div>
            </div>
            <div class="ui inverted section divider"></div>
            <h4 class="ui inverted header">
                {{ config('app.name') }} &copy; {{ date('Y') }}
            </h4>
            <div class="ui horizontal inverted small divided link list">
                <a class="item" href="#">Site Map</a>
                <a class="item" href="#">Contact Us</a>
                <a class="item" href="#">Terms and Conditions</a>
                <a class="item" href="#">Privacy Policy</a>
            </div>
        </div>
    </div>
</div>
<script src="{{ asset('assets/jquery.min.js') }}"></script>
<script src="{{ asset('assets/semantic.min.js') }}"></script>
<script>
    $(document)
        .ready(function() {

            // create sidebar and attach to menu open
            $('.ui.sidebar')
                .sidebar('attach events', '.toc.item')
            ;

            $('.message .close')
                .on('click', function() {
                    $(this)
                        .closest('.message')
                        .transition('fade')
                    ;
                })
            ;

        })
    ;
</script>
@yield('script')
</body>
</html>