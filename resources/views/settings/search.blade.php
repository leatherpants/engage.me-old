@extends('templates.frontend')
@section('title', trans('common.settings').' | '.config('app.name'))
@section('content')
    <div class="ui stackable grid">
        <div class="row">
            <div class="column">
                <div class="ui inverted tall stacked segment">
                    <h3 class="ui inverted orange header">
                        <i class="search icon"></i>
                        <span class="content">
                            {{ trans('settings.search') }}
                        </span>
                    </h3>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="four wide column">
                @include('settings._partials.sidebar')
            </div>
            <div class="twelve wide column">
                <div class="ui inverted tall stacked segment">
                    <form action="#" method="post" class="ui inverted form">
                        {{ csrf_field() }}
                        <div class="ui disabled field">
                            <label for="searching_by_email">{{ trans('settings.searching_by_email') }}</label>
                            <select name="" id="searching_by_email"></select>
                        </div>
                        <button class="ui disabled orange labeled icon button">
                            <i class="save icon"></i>
                            {{ trans('common.save') }}
                        </button>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection