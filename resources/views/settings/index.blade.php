@extends('templates.frontend')
@section('title', trans('common.settings') . ' | '.config('app.name'))
@section('content')
    <div class="ui stackable grid">
        <div class="row">
            <div class="column">
                <div class="ui inverted tall stacked segment">
                    <h3 class="ui inverted orange header">
                        <i class="settings icon"></i>
                        <span class="content">
                            {{ trans('common.settings') }}
                        </span>
                    </h3>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="column">
                <div class="ui inverted tall stacked segment">
                    <div class="ui inverted divided list">
                        <div class="item">
                            <div class="content">
                                <div class="header">
                                    {{ trans('settings.account') }}
                                </div>
                                <div class="description">
                                    {{ trans('settings.account_desc') }} <a href="{{ route('settings.account') }}"><i class="angle double right icon"></i></a>
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="content">
                                <div class="header">
                                    {{ trans('settings.image') }}
                                </div>
                                <div class="description">
                                    {{ trans('settings.image_desc') }} <a href="{{ route('settings.image') }}"><i class="angle double right icon"></i></a>
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="content">
                                <div class="header">
                                    {{ trans('settings.language') }}
                                </div>
                                <div class="description">
                                    {{ trans('settings.language_desc') }} <a href="{{ route('settings.language') }}"><i class="angle double right icon"></i></a>
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="content">
                                <div class="header">
                                    {{ trans('settings.password') }}
                                </div>
                                <div class="description">
                                    {{ trans('settings.password_desc') }} <a href="{{ route('settings.password') }}"><i class="angle double right icon"></i></a>
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="content">
                                <div class="header">
                                    {{ trans('settings.email') }}
                                </div>
                                <div class="description">
                                    {{ trans('settings.email_desc') }} <a href="{{ route('settings.email') }}"><i class="angle double right icon"></i></a>
                                </div>
                            </div>
                        </div>
                        @if (Auth::user()->settings->beta)
                        <div class="item">
                            <div class="content">
                                <div class="header">
                                    {{ trans('settings.search') }}
                                </div>
                                <div class="description">
                                    {{ trans('settings.search_desc') }} <a href="{{ route('settings.search') }}"><i class="angle double right icon"></i></a>
                                </div>
                            </div>
                        </div>
                        @endif
                        @if (Auth::user()->settings->beta)
                        <div class="item">
                            <div class="content">
                                <div class="header">
                                    {{ trans('settings.forum') }}
                                </div>
                                <div class="description">
                                    {{ trans('settings.forum_desc') }} <a href="#"><i class="angle double right icon"></i></a> <small>(No link at the moment)</small>{{--TODO--}}
                                </div>
                            </div>
                        </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection