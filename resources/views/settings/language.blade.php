@extends('templates.frontend')
@section('title', trans('common.settings') . ' | '.config('app.name'))
@section('content')
    <div class="ui stackable grid">
        <div class="row">
            <div class="column">
                <div class="ui inverted tall stacked segment">
                    <h3 class="ui inverted orange header">
                        <i class="settings icon"></i>
                        <span class="content">
                            {{ trans('settings.language') }}
                        </span>
                    </h3>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="four wide column">
                @include('settings._partials.sidebar')
            </div>
            <div class="twelve wide column">
                <div class="ui inverted tall stacked segment">
                    <form action="{{ route('settings.language') }}" method="post" autocomplete="off" class="ui inverted form">
                        {{ csrf_field() }}
                        <div class="{{ $errors->has('lang') ? 'error ' : '' }}field">
                            <label for="lang">{{ trans('common.language') }}</label>
                            <select name="lang" id="lang">
                                @foreach($langs as $lang)
                                    <option value="{{ $lang->code }}"{{ ($lang->code == $user->lang) ? ' selected' : '' }}>{{ trans('lang.' . $lang->name) }}</option>
                                @endforeach
                            </select>
                        </div>
                        <button class="ui orange labeled icon button">
                            <i class="save icon"></i>
                            {{ trans('common.save') }}
                        </button>
                    </form>
                    @if (count($errors)>0)
                        <div class="ui error message">
                            @include('_partials.message.error')
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
@endsection