@extends('templates.frontend')
@section('title', trans('common.settings') . ' | '.config('app.name'))
@section('content')
    <div class="ui stackable grid">
        <div class="row">
            <div class="column">
                <div class="ui inverted tall stacked segment">
                    <h3 class="ui inverted orange header">
                        <i class="settings icon"></i>
                        <span class="content">
                            {{ trans('settings.account') }}
                        </span>
                    </h3>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="four wide column">
                @include('settings._partials.sidebar')
            </div>
            <div class="twelve wide column">
                @include('_partials.message.session')
                <div class="ui inverted tall stacked segment">
                    <form action="{{ route('settings.account') }}" method="post" class="ui inverted form">
                        {{ csrf_field() }}
                        <div class="two fields">
                            <div class="field{{ $errors->has('first_name') ? ' error' : '' }}">
                                <label for="first_name">{{ trans('common.first_name') }}</label>
                                <input type="text" name="first_name" id="first_name" placeholder="{{ trans('common.first_name') }}" value="{{ $user->first_name }}">
                            </div>
                            <div class="field{{ $errors->has('last_name') ? ' error' : '' }}">
                                <label for="last_name">{{ trans('common.last_name') }}</label>
                                <input type="text" name="last_name" id="last_name" placeholder="{{ trans('common.last_name') }}" value="{{ $user->last_name }}">
                            </div>
                        </div>
                        <div class="field{{ $errors->has('birthday') ? ' error' : '' }}">
                            <label for="birthday">{{ trans('common.birthday') }}</label>
                            <input type="text" name="birthday" id="birthday" placeholder="{{ trans('common.birthday') }}" value="{{ ($user->birthday !== null) ? date('d.m.Y', strtotime($user->birthday)) : '' }}">
                        </div>
                        <div class="fields">
                            <div class="four wide field{{ $errors->has('gender') ? ' error' : '' }}">
                                <label for="gender">{{ trans('common.gender') }}</label>
                                <div class="inline fields" style="display: inline;">
                                    <div class="field">
                                        <div class="ui radio checkbox">
                                            <input type="radio" name="gender" id="female" {{ ($user->gender == 'f') ? ' checked' : '' }} tabindex="0" value="f" class="hidden">
                                            <label for="female">{{ trans('common.female') }}</label>
                                        </div>
                                    </div>
                                    <div class="field">
                                        <div class="ui radio checkbox">
                                            <input type="radio" name="gender" id="male" {{ ($user->gender == 'm') ? ' checked' : '' }} tabindex="0" value="m" class="hidden">
                                            <label for="male">{{ trans('common.male') }}</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="twelve wide field{{ $errors->has('occupation') ? ' error' : '' }}">
                                <label for="occupation">{{ trans('common.occupation') }}</label>
                                <input type="text" name="occupation" id="occupation" placeholder="{{ trans('common.occupation') }}" value="{{ $user->occupation }}">
                            </div>
                        </div>
                        <button class="ui orange labeled icon button">
                            <i class="save icon"></i>
                            {{ trans('common.save') }}
                        </button>
                    </form>
                    @if (count($errors)>0)
                        <div class="ui error message">
                            @include('_partials.message.error')
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')
<script type="text/javascript">
    $('.ui.radio.checkbox')
        .checkbox()
    ;
</script>
@endsection