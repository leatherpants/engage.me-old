<div class="ui inverted tall stacked segment" style="height: 100%;">
    <div class="ui inverted vertical fluid menu">
        <a href="{{ route('settings') }}" class="item">
            <i class="settings icon"></i>
            {{ trans('common.settings') }}
        </a>
        <div class="item">
            <div class="menu">
                <a href="{{ route('settings.account') }}" class="{{ Request::is('settings/account') ? 'active ' : '' }}item">
                    <i class="user icon"></i>
                    {{ trans('settings.account') }}
                </a>
                <a href="{{ route('settings.image') }}" class="{{ Request::is('settings/image') ? 'active ' : '' }}item">
                    <i class="image icon"></i>
                    {{ trans('settings.image') }}
                </a>
                <a href="{{ route('settings.language') }}" class="{{ Request::is('settings/language') ? 'active ' : '' }}item">
                    <i class="translate icon"></i>
                    {{ trans('settings.language') }}
                </a>
                <a href="{{ route('settings.password') }}" class="{{ Request::is('settings/password') ? 'active ' : '' }}item">
                    <i class="lock icon"></i>
                    {{ trans('settings.password') }}
                </a>
                <a href="{{ route('settings.email') }}" class="{{ Request::is('settings/email') ? 'active ' : '' }}item">
                    <i class="at icon"></i>
                    {{ trans('settings.email') }}
                </a>
                @if (Auth::user()->settings->beta)
                <a href="{{ route('settings.search') }}" class="{{ Request::is('settings/search') ? 'active ' : '' }}item">
                    <i class="search icon"></i>
                    {{ trans('settings.search') }}
                </a>
                @endif
            </div>
        </div>
    </div>
</div>