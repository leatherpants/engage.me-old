@extends('templates.frontend')
@section('title', trans('common.settings') . ' | '.config('app.name'))
@section('content')
    <div class="ui stackable grid">
        <div class="row">
            <div class="column">
                <div class="ui inverted tall stacked segment">
                    <h3 class="ui inverted orange header">
                        <i class="at icon"></i>
                        <span class="content">
                            {{ trans('settings.email') }}
                        </span>
                    </h3>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="four wide column">
                @include('settings._partials.sidebar')
            </div>
            <div class="twelve wide column">
                <div class="ui inverted tall stacked segment">
                    <form action="" method="post" class="ui inverted form">
                        {{ csrf_field() }}
                        <div class="field">
                            <label for="email">{{ trans('common.email') }}</label>
                            <input type="email" name="email" id="email" disabled placeholder="{{ trans('common.email') }}" value="{{ $user->email }}">
                        </div>
                        <button class="ui orange icon labeled disabled button">
                            <i class="save icon"></i>
                            {{ trans('common.save') }}
                        </button>
                    </form>
                </div>
                @if (Auth::user()->settings->beta)
                <div class="ui inverted tall stacked segment">
                    <h3 class="ui inverted dividing header">
                        {{ trans('common.subscriptions') }}
                    </h3>
                    <form class="ui inverted form">
                        <button class="ui orange icon labeled disabled button">
                            <i class="save icon"></i>
                            {{ trans('common.save') }}
                        </button>
                    </form>
                </div>
                @endif
            </div>
        </div>
    </div>
@endsection