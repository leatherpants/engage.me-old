@extends('templates.frontend')
@section('title', trans('common.settings').' | '.config('app.name'))
@section('content')
    <div class="ui stackable grid">
        <div class="row">
            <div class="column">
                <div class="ui inverted tall stacked segment">
                    <h3 class="ui inverted orange header">
                        <i class="settings icon"></i>
                        <span class="content">
                            {{ trans('settings.image') }}
                        </span>
                    </h3>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="four wide column">
                @include('settings._partials.sidebar')
            </div>
            <div class="twelve wide column">
                <div class="ui inverted tall stacked center aligned segment">
                    <h3 class="ui inverted dividing left aligned header">Profile image</h3>
                    <div class="ui image">
                        <div class="ui dimmer">
                            <div class="content">
                                <div class="center">
                                    <div class="ui orange icon button">
                                        <i class="upload icon"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <img src="{{ $user->userImage() }}" class="ui large image" alt="{{ $user->getDisplayName() }}">
                    </div>
                </div>
                <div class="ui inverted tall stacked center aligned segment">
                    <h3 class="ui inverted dividing left aligned header">CV Frontpage image</h3>
                    <div class="ui image">
                        <div class="ui dimmer">
                            <div class="content">
                                <div class="center">
                                    <div class="ui orange icon button">
                                        <i class="upload icon"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @if (isset($user->items->user_image))
                        <img src="{{ $user->items->user_image }}" class="ui large image" alt="{{ $user->getDisplayName() }}">
                        @endif
                    </div>
                </div>
                @if (Auth::user()->settings->beta)
                <div class="ui inverted tall stacked segment">
                    <form action="#" method="post" class="ui inverted form">
                        {{ csrf_field() }}
                        <div class="ui disabled field">
                            <label for="image">{{ trans('settings.image_for') }}</label>
                            <select name="image" id="image">
                                <option value="0">{{ trans('common.nobody') }}</option>
                                <option value="1">{{ trans('common.contacts') }}</option>
                                <option value="2">{{ trans('common.all') }}</option>
                            </select>
                        </div>
                        <button class="ui orange disabled labeled icon button">
                            <i class="save icon"></i>
                            {{ trans('common.save') }}
                        </button>
                    </form>
                </div>
                @endif
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script type="text/javascript">
        $('.ui.image')
            .dimmer({
                on: 'hover'
            })
        ;
    </script>
@endsection