@extends('templates.frontend')
@section('title', trans('common.settings').' | '.config('app.name'))
@section('content')
    <div class="ui stackable grid">
        <div class="row">
            <div class="column">
                <div class="ui inverted tall stacked segment">
                    <h3 class="ui inverted orange header">
                        <i class="settings icon"></i>
                        <span class="content">
                            {{ trans('settings.password') }}
                        </span>
                    </h3>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="four wide column">
                @include('settings._partials.sidebar')
            </div>
            <div class="twelve wide column">
                <div class="ui inverted tall stacked segment">
                    <form action="{{ route('settings.password') }}" method="post" class="ui inverted form">
                        {{ csrf_field() }}
                        <div class="field{{ $errors->has('current_password') ? ' error' : '' }}">
                            <label for="current_password">{{ trans('settings.password_current') }}</label>
                            <input type="password" name="current_password" id="current_password" placeholder="{{ trans('settings.password_current') }}" autocomplete="off">
                        </div>
                        <div class="field{{ $errors->has('new_password') ? ' error' : '' }}">
                            <label for="new_password">{{ trans('settings.password_new') }}</label>
                            <input type="password" name="new_password" id="new_password" placeholder="{{ trans('settings.password_new') }}">
                        </div>
                        <div class="field{{ $errors->has('confirm_password') ? ' error' : '' }}">
                            <label for="confirm_password">{{ trans('settings.password_confirm') }}</label>
                            <input type="password" name="confirm_password" id="confirm_password" placeholder="{{ trans('settings.password_confirm') }}">
                        </div>
                        <button type="submit" class="ui orange labeled icon button">
                            <i class="save icon"></i>
                            {{ trans('common.save') }}
                        </button>
                    </form>
                    @if (count($errors)>0)
                        <div class="ui error message">
                            @include('_partials.message.error')
                        </div>
                    @endif
                    @if (session('success'))
                        <div class="ui positive message">
                            {{ session('success') }}
                        </div>
                    @endif
                    @if (session('error'))
                        <div class="ui negative message">
                            {{ session('error') }}
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
@endsection