<?php

return [
    'actions' => 'Actions',
    'choose_company' => 'Choose company',
    'choose_location' => 'Choose location',
    'choose_contact' => 'Choose contact',
    'company' => 'Company',
    'content' => 'Content',
    'create' => 'Write application',
    'infos' => 'Informations',
    'status' => 'Status',
    'title' => 'Title',
    'type' => [
        'form' => 'Form',
        'mail' => 'Email',
        'type' => 'Type',
    ],
    'write' => 'Write application',
];
