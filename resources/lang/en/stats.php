<?php

return [
    'daily' => 'Daily',
    'joined' => 'Joined :time',
    'monthly' => 'Monthly',
    'statistics' => 'Statistics',
    'total' => 'Total',
    'users' => 'Users',
    'yearly' => 'Yearly',
];
