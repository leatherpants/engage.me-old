<?php

return [
    'all_tickets' => 'All tickets',
    'assign' => 'Assign',
    'assigned' => 'Assigned',
    'closed_tickets' => 'Closed tickets',
    'content' => 'Content',
    'create_ticket' => 'Create ticket',
    'support' => 'Support',
    'title' => 'Title',
    'unassigned' => 'Unassigned',
    'your_ticket' => 'Your ticket',
    'your_tickets' => 'Your tickets',
];
