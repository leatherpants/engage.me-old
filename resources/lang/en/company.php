<?php

return [
    'description' => 'Description',
    'details' => [
        'edit' => 'Edit details',
    ],
    'founded_year' => 'Founding year',
    'location' => [
        'address' => 'Address',
        'city' => 'City',
        'country' => 'Country',
        'edit' => 'Edit location',
        'location' => 'Location',
        'street' => 'Street',
        'zip_code' => 'ZIP Code',
    ],
    'profile_owner' => 'Owner of profile',
    'socialmedia' => [
        'edit' => 'Edit social media',
        'socialmedia' => 'Social Media',
    ],
];
