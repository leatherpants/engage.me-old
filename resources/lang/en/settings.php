<?php

return [
    'account' => 'Account Settings',
    'account_desc' => 'Take a view on your details what you registered',
    'email' => 'Email Settings',
    'email_desc' => 'Change your email and different settings for that',
    'image' => 'Image Settings',
    'image_desc' => 'Upload new images on your profile',
    'language' => 'Language Settings',
    'language_desc' => 'Change your language you use on engage me',
    'password' => 'Password Settings',
    'password_confirm' => 'Confirm password',
    'password_current' => 'Current password',
    'password_desc' => 'Choose a unique password that you\'re not using elsewhere',
    'password_new' => 'New password',
    'search' => 'Search Settings',
];
