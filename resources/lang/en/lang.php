<?php

return [
    'danish' => 'Danish',
    'english' => 'English',
    'french' => 'French',
    'german' => 'German',
    'italian' => 'Italian',
    'spanish' => 'Spanish',
    'swedish' => 'Swedish',

    'de' => 'German',
    'en' => 'English',
    'it' => 'Italian',
    'se' => 'Swedish',

    'rating' => [
        'native' => 'Native language',
    ],
];
