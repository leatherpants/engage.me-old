<?php

return [
    'no_applications' => 'You don\'t have any applications yet.',
    'no_contacts' => 'You don\'t have any contacts yet.',
    'no_notifications' => 'You don\'t have any notifications yet.',
    'no_received_requests' => 'You don\'t have any pending contact requests.',
    'no_sent_requests' => 'You don\'t have any pending contact requests at the moment.',
    'no_tickets' => 'No tickets available',
    'no_tickets_created_by_user' => 'You don\'t have no tickets created.',
    'user_is_blocked' => 'You\'ve been blocked',
];
