<?php

return [
    'danish' => 'Dänisch',
    'english' => 'Englisch',
    'french' => 'Französisch',
    'german' => 'Deutsch',
    'italian' => 'Italienisch',
    'spanish' => 'Spanisch',
    'swedish' => 'Schwedisch',

    'de' => 'Deutsch',
    'en' => 'Englisch',
    'it' => 'Italienisch',
    'se' => 'Schwedisch',

    'rating' => [
        'native' => 'Muttersprache',
    ],
];
