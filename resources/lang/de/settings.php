<?php

return [
    'account' => 'Kontoeinstellungen',
    'email' => 'eMaileinstellungen',
    'image' => 'Bildeinstellungen',
    'language' => 'Spracheinstellungen',
    'password' => 'Passworteinstellungen',
    'password_confirm' => 'Passwort bestätigen',
    'password_current' => 'Aktuelles Passwort',
    'password_new' => 'Neues Passwort',
    'search' => 'Sucheinstellungen',
];
