<?php

return [
    'description' => 'Beschreibung',
    'details' => [
        'edit' => 'Infos bearbeiten',
    ],
    'location' => [
        'address' => 'Adresse',
        'city' => 'Stadt',
        'country' => 'Land',
        'edit' => 'Standort bearbeiten',
        'location' => 'Standort',
        'street' => 'Straße',
        'zip_code' => 'Postleitzahl',
    ],
    'profile_owner' => 'Eigentümer des Profils',
    'socialmedia' => [
        'edit' => 'Social Media bearbeiten',
        'socialmedia' => 'Social Media',
    ],
];
