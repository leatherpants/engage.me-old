<?php

return [
    'actions' => 'Aktionen',
    'choose_company' => 'Wähle eine Firma',
    'choose_location' => 'Wähle einen Standort',
    'choose_contact' => 'Wähle einen Ansprechpartner',
    'company' => 'Firma',
    'content' => 'Inhalt',
    'create' => 'Bewerbung schreiben',
    'infos' => 'Informationen',
    'site' => 'Seite',
    'status' => 'Status',
    'title' => 'Stellenangebot',
    'type' => [
        'form' => 'Formular',
        'mail' => 'eMail',
        'type' => 'Typ',
    ],
    'write' => 'Bewerbung schreiben',
];
