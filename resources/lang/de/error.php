<?php

return [
    'no_applications' => 'Sie haben bisher keine Bewerbungen geschrieben.',
    'no_contacts' => 'Sie haben noch keine Kontakte.',
    'no_received_requests' => 'Sie haben derzeit keine offenen Kontaktanfragen.',
    'no_sent_requests' => 'Sie haben derzeit keine unbeantworteten Kontaktanfragen.',
    'wrong_email_or_password' => 'Entweder haben Sie ein falsches Passwort oder eine falsche eMailadresse eingegeben.',
];
