<?php

return [
    'birthday' => 'Födelsedag',
    'female' => 'Kvinna',
    'first_name' => 'Förnamn',
    'gender' => 'Kön',
    'language' => 'Språk',
    'last_name' => 'Efternamn',
    'male' => 'Man',
    'save' => 'Spara ändringar',
    'settings' => 'Inställningar',
    'signin' => 'Logga in',
    'signout' => 'Logga ut',
    'social' => 'Sociala Medier',
];
